Welcome
=======

Edisco is a smart vocabulary. It helps everyone to learn new words.

[UI](https://www.fluidui.com/editor/live/preview/p_8IWunqqtt7B1cGrAGO2tG5uVdp9COm63.1482918157332)

[Forgetting curve](https://docs.google.com/spreadsheets/d/10kujGUCkycGT7vg6J8j9WeTGVE2TP0iW-_yzghtoFFg/edit?usp=sharing)