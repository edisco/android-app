package cc.snakelab.edisco.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;


import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.RetentionHelper;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by snake on 08.10.16.
 */
public class DbSeeder {
    public static void Populate(SQLiteDatabase sql) {
        PopulateWord(sql);
        PopulateWordPair(sql);
        PopulateReminder(sql);
        PopulateWordPairHistory(sql);
    }
    private static void PopulateWord(SQLiteDatabase sql) {
        ContentValues vals = new ContentValues();
        vals.put("id", 1);
        vals.put("text", "cat");

        sql.insert("word", null, vals);
        vals.clear();
        vals.put("id", 2);
        vals.put("text", "кот");
        sql.insert("word", null,vals);


        vals.clear();
        vals.put("id", 3);
        vals.put("text", "собака");
        sql.insert("word", null,vals);
        sql.insert("word", null, vals);

        vals.clear();
        vals.put("id", 4);
        vals.put("text", "dog");
        sql.insert("word", null,vals);

        vals.clear();
        vals.put("id", 5);
        vals.put("text", "hamster");
        sql.insert("word", null,vals);

        vals.clear();
        vals.put("id", 6);
        vals.put("text", "хомяк");
        sql.insert("word", null,vals);
    }
    private static void PopulateWordPair(SQLiteDatabase sql) {
        ContentValues vals = new ContentValues();
        vals.put("id", 1);
        vals.put("front_id", 1);
        vals.put("back_id", 2);

        sql.insert("word_pair", null, vals);

        vals.clear();
        vals.put("id", 2);
        vals.put("front_id", 3);
        vals.put("back_id", 4);
        vals.put("stage", RetentionHelper.STAGE_2);

        sql.insert("word_pair", null, vals);

        vals.clear();
        vals.put("id", 3);
        vals.put("front_id", 5);
        vals.put("back_id", 6);
        vals.put("stage", RetentionHelper.STAGE_14);

        sql.insert("word_pair", null, vals);

    }
    private  static  void PopulateReminder(SQLiteDatabase sql) {
        ContentValues vals = new ContentValues();


        vals.put("id", 1);
        Calendar cal = new GregorianCalendar();
        long secs = cal.getTime().getTime();
        vals.put("start_hour", "8");
        vals.put("start_min", "0");
        vals.put("finish_hour", "20");
        vals.put("finish_min", "0");
        vals.put("active", true);
        vals.put("pairs", "1, 3");
        vals.put("last_remind_timestamp", "2016-10-31 10:32:01.000 GMT+0");
        vals.put("current_pair_id", "1");
        vals.put("guess_front", true);

        sql.insert("reminder", null, vals);
    }

    private static void PopulateWordPairHistory(SQLiteDatabase sql) {
        ContentValues vals = new ContentValues();
        // word pair 1
        vals.put("id", 1);
        vals.put("correct", true);
        vals.put("word_pair_id", 1);
        Calendar cal = DateHelper.getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, -2);
        vals.put("created_at", DateHelper.dateToIso8601(cal.getTime()));
        sql.insert("word_pair_history", null, vals);

        // word pair 2
        vals.clear();
        vals.put("id", 2);
        vals.put("correct", true);
        vals.put("word_pair_id", 2);
        vals.put("created_at",  DateHelper.dateToIso8601(DateHelper.getCalendar().getTime()));
        sql.insert("word_pair_history", null, vals);


    }
}
