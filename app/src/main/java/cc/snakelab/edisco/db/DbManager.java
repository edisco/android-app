package cc.snakelab.edisco.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by snake on 01.10.16.
 */
public class DbManager {
    private SQLiteDatabase sql;

    public DbManager(SQLiteDatabase sql) {
        this.sql = sql;
    }


    public SQLiteDatabase getSql() {
        return sql;
    }

}
