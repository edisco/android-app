package cc.snakelab.edisco.db;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.helper.RetentionHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTripleHistory;
import cc.snakelab.edisco.orm.Word_Table;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by snake on 18.01.17.
 */

public class DBFlowSeeder {
    private static Date date;
    public static void populate() {
        try {
            date = DateHelper.iso8601ZToDate("2017-04-20 12:12:59.123 UTC");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        populateWord();
        populateWordPair();
        populateReminder();
        populateWordPairHistory();
        populateWordTriple();
        populateWordTripleHistory();

    }
    static void populateWord() {
        Word w = new Word(1, "cat", LocaleHelper.EN);
        w.setCreated_at(date);
        w.save();

        w = new Word(2, "кот", LocaleHelper.RU);
        w.setCreated_at(date);
        w.save();

        w = new Word(3, "собака", LocaleHelper.RU);
        w.setCreated_at(date);
        w.save();

        w = new Word(4, "dog", LocaleHelper.EN);
        w.setCreated_at(date);
        w.save();

        w = new Word(5, "hamster", LocaleHelper.EN);
        w.setCreated_at(date);
        w.save();

        w = new Word(6, "хомяк", LocaleHelper.RU);
        w.setCreated_at(date);
        w.save();

        w = new Word(7, "trashed_word", LocaleHelper.EN);
        w.setCreated_at(date);
        w.trash();

        w = new Word(8, "net", LocaleHelper.EN);
        w.setCreated_at(date);
        w.save();

        w = new Word(9, "чистый доход", LocaleHelper.RU);
        w.setCreated_at(date);
        w.save();

        w = new Word(10, "сеть", LocaleHelper.RU);
        w.setCreated_at(date);
        w.save();


        w = new Word(11, "kat", LocaleHelper.DE);
        w.setCreated_at(date);
        w.save();


        if (AppHelper.isTest()) {
            SQLite.update(Word.class).set(Word_Table.uuid.eq(Word_Table.id)).async().execute();
            SQLite.update(Word.class).set(Word_Table.synced.eq(true)).async().execute();
        }


    }

    static void populateWordPair() {
        WordPair wp = new WordPair(1, 1, 2);
        wp.setUuid("1");
        wp.setCreated_at(date);
        wp.save();

        wp = new WordPair(2, 3, 4);
        wp.setCreated_at(date);
        wp.setUuid("2");
        wp.setStage(RetentionHelper.STAGE_2);
        wp.save();

        wp = new WordPair(3, 5, 6);
        wp.setCreated_at(date);
        wp.setUuid("3");
        wp.setStage(RetentionHelper.STAGE_14);
        wp.save();

        wp = new WordPair(4, 8, 9);
        wp.setCreated_at(date);
        wp.setUuid("4");
        wp.setStage(RetentionHelper.STAGE_60);
        wp.save();

        wp = new WordPair(5, 8, 10);
        wp.setCreated_at(date);
        wp.setUuid("5");
        wp.setStage(RetentionHelper.STAGE_60);
        wp.save();



    }
    static void populateReminder() {

        Reminder r = new Reminder();
        r.setStartHour(8);
        r.setStartMin(0);
        r.setFinishHour(20);
        r.setFinishMin(0);
        r.setActive(true);
        r.setPairs("1, 3");
        r.setTriples("1, 3");
        try {
            r.setLastRemind(DateHelper.iso8601ZToDate("2016-10-31 10:32:01.000 +02:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        r.setCurrentWordPairId(1);
        r.setCurrentWordTripleId(1);
        r.setGuessFront(true);
        r.setGuessPair(true);
        r.save();
    }

    static void populateWordPairHistory() {
        Calendar cal = DateHelper.getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, -2);
        WordPairHistory wph = new WordPairHistory(1, true, 1, cal.getTime());
        wph.save();

        wph = new WordPairHistory(2, true, 2, DateHelper.getCalendar().getTime());
        wph.save();

        wph = new WordPairHistory(3, true, 3, DateHelper.getCalendar().getTime());
        wph.save();
        wph = new WordPairHistory(4, true, 4, DateHelper.getCalendar().getTime());
        wph.save();
        wph = new WordPairHistory(5, true, 5, DateHelper.getCalendar().getTime());
        wph.save();

    }

    static void populateWordTripleHistory() {
        Calendar cal = DateHelper.getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, -2);
        WordTripleHistory wpt = new WordTripleHistory(1, true, 2, cal.getTime());
        wpt.save();

        wpt = new WordTripleHistory(2, true, 2, DateHelper.getCalendar().getTime());
        wpt.save();

        cal = DateHelper.getCalendar();
        cal.add(Calendar.HOUR_OF_DAY, -24);
        wpt = new WordTripleHistory(3, true, 4, cal.getTime());
        wpt.save();


    }
    static void populateWordTriple() {
        WordTriple wt = new WordTriple("fly", "flew", "flown", "летать", LocaleHelper.EN, LocaleHelper.RU);
        wt.setId(1);
        wt.setCreated_at(date);
        wt.save();

        wt = new WordTriple("cut", "cut", "cut", "резать", LocaleHelper.EN, LocaleHelper.RU);
        wt.setStage(RetentionHelper.STAGE_2);
        wt.setCreated_at(date);
        wt.setId(2);
        wt.save();

        wt = new WordTriple("dig", "dug", "dug", "копать", LocaleHelper.EN, LocaleHelper.RU);
        wt.setId(3);
        wt.setCreated_at(date);
        wt.save();

        wt = new WordTriple("eat", "ate", "eaten", "кушать", LocaleHelper.EN, LocaleHelper.RU);
        wt.setId(4);
        wt.setStage(RetentionHelper.STAGE_2);
        wt.setCreated_at(date);
        wt.save();

        wt = new WordTriple("finden", "fand", "gefunden", "искать", LocaleHelper.DE, LocaleHelper.RU);
        wt.setId(5);
        wt.setStage(RetentionHelper.STAGE_2);
        wt.setCreated_at(date);
        wt.save();

    }
}
