package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Reminder;

public class RemindSetupActivity extends AppCompatActivity implements RemindSetupFragment.OnUpdateListiner {

    private static final String TAG = "RemindSetupActivity";

    TextView mNextRemind;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.activity_remind_setup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mNextRemind = (TextView) findViewById(R.id.next_remind);
        RemindSetupFragment fragment = new RemindSetupFragment();
        fragment.setListiner(this);
        // Display the fragment as the main content.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.remind_settings, fragment)
                .commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateUI();


    }
    private void updateUI()  {
        Reminder reminder = null;
        try {
            reminder = App.app.getReminder();
        } catch (Exception e) {
            AppLog.e(TAG, "Unable to get reminder", e);
            return;
        }

        // next remind
        mNextRemind.setText(DateHelper.dateToIso8601(reminder.calcNextRemind(DateHelper.getCalendar().getTime())));


    }

    @Override
    public void onUpdate() {
        updateUI();
    }
}
