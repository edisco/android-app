package cc.snakelab.edisco;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.Reminder;

import static android.view.KeyEvent.ACTION_DOWN;

/**
 * Created by snake on 02.03.17.
 */

public class RemindWordPairFragment extends RemindFragment implements TextView.OnEditorActionListener {
    //word pair
    TextView guessText;
    TextView mTripleText;
    EditText editText;
    TextView translText;
    TextView mFrontLangText;
    TextView mBackLangText;

    Reminder mReminder;

    ColorStateList textColors;
    View mView;
    IOnGuessListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remind_word_pair, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        guessText = (TextView) view.findViewById(R.id.guessText);
        translText = (TextView) view.findViewById(R.id.transText);
        mTripleText = (TextView) view.findViewById(R.id.tripleText);
        mTripleText.setVisibility(View.GONE);
        editText = (EditText) view.findViewById(R.id.editGuess);
        editText.setOnEditorActionListener(this);
        textColors = editText.getTextColors();
        mFrontLangText = (TextView) view.findViewById(R.id.frontLang);
        mBackLangText = (TextView) view.findViewById(R.id.backLang);

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_NULL && event.getAction() == ACTION_DOWN) {
//            sendMessage();
            if (v == editText) {
                mListener.handleGuessText();
                handled = true;
            }
//            app.toast("guess");
//            handled = true;
        }
        return handled;
    }
    public void start(Reminder reminder) {

        guessText.setText(reminder.getGuessWord().toString());
        translText.setText(reminder.getTranslMask(""));
        mFrontLangText.setText(reminder.getGuessWord().getLang());
        mBackLangText.setText(reminder.getTranslationWord().getLang());
        AppHelper.focusAndKeyboard(editText);
        mReminder = reminder;
    }

    @Override
    public String getEnteredText() {
        return editText.getText().toString();
    }

    @Override
    public String getEnteredText2() {
        return null;
    }

    @Override
    public String getEnteredText3() {
        return null;
    }

    @Override
    public void setMask(String mask) {
        translText.setText(mask);
    }

    @Override
    public void setTranslText(String translMask) {
        translText.setText(translMask);
    }

    @Override
    public void guessIncorrect() {
        editText.setText(getEnteredText());
        editText.setTextColor(Color.RED);
        editText.selectAll();
        AppHelper.focusAndKeyboard(editText);
    }

    @Override
    public void setListener(IOnGuessListener listener) {
        mListener = listener;

    }

    @Override
    public int getWordIndex() {
        return -1;
    }

    @Override
    public void next() {

    }

    @Override
    public void speak() {
        App.app.speak(
                mReminder.getGuessWord().toString(),
                mReminder.getGuessWord().getLang());
    }

    @Override
    public boolean isFinish() {
        return true;
    }
}
