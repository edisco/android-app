package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.WordTriple;

import static android.view.KeyEvent.ACTION_DOWN;

/**
 * Created by snake on 02.03.17.
 */

public class RemindWordTripleOrgFragment extends RemindFragment implements TextView.OnEditorActionListener{
    TextView mOrgWord1;
    TextView mOrgWord2;
    TextView mOrgWord3;
    TextView mTransl;
    EditText mGuessWord;
    TextView mFrontLangText;
    TextView mBackLangText;
    Reminder mReminder;

    IOnGuessListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remind_word_triple_org, container, false);
    }
    @Override
    public void start(Reminder reminder) {
        mReminder = reminder;
        WordTriple wt = reminder.getCurrentWordTriple();
        mOrgWord1.setText(wt.getOrgWord1().getText());
        mOrgWord2.setText(wt.getOrgWord2().getText());
        mOrgWord3.setText(wt.getOrgWord3().getText());

        mTransl.setText(reminder.getTranslMask(""));
        mGuessWord.setText("");
        mFrontLangText.setText(reminder.getGuessWord().getLang());
        mBackLangText.setText(reminder.getTranslationWord().getLang());
        AppHelper.focusAndKeyboard(mGuessWord);

    }

    @Override
    public String getEnteredText() {
        return mGuessWord.getText().toString();
    }

    @Override
    public String getEnteredText2() {
        return null;
    }

    @Override
    public String getEnteredText3() {
        return null;
    }

    @Override
    public void setMask(String mask) {
        mTransl.setText(mask);
    }

    @Override
    public void setTranslText(String translMask) {
        mTransl.setText(translMask);
    }

    @Override
    public void guessIncorrect() {

    }

    @Override
    public void setListener(IOnGuessListener listener) {
        mListener = listener;
    }

    @Override
    public int getWordIndex() {
        return -1;
    }

    @Override
    public void next() {

    }

    @Override
    public void speak() {
        WordTriple wt = mReminder.getCurrentWordTriple();
        App.app.speak(
                wt.getOrgWord1().toString(),
                wt.getOrgWord1().getLang());
        App.app.speak(
                wt.getOrgWord2().toString(),
                wt.getOrgWord2().getLang());
        App.app.speak(
                wt.getOrgWord3().toString(),
                wt.getOrgWord3().getLang());
    }

    @Override
    public boolean isFinish() {
        return true;
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_NULL && keyEvent.getAction() == ACTION_DOWN) {

            if (textView == mGuessWord) {
                mListener.handleGuessText();
                handled = true;
            }
        }
        return handled;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOrgWord1 = (TextView) view.findViewById(R.id.orgWord1);
        mOrgWord2 = (TextView) view.findViewById(R.id.orgWord2);
        mOrgWord3 = (TextView) view.findViewById(R.id.orgWord3);
        mTransl  = (TextView) view.findViewById(R.id.transText);
        mGuessWord = (EditText) view.findViewById(R.id.editGuess);
        mGuessWord.setOnEditorActionListener(this);
        mFrontLangText = (TextView) view.findViewById(R.id.frontLang);
        mBackLangText = (TextView) view.findViewById(R.id.backLang);
    }


}
