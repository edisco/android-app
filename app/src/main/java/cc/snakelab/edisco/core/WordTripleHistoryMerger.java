package cc.snakelab.edisco.core;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.WordTripleHistory;
import cc.snakelab.edisco.orm.WordTripleHistory_Table;

import java.util.List;

/**
 * Created by snake on 18.04.17.
 */

public class WordTripleHistoryMerger extends BasicRecordMerger {
    private static final String TAG = "WordTripleHistoryMerger";
    @Override
    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        WordTripleHistory wth1 = (WordTripleHistory) record1;
        WordTripleHistory wth2 = (WordTripleHistory) record2;
        return wth1.isCorrect() == wth2.isCorrect() &&
                wth1.getWord_triple_id() == wth2.getWord_triple_id() &&
                super.isRecordSame(record1, record2);
    }

    @Override
    protected BasicRecord findRecordByUuid(String uuid) {
        return WordTripleHistory.findByUuid(WordTripleHistory.class, uuid);
    }

    @Override
    protected BasicRecord selectSameRecord(BasicRecord record) {
        WordTripleHistory wth = (WordTripleHistory) record;
        List<WordTripleHistory> list = SQLite.select().from(WordTripleHistory.class)
                .where(WordTripleHistory_Table.correct.eq(wth.isCorrect()))
                .and(WordTripleHistory_Table.word_triple_id.eq(wth.getWord_triple_id()))
                .and(WordTripleHistory_Table.created_at.eq(wth.getCreated_at())).queryList();
        if(list.size() > 1)
            AppLog.d(TAG, "found a few same records: " + list.size());

        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }
}
