package cc.snakelab.edisco.core;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPair_Table;

import java.util.List;

/**
 * Created by snake on 04.04.17.
 */

public class WordPairMerger extends BasicRecordMerger{


    private static final String TAG = "WordPairMerger";

    @Override
    protected BasicRecord selectSameRecord(BasicRecord record) {
        WordPair wordPair = (WordPair) record;
        List<WordPair> list =  SQLite.select().from(WordPair.class).where(WordPair_Table.orgWordId.eq(wordPair.getOrgWordId()))
                .and(WordPair_Table.translWordId.eq(wordPair.getTranslWordId()))
                .and(WordPair_Table.stage.eq(wordPair.getStage()))
                .and(WordPair_Table.order.eq(wordPair.getOrder()))
                .and(WordPair_Table.trashed.eq(wordPair.isTrashed()))
                .queryList();

        if(list.size() > 1)
            AppLog.d(TAG, "found a few same records: " + list.size());
        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }

    @Override
    protected BasicRecord findRecordByUuid(String uuid) {
        return WordPair.findByUuid(WordPair.class, uuid);
    }

    @Override
    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        WordPair wordPair1 = (WordPair) record1;
        WordPair wordPair2 = (WordPair) record2;

        return wordPair1.getOrgWordId() == wordPair2.getOrgWordId() &&
                wordPair1.getTranslWordId() == wordPair2.getTranslWordId() &&
                wordPair1.getStage() == wordPair2.getStage() &&
                wordPair1.getOrder() == wordPair2.getOrder() &&
                super.isRecordSame(record1, record2);
    }
}
