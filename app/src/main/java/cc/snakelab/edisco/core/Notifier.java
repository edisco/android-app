package cc.snakelab.edisco.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.NotificationCompat;

import java.util.Date;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.recievers.NotifyDeleteReceiver;

/**
 * Created by snake on 01.12.16.
 */

public class Notifier {
    final static String TAG = App.APPTAG+"Notifier";
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static void notifyIfNeeded(Context context) {

        App app = App.app;
        try {
            Reminder reminder = Reminder.get();
            if (!reminder.isWordSet()) {
                AppLog.d(TAG, "Current word pair is not set");
                return;
            }
            Date time = DateHelper.getCalendar().getTime();
            AppLog.d(TAG, "Current time is " + time.toString());
            AppLog.d(TAG, "Last Reminder " + reminder.getLastRemind());

            if (reminder.isTime(time)) {
              AppLog.d(TAG, "It's time to notify");
                if (App.app.isReminderVisible()) {
                    AppLog.d(TAG, "Reminder is visible. Do not add notify again");
                } else {
                    buildNotify(context, reminder);
                }
            }

        } catch (Exception e) {
            AppLog.e(TAG, "Error: " + e.toString(), e);

        }

    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static void buildNotify(Context context, Reminder reminder) throws Exception {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setSmallIcon(R.drawable.ic_statusbar)
                .setLargeIcon(BitmapFactory.decodeResource(App.app.getResources(), R.drawable.ic_edisco_logo))
                .setContentTitle(App.app.getString(R.string.guess_word))
                .setContentText(reminder.getGuessWord().toString())
                .setAutoCancel(true)
        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);



        Intent resultIntent = new Intent(context, App.app.getRemindActivityClass());
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
//        stackBuilder.addParentStack(RemindActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);

        //to catch swipe/delete notification event
        Intent delIntent = new Intent(context, NotifyDeleteReceiver.class);
        delIntent.setAction(NotifyDeleteReceiver.ON_DELETE_NOTIFY);

        builder.setDeleteIntent(PendingIntent.getBroadcast(context, 0, delIntent, 0));

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId=1 allows you to start the notification later on.
        mNotificationManager.notify(1, builder.build());
    }

}
