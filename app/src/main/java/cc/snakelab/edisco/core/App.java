package cc.snakelab.edisco.core;

import android.accounts.Account;
import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseHelperListener;
import com.raizlabs.android.dbflow.structure.database.OpenHelper;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.RemindActivity;
import cc.snakelab.edisco.db.DBFlowSeeder;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.AppDatabase;
import cc.snakelab.edisco.orm.CustomFlowSQliteOpenHelper;
import cc.snakelab.edisco.db.DbManager;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPairHistory_Table;
import cc.snakelab.edisco.orm.WordPair_Table;
import cc.snakelab.edisco.orm.WordTripleHistory_Table;
import cc.snakelab.edisco.orm.WordTriple_Table;
import cc.snakelab.edisco.orm.Word_Table;

/**
 * Created by snake on 31.10.16.
 */

public class App extends Application {

    public static final String APPTAG = "";
    final static String TAG = APPTAG+"App";

    private DbManager db;
    public static App app;
    public static AppPreference preference;
    private Alarm alarm;
    private Context mMainActivityContext;
    public boolean mReminderVisible = false;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private RequestQueue mQueue;
    private Word mCurrentWord;
    private Reminder mReminder;
    private Tts mTTS;

    public App() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        preference = new AppPreference(this);

        mRequestQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });

        if (AppHelper.isTest())
            deleteDatabase(AppHelper.getDBFilename()+".db");


        FlowManager.init(new FlowConfig.Builder(this)
                .addDatabaseConfig(
                        new DatabaseConfig.Builder(AppDatabase.class)
                                .openHelper(new DatabaseConfig.OpenHelperCreator() {
                                    @Override
                                    public OpenHelper createHelper(DatabaseDefinition databaseDefinition, DatabaseHelperListener helperListener) {
                                        return new CustomFlowSQliteOpenHelper(databaseDefinition, helperListener);
                                    }
                                })
                                .build())
                .build());


        //populate db if needed
        if (SQLite.selectCountOf().from(Word.class).count() == 0) {
            //if backup is empty then populate
            if (AppHelper.isTest() && SQLite.selectCountOf().from(Word.class).count() == 0) {
                DBFlowSeeder.populate();
                app.toast("DB's populated with hard coded words");
            }
        }

        if (!AppHelper.isTest()) {
            alarm = new Alarm(this);


        }
        Log.d(TAG, "created");
    }


    public void createIndexesIfNeeded() {
        Word_Table.index_wordLangIndex.createIfNotExists();
        Word_Table.index_wordPartIndex.createIfNotExists();
        Word_Table.index_wordTextIndex.createIfNotExists();

        Word_Table.index_wordCreatedAtIndex.createIfNotExists();
        Word_Table.index_wordSyncedIndex.createIfNotExists();
        Word_Table.index_wordTrashedIndex.createIfNotExists();
        Word_Table.index_wordUpdatedAtIndex.createIfNotExists();
        Word_Table.index_wordUuidIndex.createIfNotExists();
        Word_Table.index_wordVersionIndex.createIfNotExists();


        WordPair_Table.index_wordPairOrdWordIdIndex.createIfNotExists();
        WordPair_Table.index_wordTranslWordIdIndex.createIfNotExists();
        WordPair_Table.index_wordPairCreatedAtIndex.createIfNotExists();
        WordPair_Table.index_wordPairSyncedIndex.createIfNotExists();
        WordPair_Table.index_wordPairTrashedIndex.createIfNotExists();
        WordPair_Table.index_wordPairUpdatedAtIndex.createIfNotExists();
        WordPair_Table.index_wordPairUuidIndex.createIfNotExists();
        WordPair_Table.index_wordPairVersionIndex.createIfNotExists();


        WordTriple_Table.index_orgWord1IdIndex.createIfNotExists();
        WordTriple_Table.index_orgWord2IdIndex.createIfNotExists();
        WordTriple_Table.index_orgWord3IdIndex.createIfNotExists();
        WordTriple_Table.index_transWordIdIndex.createIfNotExists();
        WordTriple_Table.index_wordTripleCreatedAtIndex.createIfNotExists();
        WordTriple_Table.index_wordSyncedIndex.createIfNotExists();
        WordTriple_Table.index_wordTrashedIndex.createIfNotExists();
        WordTriple_Table.index_wordTripleUpdatedAtIndex.createIfNotExists();
        WordTriple_Table.index_wordUuidIndex.createIfNotExists();
        WordTriple_Table.index_wordVersionIndex.createIfNotExists();

        WordPairHistory_Table.index_wordPairIdIndex.createIfNotExists();
        WordPairHistory_Table.index_wordPairHistoryCreatedAtIndex.createIfNotExists();
        WordPairHistory_Table.index_wordPairHistoryUpdatedAtIndex.createIfNotExists();

        WordTripleHistory_Table.index_wordTripleHistoryCreatedAtIndex.createIfNotExists();
        WordTripleHistory_Table.index_wordTripleHistoryUpdatedAtIndex.createIfNotExists();
        WordTripleHistory_Table.index_wordTripleIdIndex.createIfNotExists();
    }

    public boolean isReminderVisible() {
        return mReminderVisible;
    }
    public void setReminderVisible(boolean visible) {
        mReminderVisible = visible;
    }
    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getMainActivityContext() {
        return mMainActivityContext;
    }
    public void setMainActivityContext(Context context) {
        mMainActivityContext = context;
    }

    public Class<?> getRemindActivityClass() {
        return RemindActivity.class;
    }
    public ImageLoader getmImageLoader(){
        return mImageLoader;
    }

    public RequestQueue getRequestQueue() {
        if(mQueue == null)
            mQueue = Volley.newRequestQueue(this);
        return mQueue;
    }

    public Word getCurrentWord() {
        return mCurrentWord;
    }
    public void setCurrentWord(Word word) {
        mCurrentWord = word;
    }

    public Reminder getReminder() {
        if(mReminder == null)
            mReminder = Reminder.get();
        return mReminder;
    }

    public void initSpeak(String lang) {
        if (mTTS == null || !mTTS.getLang().equalsIgnoreCase(lang)) {
            if(mTTS != null)
                mTTS.destroy();

            mTTS = new Tts(this, lang);
        }

    }
    public void destroySpeak() {
        if (mTTS != null) {
            mTTS.destroy();
        }
        mTTS = null;
    }
    public void speak(String text, String lang) {
        initSpeak(lang);
        mTTS.speak(text);
    }
}

