package cc.snakelab.edisco.core;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import static android.speech.tts.TextToSpeech.QUEUE_ADD;
import static android.speech.tts.TextToSpeech.QUEUE_FLUSH;

/**
 * Created by snake on 25.10.17.
 */

public class Tts implements TextToSpeech.OnInitListener {
    private TextToSpeech mTTS;
    private String mLang;

    private boolean initialized;
    private Queue<String> queue;

    public Tts(Context context, String lang) {
        mTTS = new TextToSpeech(context, this);
        mLang = lang;
        queue = new ArrayDeque<>();
    }
    public String getLang() {
        return mLang;
    }

    public void destroy() {
        mTTS.shutdown();
    }

    @Override
    public void onInit(int initStatus) {
        initialized = initStatus == TextToSpeech.SUCCESS;
        if (initialized) {
            mTTS.setLanguage(new Locale(mLang));
            while (!queue.isEmpty()) {
                mTTS.speak(queue.poll(), QUEUE_ADD, null);
            }
        }
    }
    public void speak(String text) {
        if (initialized)
            mTTS.speak(text, QUEUE_ADD, null);
        else
            queue.add(text);


    }
}
