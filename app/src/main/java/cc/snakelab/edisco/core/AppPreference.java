package cc.snakelab.edisco.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.util.ArraySet;
import android.support.v7.preference.PreferenceManager;

import com.raizlabs.android.dbflow.StringUtils;

import cc.snakelab.edisco.BuildConfig;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.SettingsFragment;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.LocaleHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by snake on 19.04.17.
 */

public class AppPreference {
    private static final String SORTING_WORD_DIRECTION_ASC = "SORTING_WORD_DIRECTION_ASC";
    private static final String SHOW_WIZARD = "SHOW_WIZARD";
    private static final String PROFILE_PHOTO_URL = "PROFILE_PHOTO_URL";
    private static final String SHOW_MAIN_SHOWCASE = "SHOW_MAIN_SHOWCASE";
    private static final String SHOW_NAVIGATION_SHOWCASE = "SHOW_NAVIGATION_SHOWCASE";
    private static final String LAST_WORD_TIMESTAMP = "LAST_WORD_TIMESTAMP";
    private static final String LAST_WORD_PAIR_TIMESTAMP = "LAST_WORD_PAIR_TIMESTAMP";
    private static final String LAST_WORD_TRIPLE_TIMESTAMP ="LAST_WORD_TRIPLE_TIMESTAMP";
    private static final String SPEAK="SPEAK";

    private SharedPreferences mPreferences;
    private Context mContext;
    private static final String SORTING_WORD_ID = "SORTING_WORD_ID";



    public AppPreference(Context context) {
        mContext = context;
        mPreferences= PreferenceManager.getDefaultSharedPreferences(context);
//        mPreferences = context.getSharedPreferences("preference", Context.MODE_MULTI_PROCESS)
    }

    //must be in russian, english etc format
    // if you need to get language code line en, ru, de, uk LocaleHelper.langCodeToString
    public String getLang() {
        return mPreferences.getString(SettingsFragment.NATIVE_LANG, LocaleHelper.langCodeToString(mContext, LocaleHelper.RU));
    }
    public void setLang(String lang) {
        mPreferences.edit().putString(SettingsFragment.NATIVE_LANG, lang).commit();
    }

    public void setCurrentLearningLang(String lang) {
        mPreferences.edit().putString(SettingsFragment.CURRENT_LEARNING_LANGUAGE, lang).commit();
    }
    public String getCurrentLearningLang() {
        String lang ="";
        String[] langs = getLearningLangs().split(", ");
        if (langs.length > 0)
            lang = langs[0];
        return mPreferences.getString(SettingsFragment.CURRENT_LEARNING_LANGUAGE, lang);
    }

    public String getLearningLangs() {

        StringBuilder sb = new StringBuilder();
        for(String lang: getLearningLangsList()) {
            sb.append(lang+", ");
        }
        if (sb.length() >1 )
            sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    public List<String> getLearningLangsList() {
        ArraySet<String> defaultLangs = new ArraySet<String>();
        defaultLangs.add(LocaleHelper.langCodeToString(App.app, LocaleHelper.EN));
        String[] langs = mPreferences.getStringSet(SettingsFragment.LEARNING_LANGUAGES, defaultLangs).toArray(new String[0]);
        ArrayList<String> ret = new ArrayList<>();
        for (String lang: langs){
            ret.add(lang);
        }
        return ret;
    }
    public void setLearningLangsList(List<String> langsList) {
        mPreferences.edit().putStringSet(SettingsFragment.LEARNING_LANGUAGES, new HashSet<String>(langsList)).commit();
    }
    public void AddLearningLanguage(String lang) {
        List<String> langs = getLearningLangsList();
        if (langs.indexOf(lang) == -1) {
            langs.add(lang);
            mPreferences.edit().putStringSet(SettingsFragment.LEARNING_LANGUAGES, new HashSet<String>(langs)).commit();
        }

    }
    public String getApiServerUrl() {
//        return "http://egg.snakelab.cc:3000/";
//
        if (AppHelper.isTest()) {
            if (StringUtils.isNullOrEmpty(BuildConfig.EDISCO_TEST_HOST))
                return "https://edisco.snakelab.cc";
            else
                return BuildConfig.EDISCO_TEST_HOST;
        } else

//            return BuildConfig.DEBUG ?  "http://egg.snakelab.cc:3000" : mPreferences.getString(SettingsFragment.SERVER, mContext.getString(R.string.main_server));
        return mPreferences.getString(SettingsFragment.SERVER, mContext.getString(R.string.main_server));

    }


    public boolean isSingedIn() {
        return getApiToken() != null;
//        return false;
    }

    public void setApiToken(String token) {
        mPreferences.edit().putString("api_token", token).commit();
    }
    public String getApiToken() {
        if (AppHelper.isTest())
            return BuildConfig.EDISCO_TEST_TOKEN;
        else
            return mPreferences.getString("api_token", null);
    }


    public void setEmail(String email) {
        mPreferences.edit().putString("email", email).commit();
    }

    public String getEmail() {
        if (AppHelper.isTest())
            return "test@edisco.snakelab.cc";
        else
            return mPreferences.getString("email", null);
    }
    public boolean isSortingDesc() {
        return mPreferences.getBoolean(SORTING_WORD_DIRECTION_ASC, true);
    }
    public void setSortingDesc(boolean b) {
        mPreferences.edit().putBoolean(SORTING_WORD_DIRECTION_ASC, b).commit();
    }
    public int getSortingTypeId() {
        return mPreferences.getInt(SORTING_WORD_ID, R.id.sort_by_date);
    }
    public void setSortingTypeId(int id) {
        mPreferences.edit().putInt(SORTING_WORD_ID, id).commit();
    }

    public void setNewUser(boolean newUser) {
        if (newUser) {
            mPreferences.edit().putBoolean(SHOW_WIZARD, true).commit();
            mPreferences.edit().putBoolean(SHOW_MAIN_SHOWCASE, true).commit();
            mPreferences.edit().putBoolean(SHOW_NAVIGATION_SHOWCASE, true).commit();
        }
    }

    public boolean isShowWizard(){
        return mPreferences.getBoolean(SHOW_WIZARD, false);
    }

    public void setShowWizard(boolean b) {
        mPreferences.edit().putBoolean(SHOW_WIZARD, b).commit();
    }

    public boolean isShowMainShowcase(){
        return mPreferences.getBoolean(SHOW_MAIN_SHOWCASE, false);
    }


    public boolean isShowNavigationShowcase() {
        return mPreferences.getBoolean(SHOW_NAVIGATION_SHOWCASE, false);
    }
    public void setShowMainShowcase(boolean b) {
        mPreferences.edit().putBoolean(SHOW_MAIN_SHOWCASE, b).commit();
    }


    public void setShowNavigationShowcase(boolean b) {
        mPreferences.edit().putBoolean(SHOW_NAVIGATION_SHOWCASE, b).commit();
    }

    public void setProfilePhotoUrl(String url) {
        mPreferences.edit().putString(PROFILE_PHOTO_URL, url).commit();

    }
    public String getProfilePhotoUrl() {
        return mPreferences.getString(PROFILE_PHOTO_URL, null);
    }

    public Date getLastWordTimestamp() {
        try {
            return DateHelper.iso8601ZToDate(mPreferences.getString(LAST_WORD_TIMESTAMP, "1980-01-01 00:00:00.000 +0000"));
        } catch (ParseException e) {
            return new Date(0);
        }
    }
    public void setLastWordTimestamp(Date date) {
        mPreferences.edit().putString(LAST_WORD_TIMESTAMP, DateHelper.dateToIso8601Z(date)).commit();
    }

    public Date getLastWordPairTimestamp() {
        try {
            return DateHelper.iso8601ZToDate(mPreferences.getString(LAST_WORD_PAIR_TIMESTAMP, "1980-01-01 00:00:00.000 +0000"));
        } catch (ParseException e) {
            return new Date(0);
        }
    }
    public void setLastWordPairTimestamp(Date date) {
        mPreferences.edit().putString(LAST_WORD_PAIR_TIMESTAMP, DateHelper.dateToIso8601Z(date)).commit();
    }

    public Date getLastWordTripleTimestamp() {
        try {
            return DateHelper.iso8601ZToDate(mPreferences.getString(LAST_WORD_TRIPLE_TIMESTAMP, "1980-01-01 00:00:00.000 +0000"));
        } catch (ParseException e) {
            return new Date(0);
        }
    }
    public void setLastWordTripleTimestamp(Date date) {
        mPreferences.edit().putString(LAST_WORD_TRIPLE_TIMESTAMP, DateHelper.dateToIso8601Z(date)).commit();
    }

    public void setSpeakEnabled(boolean enabled) {
        mPreferences.edit().putBoolean(SPEAK, enabled).commit();
    }

    public boolean isSpeakEnabled() {
        return mPreferences.getBoolean(SPEAK, true);
    }
}

