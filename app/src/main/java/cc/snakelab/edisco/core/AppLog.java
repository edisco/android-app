package cc.snakelab.edisco.core;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import cc.snakelab.edisco.BuildConfig;

/**
 * Created by snake on 03.01.17.
 */

public class AppLog {
    public static void e(String tag, String msg, Throwable e) {
        log(Log.ERROR, tag, msg, e);
    }
    private static void log(int i, String tag, String msg, Throwable e) {
        if (!BuildConfig.DEBUG &&  i != Log.ERROR)
            return;
        try {

            FirebaseCrash.logcat(i, tag, msg);
            if (!BuildConfig.DEBUG)
                FirebaseCrash.report(new Exception(msg));

            if (e != null) {
                FirebaseCrash.logcat(i, tag, e.getMessage());
                FirebaseCrash.logcat(i, tag, Log.getStackTraceString(e));
                if (!BuildConfig.DEBUG)
                    FirebaseCrash.report(e);
            }
        }catch (Exception ex) {
            Log.e(tag, "exception: " + ex.getMessage().toString() );
        }
    }
    public static void e(String tag, String msg) {
        e(tag, msg, null);

    }
    public static void d(String tag, String msg) {
        log(Log.DEBUG, tag, msg, null);
    }
    public static void d(String tag, String formatMsg, Object... args) {
        log(Log.DEBUG, tag, String.format(formatMsg, args), null);
    }

}
