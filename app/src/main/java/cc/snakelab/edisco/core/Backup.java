package cc.snakelab.edisco.core;

import android.content.Context;

import com.raizlabs.android.dbflow.list.FlowCursorList;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPair_Table;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTriple_Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

/**
 * Created by snake on 27.01.17.
 */

public class Backup {

    private static final String FILENAME = "backup.json";

    public static void save() throws Exception {
        JSONObject jo = new JSONObject();
        savePairs(jo);
        saveTriple(jo);
        saveReminder(jo);

        FileOutputStream fos = App.app.openFileOutput(FILENAME, Context.MODE_PRIVATE);
        fos.write(jo.toString().getBytes());
        fos.close();
    }

    private static void saveReminder(JSONObject jsonObject) throws JSONException {
        Reminder r = Reminder.get();
        jsonObject.put("reminder", r.saveToJSON());
    }

    private static void loadReminder(JSONObject jsonObject) throws JSONException {
        Reminder r = Reminder.loadFromJSON(jsonObject.getJSONObject("reminder"));
        r.save();
    }

    private static void savePairs(JSONObject jsonObject) throws Exception {
        FlowCursorList<WordPair> list = SQLite.select()
                .from(WordPair.class).where(WordPair_Table.trashed.eq(false))
                .cursorList();

        try {
            JSONArray ja = new JSONArray();
            for (WordPair wp : list) {
                ja.put(wp.saveToJSON());
            }
            jsonObject.put("pairs", ja);
        } finally {
            list.close();
        }
    }
    private static void saveTriple(JSONObject jsonObject) throws JSONException {
        FlowCursorList<WordTriple> list = SQLite.select()
                .from(WordTriple.class).where(WordTriple_Table.trashed.eq(false))
                .cursorList();
        try
        {
            JSONArray ja = new JSONArray();
            for (WordTriple wt: list) {
                JSONObject jo = new JSONObject();
                jo.put("orgWord1", wt.getOrgWord1().getText());
                jo.put("orgWord2", wt.getOrgWord2().getText());
                jo.put("orgWord3", wt.getOrgWord3().getText());
                jo.put("translWord", wt.getTranslWord().getText());
                jo.put("orgLang", wt.getOrgWord1().getLang());
                jo.put("translLang", wt.getTranslWord().getLang());
                ja.put(jo);


            }
            jsonObject.put("triples", ja);

        }
        finally {
            list.close();
        }
    }

    public static void load() throws Exception {
        FileInputStream fis = App.app.openFileInput(FILENAME);
        InputStreamReader isr = new InputStreamReader(fis, "utf8");
        BufferedReader br = new BufferedReader(isr);

        String txt = "";
        String line = br.readLine();
        while( line != null ) {
            txt += line;
            line = br.readLine();
        }
        JSONObject jo = new JSONObject(txt);
        loadPairs(jo);
        loadTriples(jo);
        SQLite.select().from(Word.class).queryList();
        loadReminder(jo);

    }

    private static void loadTriples(JSONObject jsonObject) throws JSONException {
        JSONArray ja = jsonObject.getJSONArray("triples");
        for(int i = 0; i < ja.length(); i++) {
            JSONObject jo = ja.getJSONObject(i);
            String orgWord1 = jo.optString("orgWord1");
            String orgWord2 = jo.optString("orgWord2");
            String orgWord3 = jo.optString("orgWord3");
            String translWord = jo.optString("translWord");
            String orgLang = jo.optString("orgLang");
            String translLang = jo.optString("translLang");
            WordTriple wt = new WordTriple(orgWord1, orgWord2,orgWord3, translWord, orgLang, translLang);
            wt.save();


        }
    }

    private static void loadPairs(JSONObject jsonObject) throws JSONException {
        JSONArray ja = jsonObject.getJSONArray("pairs");
        for(int i = 0; i < ja.length(); i++) {
            JSONObject jo = ja.getJSONObject(i);
            WordPair wp = WordPair.loadFromJSON(jo);
            wp.save();
        }

    }
}
