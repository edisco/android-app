package cc.snakelab.edisco.core;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.Word_Table;

/**
 * Created by snake on 28.03.17.
 */

public class WordMerger extends BasicRecordMerger {
    @Override
    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        Word word1 = (Word) record1;
        Word word2 = (Word) record2;

        return
                word1.getGender() == word2.getGender() &&
                word1.getText().equals(word2.getText()) &&
                word1.getLang().equals(word2.getLang()) &&
                word1.getPart() == word2.getPart() &&
                        super.isRecordSame(record1, record2);
    }

    @Override
    protected BasicRecord findRecordByUuid(String uuid) {
        return Word.findByUuid(Word.class, uuid);
    }


    @Override
    protected BasicRecord selectSameRecord(BasicRecord record) {
        Word word = (Word) record;
        return SQLite.select().from(Word.class).where(Word_Table.text.eq(word.getText()))
                .and(Word_Table.lang.eq(word.getLang()))
                .and(Word_Table.gender.eq(word.getGender()))
                .and(Word_Table.trashed.eq(word.isTrashed()))
                .and(Word_Table.part.eq(word.getPart())).querySingle();

    }


}

