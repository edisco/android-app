package cc.snakelab.edisco.core;


import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordPairHistory_Table;

import java.util.List;

/**
 * Created by snake on 11.04.17.
 */

public class WordPairHistoryMerger extends BasicRecordMerger {
    private static final String TAG = "WordPairHistoryMerger";

    @Override
    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        WordPairHistory wph1 = (WordPairHistory) record1;
        WordPairHistory wph2 = (WordPairHistory) record2;
        return wph1.getWord_pair_id() == wph2.getWord_pair_id() &&
                wph1.isCorrect() == wph2.isCorrect() &&
                super.isRecordSame(record1, record2);
    }

    @Override
    protected BasicRecord findRecordByUuid(String uuid) {
        return WordPairHistory.findByUuid(WordPairHistory.class, uuid);
    }

    @Override
    protected BasicRecord selectSameRecord(BasicRecord record) {
        WordPairHistory wph = (WordPairHistory) record;
        List<WordPairHistory> list = SQLite.select().from(WordPairHistory.class)
                .where(WordPairHistory_Table.correct.eq(wph.isCorrect()))
                .and(WordPairHistory_Table.word_pair_id.eq(wph.getWord_pair_id()))
                .and(WordPairHistory_Table.created_at.eq(wph.getCreated_at())).queryList();
        if(list.size() > 1)
            AppLog.d(TAG, "found a few same records: " + list.size());

        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }
}
