package cc.snakelab.edisco.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import cc.snakelab.edisco.recievers.AlarmReceiver;

/**
 * Created by snake on 01.12.16.
 */

public class Alarm {
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public Alarm(Context context) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(context, 0, new Intent(context, AlarmReceiver.class), 0);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() - AlarmManager.INTERVAL_HALF_HOUR,
                getInterval(), alarmIntent);
    }
    private long getInterval() {
        return 5*60*1000L;
//        return (long) 1.5*3600*1000;
    }
}
