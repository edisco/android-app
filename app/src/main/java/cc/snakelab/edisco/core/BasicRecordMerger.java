package cc.snakelab.edisco.core;

import com.raizlabs.android.dbflow.StringUtils;

import cc.snakelab.edisco.orm.BasicRecord;

/**
 * Created by snake on 04.04.17.
 */

public class BasicRecordMerger {

    private static final String TAG = "BasicRecordMerger";

    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        return record1.isTrashed() == record2.isTrashed() &&
                record1.getCreated_at().equals(record2.getCreated_at())
                ;
    }

    protected BasicRecord findRecordByUuid(String uuid) {
        return null;
    }
    protected BasicRecord selectSameRecord(BasicRecord record) {
        return null;
    }
    public BasicRecord merge(BasicRecord record) {
        BasicRecord targetRecord = null;
        AppLog.d(TAG, "merge "+ record.getClass().getName());
        AppLog.d(TAG, "record_raw " + record.toString());
        AppLog.d(TAG, "record=" + record.toJSON().toString());
        if (StringUtils.isNotNullOrEmpty(record.getUuid())) {
            targetRecord = findRecordByUuid(record.getUuid());
            if (targetRecord == null) {
                AppLog.d(TAG, "unable to find record by uuid. Finding by fields");
                //create scenario
                targetRecord = selectSameRecord(record);
                if (targetRecord != null)
                    AppLog.d(TAG, "targetRecord="+targetRecord.toJSON().toString());
                else
                    AppLog.d(TAG, "targetRecrod=null");
            } else
                AppLog.d(TAG, "found record by uuid");

        }

        if (targetRecord == null) {
            AppLog.d(TAG, "seems record is absolutly new. Adding it to db");
            targetRecord = record;
            targetRecord.setSynced(true);
        }
        else {

            if (targetRecord.getVersion() < record.getVersion()) {
                AppLog.d(TAG, "record is newer than in db");
                long id = targetRecord.getId();
                targetRecord.assign(record);
                targetRecord.setId(id);
                targetRecord.setSynced(false);
            }

            //created
            if (StringUtils.isNullOrEmpty(targetRecord.getUuid()) &&
                    StringUtils.isNotNullOrEmpty(record.getUuid())) {
                targetRecord.setUuid(record.getUuid());
                AppLog.d(TAG, "after create method. set uuid to traget record");
            }

            if (targetRecord.getVersion() == record.getVersion()) {
                if (!isRecordSame(targetRecord, record)) {
                    AppLog.d(TAG, "versions are same but data different. Bumb version");
                    targetRecord.setVersion(targetRecord.getVersion() + 1);
                    targetRecord.setSynced(false);
                } else {
                    //could happen when cloud wordPair and target wordPair are same, except uuid
                    AppLog.d(TAG, "versions are equal and data is euqal");
                    targetRecord.setUuid(record.getUuid());
                    targetRecord.setSynced(true);
                }

            } else
                AppLog.d(TAG, "version is less. Ignored");
        }

        if (targetRecord != null)
            targetRecord.save();
        return targetRecord;
    }
}
