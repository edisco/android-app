package cc.snakelab.edisco.core;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTriple_Table;

import java.util.List;

/**
 * Created by snake on 10.04.17.
 */

public class WordTripleMerger extends BasicRecordMerger{
    private static final String TAG = "WordTripleMerger";

    @Override
    protected boolean isRecordSame(BasicRecord record1, BasicRecord record2) {
        WordTriple wt1 = (WordTriple) record1;
        WordTriple wt2 = (WordTriple) record2;

        return wt1.getOrgWord1Id() == wt2.getOrgWord1Id() &&
                wt1.getOrgWord2Id() == wt2.getOrgWord2Id() &&
                wt1.getOrgWord3Id() == wt2.getOrgWord3Id() &&
                wt1.getTransWordId() == wt2.getTransWordId() &&
                wt1.getStage() == wt2.getStage() &&
                super.isRecordSame(record1, record2);

    }

    @Override
    protected BasicRecord findRecordByUuid(String uuid) {
        return WordTriple.findByUuid(WordTriple.class, uuid);
    }

    @Override
    protected BasicRecord selectSameRecord(BasicRecord record) {
        WordTriple wt = (WordTriple) record;
        List<WordTriple> list =
                SQLite.select().from(WordTriple.class)
                        .where(WordTriple_Table.orgWord1Id.eq(wt.getOrgWord1Id()))
                .and(WordTriple_Table.orgWord2Id.eq(wt.getOrgWord2Id()))
                .and(WordTriple_Table.orgWord3Id.eq(wt.getOrgWord3Id()))
                .and(WordTriple_Table.transWordId.eq(wt.getTransWordId()))
                .and(WordTriple_Table.trashed.eq(wt.isTrashed()))
                .and(WordTriple_Table.stage.eq(wt.getStage())).queryList();
        if(list.size() > 1)
            AppLog.d(TAG, "found a few same records: " + list.size());

        if (list.size() > 0)
            return list.get(0);
        else
            return null;

    }
}
