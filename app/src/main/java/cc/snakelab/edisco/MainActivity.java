package cc.snakelab.edisco;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.github.amlcurran.showcaseview.*;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Timer;
import java.util.TimerTask;

import cc.snakelab.edisco.adapters.RecycleAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.core.EndlessRecyclerViewScrollListener;

import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.helper.OnStartDragListener;
import cc.snakelab.edisco.helper.SimpleItemTouchHelperCallback;
import cc.snakelab.edisco.holders.WordsViewHolder;
import cc.snakelab.edisco.intro.IntroWizardNG;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.recievers.SyncReceiver;
import cc.snakelab.edisco.services.ReminderService;
import cc.snakelab.edisco.showcase.RectangleShowcaseView;

import static cc.snakelab.edisco.AddWordActivity.MAIN_SEARCH_TEXT;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener, OnStartDragListener, SwipeRefreshLayout.OnRefreshListener, SyncReceiver.OnSyncDoneListener, RecycleAdapter.OnAdapterChangeListener {
    public static final int ADD_WORDPAIR_REQUEST = 1;

    private static final String TAG = "MainActivity";
    public static final String START_SYNC = "START_SYNC";

    private SearchView searchView;
    private MenuItem searchMenuItem;
    private RecyclerView mRecyclerView;
    private ItemTouchHelper mItemTouchHelper;
    private RecycleAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private NetworkImageView mProfilePhoto;
    private TextView mUserName;
    private android.support.v4.widget.DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;

    private EditText mSearch;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SyncReceiver mSyncReceiver;
    private static final String FIRST_START = "com.snakelab.edisco";
    private Timer mDelayTimer;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_WORDPAIR_REQUEST) {
            if (resultCode == RESULT_OK) {
                mSearch.setText("");
                mAdapter.filter("");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(cc.snakelab.edisco.R.style.AppTheme);// set default theme after splash
        super.onCreate(savedInstanceState);

        mDelayTimer = new Timer();
        App.app.setMainActivityContext(this);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(cc.snakelab.edisco.R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(cc.snakelab.edisco.R.id.toolbar);
        setSupportActionBar(mToolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(cc.snakelab.edisco.R.id.fab_add_new_word);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddWordActivity.class);
                intent.putExtra(MAIN_SEARCH_TEXT, mSearch.getText().toString());
                startActivityForResult(intent, ADD_WORDPAIR_REQUEST);
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(cc.snakelab.edisco.R.id.drawer_layout);


        mNavigationView = (NavigationView) findViewById(cc.snakelab.edisco.R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mRefreshLayout = (SwipeRefreshLayout) findViewById(cc.snakelab.edisco.R.id.refreshLayout);
//disabled, please do not enable this
        mRefreshLayout.setEnabled(false);
        mRefreshLayout.setOnRefreshListener(this);

        mRecyclerView = (RecyclerView) findViewById(cc.snakelab.edisco.R.id.wordlist);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new RecycleAdapter(this);
        mRecyclerView.setAdapter(mAdapter);


        //divider
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page, final int totalItemsCount, RecyclerView view) {

                if (!App.preference.isSingedIn())
                    return;

                AppLog.d(TAG, String.format("page=%d, total=%d", page, totalItemsCount));
                mAdapter.fetchNextPage();

            }
        };
        mRecyclerView.addOnScrollListener(scrollListener);
        mAdapter.setOnScrollListener(scrollListener);

        //darg and remove

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);






        mSearch = (EditText) findViewById(cc.snakelab.edisco.R.id.editSearch);

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                if (!App.preference.isSingedIn())
                    return;

                mDelayTimer.cancel();
                mDelayTimer = new Timer();
                mDelayTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.filter(charSequence.toString());
                            }
                        });

                    }
                }, 900);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        startService(new Intent(this, ReminderService.class));


        View header = mNavigationView.getHeaderView(0);
        mProfilePhoto = (NetworkImageView) header.findViewById(cc.snakelab.edisco.R.id.profile_photo);
        mUserName = (TextView) header.findViewById(cc.snakelab.edisco.R.id.user_name);

//        App.preference.setShowMainShowcase(true);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(!isFinishing()) {
            App.app.setMainActivityContext(null);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, cc.snakelab.edisco.R.string.navigation_drawer_open, cc.snakelab.edisco.R.string.navigation_drawer_close);
        toggle.syncState();

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppHelper.hideSoftInput(MainActivity.this);

                mDrawerLayout.openDrawer((Gravity.LEFT));
                if (App.preference.isShowNavigationShowcase()) {
//                    App.preference.setShowNavigationShowcase(false);
//                    showShowcase3();
                }
//                toggleListener.onClick(view);
            }
        });
    }


    private void checkIfSignedIn() {
        if (AppHelper.isTest())
            return;

        if (AppHelper.isNetworkAvailable() && !App.preference.isSingedIn()) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkIfSignedIn();
        if (!App.preference.isSingedIn())
            return;


        //register broadcast
        IntentFilter filter = new IntentFilter(SyncReceiver.ON_SYNC_DONE_ACTION);
        filter.addAction(SyncReceiver.ON_SYNC_START_ACTION);
        filter.addAction(SyncReceiver.ON_SYNC_ERROR_ACTION);
        mSyncReceiver = new SyncReceiver(this);

        this.registerReceiver(mSyncReceiver, filter);

        if ( App.preference.isShowWizard()) {
            App.preference.setShowWizard(false);
            showIntroWizard();
            return;
        }
        if(!AppHelper.isTest()) {
            AppLog.d(TAG, "addPeriodicSync");
            Account account = AppHelper.createSyncAccount(this);
            ContentResolver.addPeriodicSync(
                    account,
                    getString(R.string.authorityProvider),
                    Bundle.EMPTY,
                    12 * 60 * 60);//update each 12 hours
            ContentResolver.setSyncAutomatically(account, getString(R.string.authorityProvider), true);
        }

        if (mProfilePhoto != null)
            mProfilePhoto.setImageUrl(App.preference.getProfilePhotoUrl(), App.app.getmImageLoader());
        if (mUserName != null)
            mUserName.setText(App.preference.getEmail());

        invalidateOptionsMenu();


        if(!mAdapter.equals(null)) {
            mAdapter.refresh();
        }


        if (getIntent().getBooleanExtra(START_SYNC, false)) {
            getIntent().putExtra(START_SYNC, false);
            onRefresh();
        }



    }

    @Override
    protected void onPause() {
        if (mSyncReceiver != null)
            this.unregisterReceiver(mSyncReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
//            finish();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(cc.snakelab.edisco.R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //change visible state of lang icon
        menu.findItem(cc.snakelab.edisco.R.id.action_lang_change).setVisible(App.preference.getLearningLangsList().size() > 1);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        PopupMenu popMenu;
        Menu menu;
        switch (id) {
            case cc.snakelab.edisco.R.id.action_lang_change:
                popMenu = new PopupMenu(this, findViewById(cc.snakelab.edisco.R.id.action_lang_change));
                menu = popMenu.getMenu();
                int menuId = 0;
                for (String lang : App.preference.getLearningLangsList()) {
                    MenuItem mi = menu.add(0, menuId, 0, lang);
                    mi.setCheckable(true);
                    mi.setChecked(App.preference.getCurrentLearningLang().equalsIgnoreCase(lang));
                    menuId++;
                }
                menu.setGroupCheckable(0, true, true);
                popMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        App.preference.setCurrentLearningLang(item.getTitle().toString());
                        mAdapter.refresh();
                        return false;
                    }
                });
                popMenu.show();
                return true;

            case cc.snakelab.edisco.R.id.action_sort_words:
                popMenu = new PopupMenu(this, findViewById(cc.snakelab.edisco.R.id.action_sort_words));
                popMenu.inflate(cc.snakelab.edisco.R.menu.menu_sorting);
                menu = popMenu.getMenu();
                menu.setGroupCheckable(0, true, true);
                final int sortId = App.preference.getSortingTypeId();
                MenuItem mi = menu.findItem(sortId);
                if (mi != null)
                    mi.setChecked(true);
                popMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        App.preference.setSortingTypeId(item.getItemId());
                        mAdapter.refresh();
                        return false;
                    }
                });


                popMenu.show();
                return true;

            case cc.snakelab.edisco.R.id.action_sort_direction:

                App.preference.setSortingDesc(!App.preference.isSortingDesc());
                mAdapter.refresh();
                return true;
            case R.id.action_sync:
                onRefresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.filter(newText);
        return false;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case cc.snakelab.edisco.R.id.nav_learn:{
                Intent intent = new Intent(getApplicationContext(), App.app.getRemindActivityClass());
                startActivity(intent);
                return true;
            }
            case cc.snakelab.edisco.R.id.nav_remind_setup: {
                Intent intent = new Intent(getApplicationContext(), RemindSetupActivity.class);
                startActivity(intent);
                return true;
            }
            case cc.snakelab.edisco.R.id.nav_settings: {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
            }
            case cc.snakelab.edisco.R.id.nav_archive: {
                Intent intent = new Intent(getApplicationContext(), TrashActivity.class);
                startActivity(intent);
                return true;
            }
            case cc.snakelab.edisco.R.id.nav_log_out: {
                App.preference.setApiToken(null);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
            default:
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(cc.snakelab.edisco.R.id.drawer_layout);
//        drawer.
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    private void showIntroWizard() {
        Intent i = new Intent(MainActivity.this, IntroWizardNG.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
    }

    @Override
    public void onRefresh() {
        if (AppHelper.isNetworkAvailable())
            AppHelper.sync(this);
        else {
            mRefreshLayout.setRefreshing(false);
            App.app.toast(getString(cc.snakelab.edisco.R.string.no_internet_connection));
        }
    }

    @Override
    public void onSyncDone() {
        mRefreshLayout.setRefreshing(false);
        mAdapter.refresh();
    }

    @Override
    public void onSyncStart() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });

    }

    @Override
    public void onSyncError(String msg) {
        mRefreshLayout.setRefreshing(false);
        App.app.toast(msg);
    }

    @Override
    public void onSyncProgress(Bundle params) {

    }

    private void showShowcase1() {
        RecyclerView.ViewHolder hold = mRecyclerView.findViewHolderForAdapterPosition(0);


        if (hold == null || !(hold instanceof WordsViewHolder) || ((WordsViewHolder) hold).mWordHead == null) {
            showShowcase4();
            return;
        }

        new ShowcaseView.Builder(this).setTarget(new ViewTarget(hold.itemView))
                .setContentTitle(getString(cc.snakelab.edisco.R.string.showcase_word_title))
                .setContentText(getString(cc.snakelab.edisco.R.string.showcase_words_text))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .setShowcaseDrawer(new RectangleShowcaseView(hold.itemView))
                .setStyle(cc.snakelab.edisco.R.style.ShowcaseTheme)
                .setShowcaseEventListener(new SimpleShowcaseEventListener(){
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showShowcase2();
                    }

                })
                .build();
    }
    private void showShowcase2() {
        WordsViewHolder holder = (WordsViewHolder) mRecyclerView.findViewHolderForAdapterPosition(0);
        if (holder == null || holder.mWordHead == null)
            return;
        new ShowcaseView.Builder(this).setTarget(new ViewTarget(holder.mPercents))
                .setContentTitle(getString(cc.snakelab.edisco.R.string.showcase_percent_title))
                .setContentText(getString(cc.snakelab.edisco.R.string.showcase_percent_text))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .setStyle(cc.snakelab.edisco.R.style.ShowcaseTheme)
                .setShowcaseEventListener(new SimpleShowcaseEventListener(){
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showShowcase3();
                    }

                })
                .build();
    }

    private void showShowcase3() {
        final WordsViewHolder holder = (WordsViewHolder) mRecyclerView.findViewHolderForAdapterPosition(0);
        if (holder == null || holder.mIcon == null) {
            return;
        }

        WordsViewHolder newHolder = (WordsViewHolder) mRecyclerView.findViewHolderForAdapterPosition(0);
        ShowcaseView view = new ShowcaseView.Builder(MainActivity.this).setTarget(new ViewTarget(newHolder.mIcon))
                .setContentTitle(getString(cc.snakelab.edisco.R.string.showcase_word_reminder_icon))
                .setContentText(getString(R.string.showcase_word_long_tap))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .setStyle(cc.snakelab.edisco.R.style.ShowcaseTheme)

                .build();
        view.requestFocus();

    }



    private void showShowcase3b() {
        NavigationMenuView nmv = (NavigationMenuView) mNavigationView.getChildAt(0);

        MenuItem mi = mNavigationView.getMenu().findItem(cc.snakelab.edisco.R.id.nav_learn);

        View view = nmv.getChildAt(1);
        new ShowcaseView.Builder(this).setTarget(new ViewTarget(view))
                .setContentTitle(getString(cc.snakelab.edisco.R.string.showcase_nav_learn_title))
                .setContentText(getString(cc.snakelab.edisco.R.string.showcase_nav_learn_text))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .setShowcaseDrawer(new RectangleShowcaseView(view))
                .setStyle(cc.snakelab.edisco.R.style.ShowcaseTheme)
                .build();
    }
    private void showShowcase4() {

    }

    @Override
    public void onDataChaned() {
        if (App.preference.isShowMainShowcase()) {
            App.preference.setShowMainShowcase(false);




            AppHelper.hideSoftInput(this);
            mRecyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    WordsViewHolder holder = (WordsViewHolder) mRecyclerView.findViewHolderForAdapterPosition(0);
                    WordsViewHolder holder2 = (WordsViewHolder) mRecyclerView.findViewHolderForAdapterPosition(1);
                    Reminder reminder = mAdapter.getReminder();
                    if (holder != null && holder.mWordHead != null && holder.mWordContainer != null)
                        holder.mWordContainer.onLongClick(reminder, holder.mWordHead, mAdapter);

                    if (holder2 != null && holder.mWordHead != null && holder2.mWordContainer != null) {
                        holder2.mWordContainer.onLongClick(reminder, holder2.mWordHead, mAdapter);
                    }

                }
            }, 200);

            mRecyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showShowcase1();
                }
            }, 300);
        }
    }
}
