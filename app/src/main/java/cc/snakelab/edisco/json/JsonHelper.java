package cc.snakelab.edisco.json;


import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

import cc.snakelab.edisco.orm.Word;

/**
 * Created by shen on 02.10.2016.
 */
public class JsonHelper {

    private ObjectMapper objectMapper = null;
    private JsonFactory jsonFactory = null;
    private JsonParser jp = null;

    public JsonHelper()
    {
        objectMapper = new ObjectMapper();
        jsonFactory = new JsonFactory();
    }

    public ArrayList<Word> getAllWords(String json)
    {
        ArrayList<Word> temp = new ArrayList<Word>();
        try
        {
            jp = jsonFactory.createJsonParser(json);
            Word word = objectMapper.readValue(jp, Word.class);
            temp.add(word);
            return temp;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp;
    }


}
