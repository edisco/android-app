package cc.snakelab.edisco.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.holders.RecordHolder;
import cc.snakelab.edisco.orm.BasicRecord;
import java.util.List;

/**
 * Created by snake on 12.04.17.
 */

public class RecordViewAdapter extends RecyclerView.Adapter<RecordHolder> {
    Class mKlass;
    List<BasicRecord> mList;
    public RecordViewAdapter() {
    }
    @Override
    public RecordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.record_item, parent, false);

        return new RecordHolder(v);
    }

    @Override
    public void onBindViewHolder(RecordHolder holder, int position) {
            BasicRecord record =mList.get(position);
            holder.mText.setText(
                    String.format("id=%d uuid=%s %s",
                            record.getId(),
                            record.getUuid(),
                        record.toJSON().toString()
                    ));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public void update(Class klass) {
        mKlass = klass;
        mList = SQLite.select().from(mKlass).queryList();
        notifyDataSetChanged();
    }
}
