package cc.snakelab.edisco.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.holders.TranslationViewHolder;
import cc.snakelab.edisco.orm.Word;

/**
 * Created by snake on 31.01.17.
 */

public class TranslationViewAdapter extends RecyclerView.Adapter<TranslationViewHolder> {
    private Word mWord;
    public TranslationViewAdapter(Word word) {
        mWord = word;
    }

    @Override
    public TranslationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.translation_view_word_item, parent, false);

        TranslationViewHolder  vh = new TranslationViewHolder(v);

        return vh;

    }

    @Override
    public void onBindViewHolder(TranslationViewHolder holder, int position) {
        Word traslWord = mWord.getPairTranslations().get(position);
//        holder.mPart.setText(traslWord.getPart());
        holder.mTranslation.setText(String.format("%s (%s)", traslWord.getText(), traslWord.getLang()));



    }

    @Override
    public int getItemCount() {
        return mWord.getPairTranslations().size();
    }
}
