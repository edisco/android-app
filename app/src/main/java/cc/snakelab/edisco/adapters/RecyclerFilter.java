package cc.snakelab.edisco.adapters;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import com.android.volley.VolleyError;

import org.json.JSONException;

import cc.snakelab.edisco.api.Api;
import cc.snakelab.edisco.api.EdiscoApi;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.FilterHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.ui.BasicWordUIContainer;

import static cc.snakelab.edisco.api.EdiscoApi.TRANSLATION_SUGGESTIONS;

/**
 * Created by snake on 05.11.16.
 */

public class RecyclerFilter extends Filter implements Api.ApiResultListener {
    private static final String TAG = "RecyclerFilter";
    ResultReceiver mAdapter;

    private EdiscoApi mApi;
    private int mOffset;
    private int mLimit;
    private String mCurrentQuery;
    private boolean mFiltering;

    public RecyclerFilter(ResultReceiver adapter) {
        super();
        mApi = new EdiscoApi(this,  App.preference.getApiServerUrl(), App.preference.getEmail(), App.preference.getApiToken());
        mAdapter = adapter;
        mOffset = 0;
        mLimit = 20;
        mCurrentQuery = "";
        mFiltering = false;



    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        mAdapter.startProcess();
        mFiltering = true;
        mCurrentQuery = constraint.toString();
        AppLog.d(TAG, "filter '" + mCurrentQuery + "'");

        mOffset = 0;
        List<BasicWordUIContainer> list = new ArrayList<>();
        mOffset = FilterHelper.fetchContainers(list, mCurrentQuery, mOffset, mLimit);

        //suggestion
        if (mCurrentQuery.length() >= 3)
            try {
                mAdapter.startProcess();
                mApi.translationSuggestion(mCurrentQuery,
                        LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang()),
                        LocaleHelper.stringToCodeLang(App.app, App.preference.getLang()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        FilterResults results = new FilterResults();
        results.values = list;
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        AppLog.d(TAG, "publishResults");
        List<BasicWordUIContainer> list = (List<BasicWordUIContainer>) results.values;

        mAdapter.onResult(list);
        mFiltering = false;
    }



    public void fetchNextPage() {
        if(mFiltering)
            return;
        AppLog.d(TAG, "fetchNextPage");
        mAdapter.startProcess();
        Thread thread = new Thread() {

            @Override
            public void run() {
                AppLog.d(TAG, "fetching");
                List<BasicWordUIContainer> list = new ArrayList<>();
                mOffset = FilterHelper.fetchContainers(list, mCurrentQuery, mOffset, mLimit);

                mAdapter.onResult(list);
            }
        };
        thread.start();


    }


    public void refresh() {
        filter(mCurrentQuery);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Api.ApiResult response) {
        List<BasicWordUIContainer> list = new ArrayList<>();
        try {
            if (response.isOk() && response.getMethodId() == TRANSLATION_SUGGESTIONS) {

                for (int i = 0 ; i <  response.getData().length(); i++) {
                    String backText=null, frontText=null;
                    try {
                        frontText = response.getData().getJSONObject(i).optJSONObject("attributes").optString("front_text");
                        backText = response.getData().getJSONObject(i).optJSONObject("attributes").optString("back_text");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    FilterHelper.addSuggestionIfNeeded(list, frontText, backText);
                }

            }
        } finally {
            mAdapter.onResult(list);
        }
        AppLog.d(TAG, "suggestion response: " + response);

    }


    public interface ResultReceiver {
        public void onResult(List<BasicWordUIContainer> list);
        public boolean isInProcess();
        public void startProcess();
        public void finishProcess();
    }
}
