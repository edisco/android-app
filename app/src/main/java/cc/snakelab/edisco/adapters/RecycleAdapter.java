package cc.snakelab.edisco.adapters;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import cc.snakelab.edisco.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.core.EndlessRecyclerViewScrollListener;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.ItemTouchHelperAdapter;
import cc.snakelab.edisco.holders.LoadingFooterViewHolder;
import cc.snakelab.edisco.holders.RecycleViewHolder;

import cc.snakelab.edisco.holders.SuggestionWordHolder;
import cc.snakelab.edisco.holders.WordsViewHolder;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.ContainerDeleteListener;
import cc.snakelab.edisco.ui.WordContainer;
import cc.snakelab.edisco.ui.WordSuggestionContainer;

import static cc.snakelab.edisco.R.drawable.ic_reminder_black_24dp;
import static cc.snakelab.edisco.R.drawable.ic_sync_black_16dp;

/**
 * Created by shen on 24.10.2016.
 */
public class RecycleAdapter extends RecyclerView.Adapter<RecycleViewHolder> implements ItemTouchHelperAdapter, Filterable, RecyclerFilter.ResultReceiver {

    public static final int VIEW_TYPE_LOADING=1;
    public static final int VIEW_TYPE_WORDHOLDER=2;
    public static final int VIEW_TYPE_WORDSUGGESTION = 3;

    private static final String TAG = "RecycleAdapter";
    private List<BasicWordUIContainer> mFiltredWordList;
    private RecyclerFilter mFilter;
    private Reminder mReminder;
    private Drawable mReminderIcon;
    private Drawable mSyncIcon;
    private RecyclerView mRecyclerView;
    private EndlessRecyclerViewScrollListener mScrollListener;

    private AtomicInteger mProcessCount;
    private OnAdapterChangeListener mAdapterChangeListener;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mRecyclerView = null;
    }

    // Конструктор
    public RecycleAdapter(OnAdapterChangeListener adapterChangeListener) {
        mAdapterChangeListener = adapterChangeListener;
        mProcessCount = new AtomicInteger(0);
        mFiltredWordList = new ArrayList<BasicWordUIContainer>();
        mFilter = new RecyclerFilter(this);
        mReminderIcon = App.app.getApplicationContext().getResources().getDrawable(ic_reminder_black_24dp);
        mSyncIcon = App.app.getApplicationContext().getResources().getDrawable(ic_sync_black_16dp);
        mReminder = App.app.getReminder();

    }


    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RecycleViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {

        View v = null;
        RecycleViewHolder vh = null;
        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)
        switch (viewType) {
            case VIEW_TYPE_WORDHOLDER:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recycler_item, parent, false);
                vh = new WordsViewHolder(v, this, mReminder);
                break;
            case VIEW_TYPE_WORDSUGGESTION:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.suggestion_word, parent, false);
                vh = new SuggestionWordHolder(v, this);
                break;
            case VIEW_TYPE_LOADING:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.loading_item, parent, false);
                vh = new LoadingFooterViewHolder(v);
                break;
        }

        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(final RecycleViewHolder holder, int position) {
        AppLog.d(TAG, "onBindViewHolder: %d/%d", position, mFiltredWordList.size());
        switch (getItemViewType(position)) {
            case VIEW_TYPE_WORDHOLDER:
                AppLog.d(TAG, "WORDHOLDER");
                bindWordViewHolder((WordsViewHolder) holder, position);
                break;
            case VIEW_TYPE_LOADING:
                AppLog.d(TAG, "Footer");
                bindLoadingFooterViewHoler((LoadingFooterViewHolder) holder);
                break;
            case VIEW_TYPE_WORDSUGGESTION:
                AppLog.d(TAG, "SUGGESTION");
                bindSuggestionWord((SuggestionWordHolder) holder, position);
                break;

        }

    }

    private void bindSuggestionWord(SuggestionWordHolder holder, int position) {

        holder.setContainer(mFiltredWordList.get(position));

    }

    private void bindLoadingFooterViewHoler(LoadingFooterViewHolder holder) {
        holder.startAnim();
        if (!isInProcess())
            holder.itemView.setVisibility(View.INVISIBLE);
        else
            holder.itemView.setVisibility(View.VISIBLE);


    }

    private void bindWordViewHolder(WordsViewHolder holder, int pos) {

        BasicWordUIContainer w = mFiltredWordList.get(pos);
        Drawable remIcon = null;
        Drawable syncIcon = null;
        if (!w.isSynced())
            syncIcon = mSyncIcon;

        if (w.isUsedByReminder(mReminder))
            remIcon = mReminderIcon;


        holder.mIcon.setCompoundDrawablesWithIntrinsicBounds(remIcon, null, null, null);
        holder.mUnsyncedIcon.setCompoundDrawablesWithIntrinsicBounds(syncIcon, null, null, null);

        holder.mWordHead.setText(w.getHead());
        holder.mWordBody.setText(w.getBody());

        holder.mDate.setText(DateHelper.getReadableFormat(w.getRecord().getCreated_at()));
        holder.mPercents.setText(String.format("%d %%", w.getRetention()));
        holder.mWordContainer = w;
    }

    @Override
    public int getItemViewType(int position) {

        if (position >= mFiltredWordList.size()) return VIEW_TYPE_LOADING;
        if (mFiltredWordList.get(position) instanceof WordContainer)
            return VIEW_TYPE_WORDHOLDER;
        if (mFiltredWordList.get(position) instanceof WordSuggestionContainer)
            return VIEW_TYPE_WORDSUGGESTION;
        return VIEW_TYPE_WORDHOLDER;
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        int count = mFiltredWordList.size();
        if (isInProcess())
            count += 1;
        return  count;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private void reset() {
        mFiltredWordList.clear();
        if (mScrollListener != null)
            mScrollListener.resetState();

        //fix for https://stackoverflow.com/questions/30220771/recyclerview-inconsistency-detected-invalid-item-position
        notifyDataSetChanged();

        mProcessCount = new AtomicInteger(0);

    }
    public void filter(String text) {
        reset();
        mFilter.filter(text);
    }

    public void refresh() {
        reset();
        mFilter.refresh();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mFiltredWordList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }
    @Override
    public void onItemDismiss(int position) {
        BasicWordUIContainer c = mFiltredWordList.get(position);
        c.delete(mReminder);

        mFiltredWordList.remove(position);
        notifyItemRemoved(position);
    }

    public boolean onWordPairSelected(WordPair wp, BasicWordUIContainer wordContainer) {
        mReminder.includeWordPair(wp);
        mReminder.save();

        notifyContainerChanged(wordContainer);
        return true;
    }



    public void fetchNextPage() {
        mFilter.fetchNextPage();
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });

    }

    public void setOnScrollListener(EndlessRecyclerViewScrollListener scrollListener) {
        mScrollListener = scrollListener;

    }

    @Override
    public void onResult(final List<BasicWordUIContainer> list) {
        AppLog.d(TAG, "onFiltredWordUpdate");
        finishProcess();
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                AppLog.d(TAG, "Post onFiltredWordUpdate list.size=%d", list.size());
                mFiltredWordList.addAll(list);
                notifyDataSetChanged();
                mAdapterChangeListener.onDataChaned();
            }});

    }
    @Override
    public boolean isInProcess() {
        return mProcessCount.get() > 0;
    }
    @Override
    public void startProcess() {
        mProcessCount.incrementAndGet();
    }
    @Override
    public void finishProcess() {
        mProcessCount.decrementAndGet();
    }

    public void notifyContainerChanged(BasicWordUIContainer container) {
        int index =mFiltredWordList.indexOf(container);
        if (index >= 0) {
            notifyItemRangeChanged(index, 1);
        }
    }

    public Reminder getReminder() {
        return mReminder;
    }

    public interface OnAdapterChangeListener {
        void onDataChaned();
    }
}

