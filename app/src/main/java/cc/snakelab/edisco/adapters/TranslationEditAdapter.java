package cc.snakelab.edisco.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raizlabs.android.dbflow.StringUtils;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.holders.TranslationEditHolder;
import cc.snakelab.edisco.orm.Word;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by snake on 02.02.17.
 */

public class TranslationEditAdapter extends RecyclerView.Adapter<TranslationEditHolder> implements View.OnTouchListener {
    private static String TAG = "TranslationEditAdapter";
    private Word mWord;
    private List<Word> mTranslations;
    private int mSelectedPosition = -1;
    private  TextView.OnEditorActionListener mActionListiner;
    private boolean mValidate;


    public TranslationEditAdapter(TextView.OnEditorActionListener actionListiner) {
        super();

        mActionListiner = actionListiner;
        mTranslations = new ArrayList<>();



    }
    public void focusFirstTranslation() {
        if (mTranslations.size() > 0) {
            mSelectedPosition = 0;
            notifyDataSetChanged();
        }
    }
    @Override
    public TranslationEditHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.translation_edit_word_item, parent, false);
        TranslationEditHolder holder = new TranslationEditHolder(this, v, mTranslations, mActionListiner);

        return holder;
    }

    @Override
    public void onBindViewHolder(TranslationEditHolder holder, int position) {
        int pos = position;
        final Word word = mTranslations.get(pos);

        holder.updatePosition(pos);
        holder.mText.setTag(holder);
        holder.mText.setOnTouchListener(this);
        holder.mText.setText(word.getText());
        holder.mText.setError(null);
        if (mValidate && !word.getErrors().isEmpty()) {
            holder.mText.setError(word.getErrors().get(Word.VALID_TEXT));
            mSelectedPosition = pos;
        }



        if (pos == mSelectedPosition) {
            holder.mText.requestFocus();
            holder.mText.setSelected(true);
            mSelectedPosition = -1;
        }
    }

    @Override
    public int getItemCount() {
        return mTranslations.size();
    }

    public void putWord(Word word) {
        mWord = word;
        mValidate = false;
        mTranslations.addAll(mWord.getPairTranslations());
        mTranslations.add(getNewTranslWord());

        notifyDataSetChanged();

    }
    private Word getNewTranslWord() {
        return new Word(-1, "", LocaleHelper.stringToCodeLang(App.app, App.preference.getLang()));
    }
    public int addWord() {
        mValidate = false;
        mTranslations.add(getNewTranslWord());
        int pos = mTranslations.size() - 1;
        mSelectedPosition = pos;
        notifyItemInserted(pos);
        return pos;

    }
    public List<Word> getTranslations(){
        return mTranslations;
    }

    public boolean isValid() {
        mValidate = true;

        boolean valid = mTranslations.size() > 0;
        for(int i=0; i < mTranslations.size(); i++){
            Word word = mTranslations.get(i);
            word.getErrors().clear();
            if(i == mTranslations.size() - 1 && mTranslations.size() != 1)
                //ignore empty
                valid = valid && (word.isValidIgnoreEmpty());
            else
                valid = valid && (word.isValid());

        }

        notifyDataSetChanged();
        return valid;
    }

    public void finish() {
        mTranslations.clear();
        notifyDataSetChanged();
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {


        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            TranslationEditHolder holder = (TranslationEditHolder) view.getTag();
            if (holder != null && holder.isLast()) {
                addWord();

            }

        }
        return false;
    }
}
