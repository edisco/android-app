package cc.snakelab.edisco.adapters;

import android.content.ContentResolver;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.holders.TrashViewHolder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordTrashContainer;

import java.util.ArrayList;
import java.util.List;


public class TrashAdapter extends RecyclerView.Adapter<TrashViewHolder> {
    private List<BasicWordUIContainer> mDataset;
    private Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public TrashAdapter(Context context) {
        mDataset = new ArrayList<>();
        mContext = context;
        update();

    }
    public void update() {
        mDataset.clear();
        List<Word> list = SQLite.select().from(Word.class).where(Word_Table.trashed.eq(true)).queryList();
        for (Word w : list) {
            mDataset.add(new WordTrashContainer(w, this, mContext));

        }
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TrashViewHolder onCreateViewHolder(ViewGroup parent,
                                              int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trash_recycler_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        TrashViewHolder vh = new TrashViewHolder(v, this);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(TrashViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.container = mDataset.get(position);
        holder.firstTextView.setText(mDataset.get(position).getHead());
        holder.secondTextView.setText(mDataset.get(position).getBody());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
