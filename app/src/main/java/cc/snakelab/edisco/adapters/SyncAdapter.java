package cc.snakelab.edisco.adapters;


import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.api.Api;
import cc.snakelab.edisco.api.EdiscoApi;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.core.WordMerger;
import cc.snakelab.edisco.core.WordPairHistoryMerger;
import cc.snakelab.edisco.core.WordPairMerger;
import cc.snakelab.edisco.core.WordTripleHistoryMerger;
import cc.snakelab.edisco.core.WordTripleMerger;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordPairHistory_Table;
import cc.snakelab.edisco.orm.WordPair_Table;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTripleHistory;
import cc.snakelab.edisco.orm.WordTripleHistory_Table;
import cc.snakelab.edisco.orm.WordTriple_Table;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.recievers.SyncReceiver;

import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import static cc.snakelab.edisco.recievers.SyncReceiver.ON_SYNC_ERROR_ACTION;

/**
 * Created by snake on 24.03.17.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter implements Api.ApiResultListener {
    private static final String TAG = "SyncAdapter";
    private static final String OutdatedVersionLogMsg = "Seems our version is outdated. Bump version and send it again";
    private static final int HTTP_OUTDATE_CODE = 409;
    EdiscoApi mApi;
    List<Word> mWordList;
    List<WordPair> mWordPairList;
    List<WordTriple> mWordTripleList;

    List<WordPairHistory> mWordPairHistoryList;
    List<WordTripleHistory> mWordTripleHistoryList;

    WordMerger mWordMerger;
    WordPairMerger mWordPairMerger;
    WordTripleMerger mWordTripleMerger;
    WordPairHistoryMerger mWordPairHistoryMerger;
    WordTripleHistoryMerger mWordTripleHistoryMerger;
    int mCurrentWordIndex;
    int mCurrentWordPairIndex;
    int mCurrentWordTripleIndex;

    int mCurrentWordPairHistoryIndex;
    int mCurrentWordTripleHistoryIndex;
    boolean isDone;
    boolean hasErrors;
    protected CountDownLatch mSignal;
    private Throwable mException;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        init();
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        init();
    }
    private void init(){
        if(FirebaseApp.getApps(App.app).isEmpty()){
            FirebaseApp.initializeApp(App.app, FirebaseOptions.fromResource(App.app));
        }

        mWordMerger = new WordMerger();
        mWordPairMerger = new WordPairMerger();
        mWordTripleMerger = new WordTripleMerger();
        mWordPairHistoryMerger = new WordPairHistoryMerger();
        mWordTripleHistoryMerger = new WordTripleHistoryMerger();
        isDone = false;
        hasErrors = false;
        mException = null;
        if(FirebaseApp.getApps(App.app).isEmpty()){
            FirebaseApp.initializeApp(App.app, FirebaseOptions.fromResource(App.app));
        }
        App.app.sendBroadcast(new Intent(SyncReceiver.ON_SYNC_START_ACTION));
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        try {
            AppLog.d(TAG, "perform sync");
            mApi = new EdiscoApi(this,  App.preference.getApiServerUrl(), App.preference.getEmail(), App.preference.getApiToken());
            Log.d(TAG, "api token="+App.preference.getApiToken());
            mSignal = new CountDownLatch(1);
            try {

                getWords(-1);
                while (!isDone) {
                    if (Thread.currentThread().isInterrupted())
                        finish();

                    try {
                        mSignal.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            } catch (Exception e) {
                if (mException != null)
                    AppLog.e(TAG, "Error", e);
                else
                    mException = e;
            }
        } finally {
            if (mException != null) {
                AppLog.d(TAG, "Sync Done with Exception: " + mException.getMessage());
                App.app.sendBroadcast(new Intent(ON_SYNC_ERROR_ACTION).putExtra("msg", mException.getMessage()));
            } else {
                AppLog.d(TAG, "Sync Done");
                App.app.sendBroadcast(new Intent(SyncReceiver.ON_SYNC_DONE_ACTION));
            }

        }
    }



    @Override
    public void onErrorResponse(VolleyError error) {
        AppLog.e(TAG, "Error: " +error.getMessage());
    }

    @Override
    public void onResponse(Api.ApiResult response) {
        try {
            AppLog.d(TAG, "onResponse: " +response.getResult());
            BroadcastResponse(response);
            if (response.isOk()) {
                AppLog.d(TAG, "Timestamp %s", DateHelper.dateToIso8601Z(response.getLastTimestamp()));

                switch (response.getMethodId()) {
                    case EdiscoApi.GET_USER_WORDS:
                        for (int i = 0; i < response.getData().length(); i++) {
                            JSONObject jo = response.getData().optJSONObject(i);
                            Word word = Word.fromJSON(jo);
                            AppLog.d(TAG, "GET_USER_WORDS");
                            mWordMerger.merge(word);
                        }
                        //call next page
                        if (response.getPage() > 0)
                            getWords(response.getPage() + 1);
                        else {
                            if (response.getLastTimestamp().after(App.preference.getLastWordTimestamp())) {
                                AppLog.d(TAG, "Updated LastWordTimestamp");
                                App.preference.setLastWordTimestamp(response.getLastTimestamp());
                            }
                            initSendWords();
                        }
                        break;

                    case EdiscoApi.CREATE_USER_WORD:
                    case EdiscoApi.UPDATE_USER_WORD:
                        mCurrentWordIndex += 1;
                        Word word = Word.fromJSON(response.getData().getJSONObject(0));
                        AppLog.d(TAG, "CREATE_UPDATE_USER_WORD");
                        mWordMerger.merge(word);
                        sendWords();
                        break;

                    case EdiscoApi.GET_USER_WORD_PAIRS:
                        for (int i = 0; i < response.getData().length(); i++) {
                            try {
                                JSONObject jo = response.getData().optJSONObject(i);
                                WordPair wordPair = WordPair.fromJSON(jo);
                                AppLog.d(TAG, "GET_USER_WORD_PAIRS");
                                mWordPairMerger.merge(wordPair);
                            } catch (Exception e) {
                                AppLog.e(TAG, "on get user word pairs", e);
                            }
                        }
                        //call next page
                        if (response.getPage() > 0)
                            getWordPairs(response.getPage() + 1);
                        else {
                            if (response.getLastTimestamp().after(App.preference.getLastWordPairTimestamp())) {
                                AppLog.d(TAG, "Updated LastWordPairTimestamp");
                                App.preference.setLastWordPairTimestamp(response.getLastTimestamp());
                            }
                            initSendWordPairs();
                        }
                        break;
                    case EdiscoApi.CREATE_USER_WORD_PAIR:
                    case EdiscoApi.UPDATE_USER_WORD_PAIR:
                        mCurrentWordPairIndex += 1;
                        WordPair wp = WordPair.fromJSON(response.getData().getJSONObject(0));
                        AppLog.d(TAG, "CREATE_UPDATE_USER_WORD_PAIR");
                        mWordPairMerger.merge(wp);
                        sendWordPairs();
                        break;
                    case EdiscoApi.GET_USER_WORD_TRIPLES:
                        for (int i = 0; i < response.getData().length(); i++) {
                            try {
                                JSONObject jo = response.getData().optJSONObject(i);
                                WordTriple wordTriple = WordTriple.fromJSON(jo);
                                AppLog.d(TAG, "GET_USER_WORD_TRIPLES");
                                mWordTripleMerger.merge(wordTriple);
                            } catch (Exception e) {
                                AppLog.e(TAG, "on get user word triples", e);
                            }
                        }
                        //call next page
                        if (response.getPage() > 0)
                            getWordTriples(response.getPage() + 1);
                        else {
                            if (response.getLastTimestamp().after(App.preference.getLastWordTripleTimestamp())) {
                                AppLog.d(TAG, "Updated LastWordTripleTimestamp");
                                App.preference.setLastWordTripleTimestamp(response.getLastTimestamp());
                            }
                            initSendWordTriples();
                        }
                        break;
                    case EdiscoApi.UPDATE_USER_WORD_TRIPLE:
                        mCurrentWordTripleIndex += 1;
                        WordTriple wt = WordTriple.fromJSON(response.getData().getJSONObject(0));
                        AppLog.d(TAG, "UPDATE_USER_WORD_TRIPLE");
                        mWordTripleMerger.merge(wt);
                        sendWordTriples();
                        break;

                    case EdiscoApi.GET_USER_WORD_PAIR_HISTORIES:
                        for (int i = 0; i < response.getData().length(); i++) {
                            try {
                                JSONObject jo = response.getData().optJSONObject(i);
                                WordPairHistory wph = WordPairHistory.fromJSON(jo);
                                AppLog.d(TAG, "GET_USER_WORD_PAIR_HISTORIES");
                                mWordPairHistoryMerger.merge(wph);
                            } catch (Exception e) {
                                AppLog.e(TAG, "on get user word pair history", e);
                            }
                        }
                        //call next page
                        if (response.getPage() > 0)
                            getWordPairHistories(response.getPage() + 1);
                        else
                            initSendWordPairHistories();
                        break;

                    case EdiscoApi.CREATE_USER_WORD_PAIR_HISTORY:
                    case EdiscoApi.UPDATE_USER_WORD_PAIR_HISTORIES:
                        mCurrentWordPairHistoryIndex += 1;
                        WordPairHistory wph = WordPairHistory.fromJSON(response.getData().getJSONObject(0));
                        AppLog.d(TAG, "CREATE_UPDATE_USER_WORD_PAIR_HISTORIES");
                        mWordPairHistoryMerger.merge(wph);
                        sendWordPairHistory();
                        break;
                    case EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES:
                        for (int i = 0; i < response.getData().length(); i++) {
                            try {
                                JSONObject jo = response.getData().optJSONObject(i);
                                WordTripleHistory wth = WordTripleHistory.fromJSON(jo);
                                AppLog.d(TAG, "GET_USER_WORD_TRIPLE_HISTORIES");
                                mWordTripleHistoryMerger.merge(wth);
                            } catch (Exception e) {
                                AppLog.e(TAG, "on get user word triple history", e);
                            }
                        }
                        //call next page
                        if (response.getPage() > 0)
                            getWordTripleHistories(response.getPage() + 1);
                        else
                            initSendWordTripleHistories();
                        break;
                    case EdiscoApi.CREATE_USER_WORD_TRIPLE_HISTORY:
                    case EdiscoApi.UPDATE_USER_WORD_TRIPLE_HISTORIES:
                        mCurrentWordTripleHistoryIndex += 1;
                        WordTripleHistory wth = WordTripleHistory.fromJSON(response.getData().getJSONObject(0));
                        AppLog.d(TAG, "CREATE_UPDATE_USER_WORD_TRIPLE_HISTORIES");
                        mWordTripleHistoryMerger.merge(wth);
                        sendWordTripleHistory();
                        break;
                }
            } else
            {
                if (response.getException() != null)
                    AppLog.e(TAG, "error response", response.getException());
                else
                    AppLog.e(TAG, String.format("error response: %d %s",response.getStatusCode(), response.getResult()));
                if (response.getException() instanceof com.android.volley.TimeoutError) {
                    finishWithException(new Exception(String.format("Unable to connect the cloud")));
                    return;
                }
                if (response.getException() instanceof Api.WrongTokenException) {
                    finishWithException(response.getException());
                    return;
                }
                switch (response.getMethodId()) {
                    case EdiscoApi.GET_USER_WORDS:
                        initSendWords();
                        break;
                    case EdiscoApi.UPDATE_USER_WORD:
                    case EdiscoApi.CREATE_USER_WORD:
                        handleWordError(response);
                        sendWords();
                        break;

                    case EdiscoApi.GET_USER_WORD_PAIRS:
                        initSendWordPairs();
                        break;
                    case EdiscoApi.UPDATE_USER_WORD_PAIR:
                    case EdiscoApi.CREATE_USER_WORD_PAIR:
                        handleWordPairError(response);
                        sendWordPairs();
                        break;
                    case EdiscoApi.GET_USER_WORD_TRIPLES:
                        initSendWordTriples();
                        break;
                    case EdiscoApi.UPDATE_USER_WORD_TRIPLE:
                        handeWordTripleError(response);
                        sendWordTriples();
                        break;
                    case EdiscoApi.GET_USER_WORD_PAIR_HISTORIES:
                        initSendWordPairHistories();
                        break;
                    case EdiscoApi.UPDATE_USER_WORD_PAIR_HISTORIES:
                    case EdiscoApi.CREATE_USER_WORD_PAIR_HISTORY:
                        handleWordPairHistoryError(response);
                        sendWordPairHistory();
                        break;
                    case EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES:
                        initSendWordTripleHistories();
                        break;
                    case EdiscoApi.UPDATE_USER_WORD_TRIPLE_HISTORIES:
                    case EdiscoApi.CREATE_USER_WORD_TRIPLE_HISTORY:
                        handleWordTripleHistoryError(response);
                        sendWordTripleHistory();
                        break;
                }
            }

        } catch (Exception e) {
            finishWithException(e);

        }
    }

    private void BroadcastResponse(Api.ApiResult response) {
        Intent intent;
        if (response.isOk()) {
            intent = new Intent(SyncReceiver.ON_SYNC_PROGRESS_ACTION);
            intent.putExtra(SyncReceiver.API_METHOD, response.getMethodId());
            if (response.getData() != null)
                intent.putExtra(SyncReceiver.RECORDS_NUM, response.getData().length());
        } else {
            intent = new Intent(ON_SYNC_ERROR_ACTION);
            intent.putExtra(SyncReceiver.ERROR, response.getException().getMessage());
        }
        App.app.sendBroadcast(intent);
    }


    private void handleWordPairHistoryError(Api.ApiResult result) {
        if (mCurrentWordPairHistoryIndex < mWordPairHistoryList.size() && result.getStatusCode() == HTTP_OUTDATE_CODE) {
            AppLog.d(TAG, OutdatedVersionLogMsg);
            WordPairHistory wph = mWordPairHistoryList.get(mCurrentWordPairHistoryIndex);
            wph.setVersion(wph.getVersion() + 1);
            wph.save();
        } else
            mCurrentWordPairHistoryIndex += 1;
    }

    private void handleWordTripleHistoryError(Api.ApiResult result) {
        if (mCurrentWordTripleHistoryIndex < mWordTripleHistoryList.size() && result.getStatusCode() == HTTP_OUTDATE_CODE) {
            AppLog.d(TAG, OutdatedVersionLogMsg);
            WordTripleHistory wth = mWordTripleHistoryList.get(mCurrentWordTripleHistoryIndex);
            wth.setVersion(wth.getVersion() + 1);
            wth.save();
        } else
            mCurrentWordTripleHistoryIndex += 1;

    }

    private void handeWordTripleError(Api.ApiResult result) {
        if (mCurrentWordTripleIndex < mWordTripleList.size() && result.getStatusCode() == HTTP_OUTDATE_CODE) {
            AppLog.d(TAG, OutdatedVersionLogMsg);
            WordTriple wt = mWordTripleList.get(mCurrentWordTripleIndex);
            wt.setVersion(wt.getVersion() + 1);
            wt.save();
        } else
            mCurrentWordTripleIndex += 1;
    }

    private void handleWordPairError(Api.ApiResult result) {
        if (mCurrentWordPairIndex < mWordPairList.size() && result.getStatusCode() == HTTP_OUTDATE_CODE) {
            AppLog.d(TAG, OutdatedVersionLogMsg);
            WordPair wp = mWordPairList.get(mCurrentWordPairIndex);
            wp.setVersion(wp.getVersion() + 1);
            wp.save();
        } else
            mCurrentWordPairIndex += 1;
    }

    private void handleWordError(Api.ApiResult result) {
        if (mCurrentWordIndex < mWordList.size() && result.getStatusCode() == HTTP_OUTDATE_CODE) {
            AppLog.d(TAG, OutdatedVersionLogMsg);
            Word word = mWordList.get(mCurrentWordIndex);
            word.setVersion(word.getVersion() + 1);
            word.save();
        } else
            mCurrentWordIndex += 1;

    }

    private void sendWordPairs() {
        while (mCurrentWordPairIndex < mWordPairList.size()) {
            WordPair wordPair = mWordPairList.get(mCurrentWordPairIndex);
            try {
                if (StringUtils.isNullOrEmpty(wordPair.getUuid())) {
                    AppLog.d(TAG, "createUserWordPair: " + wordPair.toJSON().toString());
                    AppLog.d(TAG, "createUserWordPair text: " + wordPair.toString());
                    mApi.createUserWordPair(wordPair);
                } else {
                    AppLog.d(TAG, "updateUserWordPair: " + wordPair.toJSON().toString());
                    AppLog.d(TAG, "updateUserWordPair text: " + wordPair.toString());
                    mApi.updateUserWordPair(wordPair);
                }
                return;

            } catch (Exception e) {
                AppLog.e(TAG, "fail to sendWordPair. Delete it", e);
                wordPair.delete();

            }
            mCurrentWordPairIndex += 1;
        }

        getWordTriples(-1);

    }

    private void getWordTriples(int page) {
        AppLog.d(TAG, "Last WordTriple Timestamp %s", DateHelper.dateToIso8601Z(App.preference.getLastWordTripleTimestamp()));
        mApi.getUserWordTriples(page, 50, App.preference.getLastWordTripleTimestamp().getTime() ,null);

    }

    private void getWordPairHistories(int page) {
        mApi.getUserWordPairHistories(page, 50, null);
    }


    private void sendWordTriples() {
        while (mCurrentWordTripleIndex < mWordTripleList.size()) {
            WordTriple wordTriple = mWordTripleList.get(mCurrentWordTripleIndex);
            try {
                if (StringUtils.isNullOrEmpty(wordTriple.getUuid())) {
                    AppLog.d(TAG, "createUserWordTriple: " + wordTriple.toJSON().toString());
                    AppLog.d(TAG, "createUserWordTriple text: " + wordTriple.toString());
                    AppLog.d(TAG, "ignoring creation of WordTriple");
                    mCurrentWordTripleIndex += 1;
                    continue;
                } else {
                    AppLog.d(TAG, "updateUserWordPair: " + wordTriple.toJSON().toString());
                    AppLog.d(TAG, "updateUserWordPair text: " + wordTriple.toString());
                    mApi.updateUserWordTriple(wordTriple);
                }
                return;
            }catch (Exception e)
            {
                AppLog.e(TAG, "fail to sendWordTriple", e);

            }
            mCurrentWordTripleIndex += 1;


        }

        getWordPairHistories(-1);

    }

    private void initSendWordPairs() {
        mWordPairList = SQLite.select().from(WordPair.class).where(WordPair_Table.synced.eq(false)).queryList();
        mCurrentWordPairIndex = 0;
        sendWordPairs();
    }

    private void initSendWordTriples() {
        mWordTripleList = SQLite.select().from(WordTriple.class).where(WordTriple_Table.synced.eq(false)).queryList();
        mCurrentWordTripleIndex = 0;
        sendWordTriples();
    }

    private void initSendWordPairHistories() {
        mWordPairHistoryList = SQLite.select().from(WordPairHistory.class).where(WordPairHistory_Table.synced.eq(false)).queryList();
        mCurrentWordPairHistoryIndex = 0;
        sendWordPairHistory();
    }
    private void initSendWordTripleHistories() {
        mWordTripleHistoryList = SQLite.select().from(WordTripleHistory.class).where(WordTripleHistory_Table.synced.eq(false)).queryList();
        mCurrentWordTripleHistoryIndex = 0;
        sendWordTripleHistory();
    }



    private void initSendWords() {
        mWordList = SQLite.select().from(Word.class).where(Word_Table.synced.eq(false)).queryList();
        mCurrentWordIndex = 0;
        sendWords();
    }

    private void sendWords() {
        while (mCurrentWordIndex < mWordList.size()) {
            try {
                Word word = mWordList.get(mCurrentWordIndex);
                if (StringUtils.isNullOrEmpty(word.getUuid())) {
                    AppLog.d(TAG, "createUserWord: " + word.toJSON().toString());
                    mApi.createUserWord(word);
                } else {
                    AppLog.d(TAG, "updateUserWord: " + word.toJSON().toString());
                    mApi.updateUserWord(word);
                }
                return;

            } catch (Exception e) {
                AppLog.e(TAG, "faild to send Words", e);
            }
            mCurrentWordIndex +=1;

        }
        getWordPairs(-1);

    }
    private void sendWordPairHistory() {
        while (mCurrentWordPairHistoryIndex < mWordPairHistoryList.size()) {
            try {
                WordPairHistory wph = mWordPairHistoryList.get(mCurrentWordPairHistoryIndex);
                if (StringUtils.isNullOrEmpty(wph.getUuid())) {
                    AppLog.d(TAG, "createUserWordPairHistory: " + wph.toJSON().toString());
                    mApi.createUserWordPairHistory(wph);
                } else {
                    AppLog.d(TAG, "updateUserWordPairHistory: " + wph.toJSON().toString());
                    mApi.updateUserWordPairHistory(wph);
                }
                return;
            } catch (Exception e) {
                AppLog.e(TAG, "Fail to send PairHistory", e);

            }
            mCurrentWordPairHistoryIndex +=1;
        }
        getWordTripleHistories(-1);

    }

    private void sendWordTripleHistory() {
        while (mCurrentWordTripleHistoryIndex < mWordTripleHistoryList.size()) {
            try {
                WordTripleHistory wth = mWordTripleHistoryList.get(mCurrentWordTripleHistoryIndex);
                if (StringUtils.isNullOrEmpty(wth.getUuid())) {
                    AppLog.d(TAG, "createUserWordTripleHistory: " + wth.toJSON().toString());
                    mApi.createUserWordTripleHistory(wth);
                } else {
                    AppLog.d(TAG, "updateUserWordTripleHistory: " + wth.toJSON().toString());
                    mApi.updateUserWordTripleHistory(wth);
                }
                return;
            } catch (Exception e) {
                AppLog.e(TAG, "Fail to send TripleHistory", e);

            }
            mCurrentWordTripleHistoryIndex +=1;
        }
        finish();
    }

    private void getWordTripleHistories(int page) {
        mApi.getUserWordTripleHistories(page, 50, null);
    }

    private void getWordPairs(int page) {
        AppLog.d(TAG, "Last WordPair Timestamp %s", DateHelper.dateToIso8601Z(App.preference.getLastWordPairTimestamp()));
        mApi.getUserWordPairs(page, 50, App.preference.getLastWordPairTimestamp().getTime(), null);
    }

    private void getWords(int page) {
        AppLog.d(TAG, "Last Word Timestamp %s", DateHelper.dateToIso8601Z(App.preference.getLastWordTimestamp()));
        mApi.getUserWords(page, 50, App.preference.getLastWordTimestamp().getTime(), null);


    }

    private void finish() {
        isDone = true;
        mSignal.countDown();
    }
    private void finishWithException(Throwable e) {
        isDone = true;
        mException = e;
        mSignal.countDown();
    }
}
