package cc.snakelab.edisco;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cc.snakelab.edisco.core.App;

/**
 * Created by snake on 23.02.17.
 */

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    final public static String PREF_SCREEN="preferenceScreen";
    final public static String NATIVE_LANG="native_language";
    final public static String SERVER="server";
    final public static String LEARNING_LANGUAGES="learning_languages";
    final public static String CURRENT_LEARNING_LANGUAGE="current_learning_language";
    ListPreference mNativeLanguage;
    ListPreference mServer;
    MultiSelectListPreference mLearningLanguages;
    PreferenceScreen mPrefScreen;
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        unregister();
        try {
            switch (key) {
                case NATIVE_LANG:
                    break;
                case LEARNING_LANGUAGES:
                    break;

            }
            int size = App.preference.getLearningLangsList().size();
            if (size == 1)
                App.preference.setCurrentLearningLang(App.preference.getLearningLangsList().get(0));
            if (size == 0) {
                String[] langs = getResources().getStringArray(R.array.default_learning_languages);


                if (langs.length > 0) {
                    App.preference.setCurrentLearningLang(langs[0]);
                    App.preference.setLearningLangsList(Arrays.asList(langs));
                }
            }
            updateUI();
        }finally {
            register();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        //make sure settings are populated with default values
        App.preference.getLang();
        App.preference.getLearningLangs();
        App.preference.getApiServerUrl();

        addPreferencesFromResource(R.xml.settings);
        mPrefScreen = (PreferenceScreen) findPreference(PREF_SCREEN);
        mNativeLanguage = (ListPreference) findPreference(NATIVE_LANG);
        mLearningLanguages = (MultiSelectListPreference) findPreference(LEARNING_LANGUAGES);
        mServer = (ListPreference) findPreference(SERVER);
        if (!BuildConfig.DEBUG)
            mPrefScreen.removePreference(mServer);


    }

    @Override
    public void onPause() {
        super.onPause();
        unregister();
    }

    @Override
    public void onResume() {
        super.onResume();
        register();
        updateUI();

    }
    private void register() {
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }
    private void unregister() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private void updateUI() {
        mNativeLanguage.setSummary(App.preference.getLang());
        mLearningLanguages.setSummary(App.preference.getLearningLangs());
        mServer.setSummary(App.preference.getApiServerUrl());
    }

}
