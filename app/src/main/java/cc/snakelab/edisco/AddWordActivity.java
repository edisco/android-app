package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.Word_Table;


public class AddWordActivity extends AppCompatActivity implements WordEditFragment.OnWordFinishListener {
    public static final String MAIN_SEARCH_TEXT="MAIN_SEARCH_TEXT";
    public static final String SUGGESTION_FRONT_TEXT="SUGGESTION_FRONT_TEXT";
    public static final String SUGGESTION_BACK_TEXT="SUGGESTION_BACK_TEXT";

    WordEditFragment mEditFragment;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(R.layout.activity_add_word_pair);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEditFragment = (WordEditFragment) getSupportFragmentManager().findFragmentById(R.id.edit_word_fragment);

        String orgText = getIntent().getStringExtra(MAIN_SEARCH_TEXT);
        String translText = getIntent().getStringExtra(SUGGESTION_BACK_TEXT);
        String lang = LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang());
        if (StringUtils.isNullOrEmpty(orgText))
            orgText = getIntent().getStringExtra(SUGGESTION_FRONT_TEXT);

        Word word =
                SQLite.select().from(Word.class).where(Word_Table.text.eq(orgText)).and(Word_Table.lang.eq(lang)).querySingle();
        if (word == null) {
            word = new Word();
            word.setLang(lang);
            word.setText(orgText);
        }

        if (!StringUtils.isNullOrEmpty(translText))
            word.addTranslation(translText, LocaleHelper.stringToCodeLang(App.app, App.preference.getLang()));
        mEditFragment.putWord(word);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save_word, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.save_word) {
            mEditFragment.save();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onWordSave(Word word) {
        setResult(RESULT_OK);
        finish();
    }
}
