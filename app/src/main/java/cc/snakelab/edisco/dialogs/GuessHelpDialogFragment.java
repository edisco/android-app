package cc.snakelab.edisco.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 02.12.16.
 */

public class GuessHelpDialogFragment extends DialogFragment {
    public static final java.lang.String GUESS_WORD = "guess_word";
    private GuessHelpListener mListener;

    public interface GuessHelpListener {
        void OnGuessHelpOkClick(android.support.v4.app.DialogFragment dialog);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (GuessHelpListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_guess_help, null, false);
        TextView textView = (TextView) view.findViewById(R.id.guess_word);
        textView.setText(getArguments().getString(GUESS_WORD));

        builder
                .setView(view)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        mListener.OnGuessHelpOkClick(GuessHelpDialogFragment.this);

                    }
                })

        ;
        return builder.create();
    }

}
