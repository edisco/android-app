package cc.snakelab.edisco.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 02.12.16.
 */

public class GuessIncorrectDialogFragment extends DialogFragment {


    public static final String ATTEMPTS = "attempts";
    public static final String MAX_ATTEMPTS = "max_attempts";

    public interface GuessIncorrectDialogListener {
        void OnGuessIncorrectOkClick(android.support.v4.app.DialogFragment dialog);


    }
    GuessIncorrectDialogListener mListener;
    TextView mAttempts;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_guess_incorrect, null);
        mAttempts = (TextView) view.findViewById(R.id.attempts);

        mAttempts.setText(
                getString(
                        R.string.guess_attempts,
                        getArguments().getInt(ATTEMPTS),
                        getArguments().getInt(MAX_ATTEMPTS)
                )
        );


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        mListener.OnGuessIncorrectOkClick(GuessIncorrectDialogFragment.this);

                    }
                })

        ;
        return builder.create();
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (GuessIncorrectDialogListener) context;
    }
}
