package cc.snakelab.edisco.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 02.12.16.
 */

public class GuessCorrectDialogFragment extends DialogFragment {
    public interface GuessDialogListener {
        void OnGuessCorrectOkClick(android.support.v4.app.DialogFragment dialog);
        void OnGuessLearntClick(android.support.v4.app.DialogFragment dialog);
    }
    Activity mActivity;
    GuessDialogListener mListener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
        mListener = (GuessDialogListener) context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_guess_correct, null))
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        mListener.OnGuessCorrectOkClick(GuessCorrectDialogFragment.this);

                    }
                })
                .setNegativeButton(R.string.learnt, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                                mListener.OnGuessLearntClick(GuessCorrectDialogFragment.this);
                            }
                        }
                )
        ;
        return builder.create();
    }
}
