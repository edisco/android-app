package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by snake on 10.03.17.
 */
@Table(
        database = AppDatabase.class,
        indexGroups =
                {
                        @IndexGroup(number = 1, name = "wordTripleIdIndex"),
                        @IndexGroup(number = 95, name = "wordTripleHistoryCreatedAtIndex"),
                        @IndexGroup(number = 105, name = "wordTripleHistoryUpdatedAtIndex")

                })
public class WordTripleHistory extends BasicRecord {
    private static final String TAG = "WordTripleHistory";

    @Column
    private boolean correct;

    @Index(indexGroups = 1)
    @Column
    private long word_triple_id;
    private WordTriple wordTriple;

    public WordTripleHistory(){super();}
    public WordTripleHistory(long id, boolean correct, long tripleId, Date created_at) {
        setId(id);
        setCorrect(correct);
        setWord_triple_id(tripleId);
        setCreated_at(created_at);
    }

    public WordTripleHistory(boolean correct, long tripleId) {
        this(-1, correct, tripleId, DateHelper.getCalendar().getTime());
    }


    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public long getWord_triple_id() {
        return word_triple_id;
    }

    public void setWord_triple_id(long word_triple_id) {
        this.word_triple_id = word_triple_id;
    }

    public static WordTripleHistory fromJSON(JSONObject jsonObject) throws Exception {
        WordTripleHistory wth = new WordTripleHistory();

        wth.setUuid(jsonObject.optString("uuid"));
        JSONObject attr = jsonObject.optJSONObject("attributes");
        wth.setVersion(attr.optInt("version"));
        wth.setTrashed(attr.optBoolean("trashed"));
        wth.setCorrect(attr.optBoolean("correct"));
        wth.setCreated_at(DateHelper.iso8601UtcToDate(attr.optString("created_at")));
        String uuid = attr.optString("word_triple_uuid");
        WordTriple wt = findByUuid(WordTriple.class, uuid);
        if (wt == null)
            throw new Exception("Unable to find word pair: "+ uuid);
        wth.setWord_triple_id(wt.getId());
        return wth;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", getUuid());
            JSONObject attr = new JSONObject();
            attr.put("correct", isCorrect());
            attr.put("word_triple_uuid", getWordTriple().getUuid());
            attr.put("created_at", DateHelper.dateToIso8601Z(getCreated_at()));
            attr.put("version", getVersion());
            attr.put("trashed", isTrashed());
            jsonObject.put("attributes", attr);


        } catch (JSONException e) {

            AppLog.e(TAG, "toJSON", e);
        }
        return jsonObject;

    }

    public WordTriple getWordTriple() {
        if (wordTriple == null)
            wordTriple = findById(WordTriple.class, getWord_triple_id());
        return wordTriple;
    }
}
