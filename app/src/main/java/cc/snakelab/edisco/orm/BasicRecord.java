package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.sql.QueryBuilder;

import com.raizlabs.android.dbflow.sql.language.SQLOperator;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import cc.snakelab.edisco.helper.DateHelper;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by snake on 08.10.16.
 */

public class BasicRecord extends BaseModel {
    public static long NOID = -1;
    private static String TAG="BasicRecord";
    private HashMap<String, String> mValidationErrors;

    @PrimaryKey(autoincrement = true)
    private long id;

    @Index(indexGroups = 55)
    @Column()
    private String uuid;

    @Index(indexGroups = 65)
    @Column(defaultValue = "0")
    private boolean synced;

    @Index(indexGroups = 75)
    @Column(defaultValue = "0")
    private int version;

    @Index(indexGroups = 85)
    @Column(defaultValue = "0")
    private boolean trashed;
    @Index(indexGroups = 95)
    @Column(typeConverter = DateStrConverter.class)
    private Date created_at;

    @Index(indexGroups = 105)
    @Column(typeConverter = DateStrConverter.class)
    private Date updated_at;

    public BasicRecord(long id) {
        this();
        this.id = id;
    }
    public BasicRecord() {

        this.id = NOID;
        this.created_at = DateHelper.getCalendar().getTime();
        this.updated_at = DateHelper.getCalendar().getTime();
        mValidationErrors = new HashMap<>();
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public void assign(BasicRecord basicRecord) {
        this.setUuid(basicRecord.getUuid());
        this.setSynced(basicRecord.isSynced());
        this.setTrashed(basicRecord.isTrashed());
        this.setVersion(basicRecord.getVersion());
        this.setId(basicRecord.getId());
        this.setCreated_at(basicRecord.getCreated_at());
        this.setUpdated_at(basicRecord.getUpdated_at());

    }

    public static <T extends BasicRecord> T findById(Class<T> T, final long id) {
        return  SQLite.select().from(T).where(new SQLOperator() {
            @Override
            public void appendConditionToQuery(QueryBuilder queryBuilder) {
                queryBuilder.append(columnName()).append(operation()).append(value());
            }

            @Override
            public String columnName() {
                return "id";
            }

            @Override
            public String separator() {
                return "";
            }

            @Override
            public SQLOperator separator(String separator) {
                return this;
            }

            @Override
            public boolean hasSeparator() {
                return false;
            }

            @Override
            public String operation() {
                return "=";
            }

            @Override
            public Object value() {
                return new Long(id);
            }
        }).querySingle();
    }
    public static <T extends BasicRecord> T findByUuid(Class<T> T, final String uuid) {
        return  SQLite.select().from(T).where(new SQLOperator() {
            @Override
            public void appendConditionToQuery(QueryBuilder queryBuilder) {
                queryBuilder.append(columnName()).append(operation()).append(value());
            }

            @Override
            public String columnName() {
                return "uuid";
            }

            @Override
            public String separator() {
                return "";
            }

            @Override
            public SQLOperator separator(String separator) {
                return this;
            }

            @Override
            public boolean hasSeparator() {
                return false;
            }

            @Override
            public String operation() {
                return "=";
            }

            @Override
            public Object value() {
                return "\""+uuid+"\"";
            }
        }).querySingle();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public JSONObject toJSON() {
        return null;
    }

    public JSONObject saveToJSON() {
        return null;
    }
    // validation block
    public boolean isValid() {
        return true;
    }
    protected void addError(String key, String text) {
        mValidationErrors.put(key, text);
    }
    public HashMap<String, String> getErrors() {
        return mValidationErrors;
    }


    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isTrashed() {
        return trashed;
    }

    public void setTrashed(boolean trashed) {
        this.trashed = trashed;
    }
    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public void setCloudNew() {
        setSynced(false);
        setUuid(null);
        save();
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }
}
