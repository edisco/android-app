package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.converter.TypeConverter;
import cc.snakelab.edisco.helper.DateHelper;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by snake on 19.01.17.
 */

@com.raizlabs.android.dbflow.annotation.TypeConverter
public class DateStrConverter extends TypeConverter<String, Date> {

    @Override
    public String getDBValue(Date model) {
        return DateHelper.dateToDBIso8601(model);
    }

    @Override
    public Date getModelValue(String data) {
        try {
            return DateHelper.DBIso8601ToDate(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return DateHelper.getCalendar().getTime();
    }
}