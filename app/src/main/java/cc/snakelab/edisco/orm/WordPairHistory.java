package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by snake on 28.12.16.
 */

@Table(
        database = AppDatabase.class,
        indexGroups =
        {
                @IndexGroup(number = 1, name = "wordPairIdIndex"),
                @IndexGroup(number = 95, name = "wordPairHistoryCreatedAtIndex"),
                @IndexGroup(number = 105, name = "wordPairHistoryUpdatedAtIndex")

        })
public class WordPairHistory extends BasicRecord {
    private static final String TAG = "WordPairHistory";

    @Column
    private boolean correct;

    @Index(indexGroups = 1)
    @Column
    private long word_pair_id;

    WordPair wordPair;
    public WordPairHistory(long id, boolean correct, long word_pair_id, Date created_at) {
        setId(id);
        setCorrect(correct);
        setWord_pair_id(word_pair_id);
        setCreated_at(created_at);
    }

    public WordPairHistory(boolean correct, long word_pair_id) {
        this(-1, correct, word_pair_id, DateHelper.getCalendar().getTime());
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", getUuid());
            JSONObject attr = new JSONObject();
            attr.put("correct", isCorrect());
            attr.put("word_pair_uuid", getWordPair().getUuid());
            attr.put("created_at", DateHelper.dateToIso8601Z(getCreated_at()));
            attr.put("version", getVersion());
            attr.put("trashed", isTrashed());
            jsonObject.put("attributes", attr);


        } catch (JSONException e) {

            AppLog.e(TAG, "toJSON", e);
        }
        return jsonObject;

    }

    public WordPairHistory() {
        super();
    }



    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public long getWord_pair_id() {
        return word_pair_id;
    }

    public void setWord_pair_id(long word_pair_id) {
        this.word_pair_id = word_pair_id;
    }
    public WordPair getWordPair() {
        if (wordPair == null)
            wordPair = SQLite.select().from(WordPair.class).where(WordPair_Table.id.eq(word_pair_id)).querySingle();
        return wordPair;
    }

    public static WordPairHistory fromJSON(JSONObject jsonObject) throws Exception {
        WordPairHistory wph = new WordPairHistory();
        wph.setUuid(jsonObject.optString("uuid"));
        JSONObject attr = jsonObject.optJSONObject("attributes");
        wph.setVersion(attr.optInt("version"));
        wph.setTrashed(attr.optBoolean("trashed"));
        wph.setCorrect(attr.optBoolean("correct"));
        wph.setCreated_at(DateHelper.iso8601UtcToDate(attr.optString("created_at")));
        String uuid = attr.optString("word_pair_uuid");
        WordPair wp = WordPair.findByUuid(WordPair.class, uuid);
        if (wp == null)
            throw new Exception("Unable to find word pair: "+ uuid);
        wph.setWord_pair_id(wp.getId());

        return wph;
    }
}
