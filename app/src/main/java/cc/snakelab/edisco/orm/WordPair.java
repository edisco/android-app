package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.RetentionHelper;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by snake on 01.10.16.
 */
@Table(
        database = AppDatabase.class,
        indexGroups =
                {
                        @IndexGroup(number = 1, name = "wordPairOrdWordIdIndex"),
                        @IndexGroup(number = 2, name = "wordTranslWordIdIndex"),
                        @IndexGroup(number = 55, name = "wordPairUuidIndex"),
                        @IndexGroup(number = 65, name = "wordPairSyncedIndex"),
                        @IndexGroup(number = 75, name = "wordPairVersionIndex"),
                        @IndexGroup(number = 85, name = "wordPairTrashedIndex"),
                        @IndexGroup(number = 95, name = "wordPairCreatedAtIndex"),
                        @IndexGroup(number = 105, name = "wordPairUpdatedAtIndex")


                })
public class WordPair extends BasicRecord {

    private static final String TAG = "WordPair";

    @Index(indexGroups = 1)
    @Column
    private long orgWordId;


    @Index(indexGroups = 2)
    @Column
    private long translWordId;

    @Column(defaultValue = "0")
    private int stage;

    @Column(defaultValue = "0")
    private int order;



    private Word translation;
    private Word original;

    private List<WordPairHistory> wordPairHistoryList;



    public WordPair(long id, long orgWordId, long translWordId) {
        this(orgWordId, translWordId);
        this.setId(id);
    }

    public WordPair(long orgWordId, long translWordId) {
        this();
        this.orgWordId = orgWordId;
        this.translWordId = translWordId;

    }


    public WordPair() {
        super();

        wordPairHistoryList = null;

    }

    public WordPair(Word org, Word transl) {
        this(org.getId(), transl.getId());
        this.original = org;
        this.translation = transl;
    }

    public WordPair(String original, String translation, String orgLang, String translLang) {
        Word frontWord = Word.findOrCreate(original, orgLang);
        Word backWord =Word.findOrCreate(translation, translLang);
        WordPair wp = findByForntIdBackId(frontWord.getId(), backWord.getId());
        if (wp!=null) {
            this.assign(wp);
        } else {
            setOrgWordId(frontWord.getId());
            setTranslWordId(backWord.getId());
        }


    }

    private WordPair findByForntIdBackId(long org_id, long transl_id) {
        return SQLite.select().from(WordPair.class).where(WordPair_Table.orgWordId.eq(org_id)).and(WordPair_Table.translWordId.eq(transl_id)).querySingle();
    }

    @Override
    public void assign(BasicRecord basicRecord) {
        super.assign(basicRecord);
        WordPair source = (WordPair) basicRecord;
        this.setStage(source.getStage());
        this.setTranslWordId(source.getTranslWordId());
        this.setOrgWordId(source.getOrgWordId());

    }





    public long getTranslWordId() {
        return translWordId;
    }

    public void setTranslWordId(long translWordId) {
        this.translWordId = translWordId;
        this.translation = null;
    }


    public long getOrgWordId() {
        return orgWordId;
    }

    public void setOrgWordId(long orgWordId) {
        this.orgWordId = orgWordId;
        this.original = null;
    }

    public Word getTranslation()  {
        try {
            if (translation == null) translation = findById(Word.class, translWordId);
            return translation;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }



    public Word getOriginal() {
        try {
            if (original == null) original = findById(Word.class, orgWordId);

            return original;
        }catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }





    @Override
    public String toString() {
        return getOriginal().toString() + " - " + getTranslation().toString();
    }


    public boolean hasLearnt() {
        try {
            Calendar cal = DateHelper.getCalendar();
            cal.add(Calendar.DAY_OF_YEAR, -1);
            List<WordPairHistory> list = getHistory().and(WordPairHistory_Table.created_at.greaterThanOrEq(cal.getTime())).queryList();
                    //WordPairHistory.where(WordPairHistory.class, "word_pair_id = "+getId()+" and created_at >= \""+ DateHelper.dateToIso8601(cal.getTime())+"\"", null, null, null);
            int correctAmount = 0;

            for(WordPairHistory wph: list) {
                if (wph.isCorrect()) {
                    correctAmount = correctAmount + 1;
                }
            }
            //word pair has learnt if:
            //user answered at least 50% of scheduled reminds
            ///amount of correct answers for last day is greater 90%
            return
                    (list.size() *100 / Reminder.get().calcRemindsPerDay()) >= 50 &&
                    (correctAmount * 100.0/list.size()) > 90;

        } catch (Exception e) {
            e.printStackTrace();
        }


        return false;
    }

    public WordPairHistory doCorrectAnswer(boolean correct) {
        WordPairHistory wph = new WordPairHistory(correct, getId());
        wph.save();
        return wph;

    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int calcRetention() {
        return calcRetention(DateHelper.getCalendar().getTime());

    }
    public int calcRetention(Date currentDate){
        WordPairHistory wph = getHistory().and(WordPairHistory_Table.correct.eq(true)).orderBy(WordPairHistory_Table.created_at, false).querySingle();
        Date lastCorrect;
        if (wph != null) {
            lastCorrect = wph.getCreated_at();
        } else
            return 0;
        return RetentionHelper.getRetention(currentDate, getStage(), lastCorrect);

    }


    public void nextStage() {
        setStage(RetentionHelper.nextStage(getStage()));
    }



    public Where<WordPairHistory> getHistory() {
        return SQLite.select().from(WordPairHistory.class).where(WordPairHistory_Table.word_pair_id.eq(getId()));

    }
    public List<WordPairHistory> getHistoryList() {
        if (wordPairHistoryList == null)
            wordPairHistoryList = getHistory().queryList();
        return wordPairHistoryList;
    }
    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isWordUsed(Word word) {
        return getOrgWordId() == word.getId() || getTranslWordId() == word.getId();
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", getUuid());
            JSONObject attr = new JSONObject();
            attr.put("front_uuid", getOriginal().getUuid());
            attr.put("back_uuid", getTranslation().getUuid());
            attr.put("order", getOrder());
            attr.put("stage", getStage());
            attr.put("version", getVersion());
            attr.put("trashed", isTrashed());
            attr.put("created_at", DateHelper.dateToIso8601Z(getCreated_at()));
            jsonObject.put("attributes", attr);


        } catch (JSONException e) {

            AppLog.e(TAG, "toJSON", e);
        }
        return jsonObject;
    }

    public static WordPair fromJSON(JSONObject jsonObject) throws Exception {
        WordPair wp = new WordPair();
        wp.setUuid(jsonObject.optString("uuid"));
        JSONObject attr = jsonObject.optJSONObject("attributes");
        wp.setVersion(attr.optInt("version"));
        wp.setTrashed(attr.optBoolean("trashed"));
        wp.setStage(attr.optInt("stage"));
        wp.setOrder(attr.optInt("order"));
        wp.setCreated_at(DateHelper.iso8601UtcToDate(attr.optString("created_at")));


        String uuid = attr.optString("front_uuid");
        Word word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wp.setOrgWordId(word.getId());
        else
            throw new Exception("Unable to find front word: "+ uuid);

        uuid = attr.optString("back_uuid");
        word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wp.setTranslWordId(word.getId());
        else
            throw new Exception("Unable to find back word: "+ uuid);

        return wp;

    }
    @Override
    public JSONObject saveToJSON() {
        JSONObject jo = new JSONObject();
        try {
            jo.put("lang", getOriginal().getLang());
            jo.put("text", getOriginal().getText());
            JSONObject jt = new JSONObject();
            jt.put("lang", getTranslation().getLang());
            jt.put("text", getTranslation().getText());
            jo.put("translation", jt);

        } catch (Exception e) {

        }
        return jo;

    }
    public static WordPair loadFromJSON(JSONObject jo) {
        try {
            String orgLang = jo.optString("lang");
            String orgText = jo.optString("text");

            JSONObject jt = null;

            jt = jo.getJSONObject("translation");

            String transLang = jt.optString("lang");
            String transText = jt.optString("text");

            return new WordPair(orgText, transText, orgLang, transLang);

        } catch (JSONException e) {
            return null;
        }

    }


}
