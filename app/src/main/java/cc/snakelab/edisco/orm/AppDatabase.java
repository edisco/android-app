package cc.snakelab.edisco.orm;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.QueryBuilder;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.language.Index;
import com.raizlabs.android.dbflow.sql.language.property.IProperty;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;
import com.raizlabs.android.dbflow.sql.migration.BaseMigration;
import com.raizlabs.android.dbflow.sql.migration.IndexMigration;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import cc.snakelab.edisco.BuildConfig;
import cc.snakelab.edisco.core.AppLog;

/**
 * Created by snake on 13.01.17.
 */

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    private static final String MIGRATION_TAG = "migration";
    public static final String NAME = BuildConfig.DB_FILENAME; // we will add the .db extension

    public static final int VERSION = 5;

    @Migration(version = 2, database = AppDatabase.class)
    public static class Migration2 extends AlterTableMigration<Reminder> {
        @Override
        public void onPreMigrate() {
            AppLog.d(MIGRATION_TAG, String.format("migrating to DB version %d", 2));
            addColumn(SQLiteType.INTEGER, "playGame");
            AppLog.d(MIGRATION_TAG, "finished");
        }

        public Migration2(Class<Reminder> table) {
            super(table);

        }
    }
    @Migration(version = 4, database = AppDatabase.class)
    public static class Migration4 extends AlterTableMigration<WordTriple> {
        @Override
        public void onPreMigrate() {
            AppLog.d(MIGRATION_TAG, String.format("migrating to DB version %d", 4));
            addColumn(SQLiteType.INTEGER, "stage");
            AppLog.d(MIGRATION_TAG, "finished");
        }

        public Migration4(Class<WordTriple> table) {
            super(table);

        }
    }
    @Migration(version = 5, database = AppDatabase.class)
    public static class Migration5 extends AlterTableMigration<Word> {
        @Override
        public void onPreMigrate() {
            AppLog.d(MIGRATION_TAG, String.format("migrating to DB version %d", 5));
            addColumn(SQLiteType.INTEGER, "version");
            addColumn(SQLiteType.TEXT, "uuid");

            AppLog.d(MIGRATION_TAG, "finished");
        }

        public Migration5(Class<Word> table) {
            super(table);

        }
    }

}
