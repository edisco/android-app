package cc.snakelab.edisco.orm;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;

import com.raizlabs.android.dbflow.sql.language.OperatorGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.FilterHelper;
import cc.snakelab.edisco.helper.LocaleHelper;

/**
 * Created by snake on 01.10.16.
 */

@Table(
        database = AppDatabase.class,
        indexGroups =
                {
                        @IndexGroup(number = 95, name = "ReminderCreatedAtIndex"),
                        @IndexGroup(number = 105, name = "ReminderUpdatedAtIndex")

                })
public class Reminder extends BasicRecord{
//    private static final float REMIND_INTERVAL = 1.5f;
//    public static final int REMIND_INTERVAL_MIN = (int) (60 * REMIND_INTERVAL);
    final static String TAG = App.APPTAG+"App";
    @Column
    private int startHour;
    @Column
    private int finishHour;
    @Column
    private int startMin;
    @Column
    private int finishMin;
    @Column
    private boolean active;
    @Column
    private boolean playGame;

    @Column
    private String pairs;

    @Column(typeConverter = DateStrConverter.class)
    private Date lastRemind;

    @Column
    private long currentWordPairId;
    private WordPair currentWordPair;

    @Column
    private boolean guessFront;

    @Column(defaultValue = "90")
    private int period;

    @Column(defaultValue = "true")
    private boolean guessPair;

    @Column
    private long currentWordTripleId;
    private WordTriple currentWordTriple;

    @Column
    private String triples;


    public Reminder() {
        super();
        pairs = "";
        playGame = false;
    }
    private boolean withinHoursFrame(int hours, int mins) {
        return (hours > startHour || hours == startHour && mins >= startMin) &&
                (hours < finishHour || hours == finishHour && mins < finishMin);
    }



    public boolean isTime(Date date) {
        int date_hours = DateHelper.getHours(date);
        int date_mins = DateHelper.getMinutes(date);

        return active &&
                date.after(calcNextRemind()) &&
                withinHoursFrame(date_hours, date_mins);
    }

    public Date calcNextRemind() {
        return calcNextRemind(getLastRemind());
    }
    public Date calcNextRemind(Date date) {
        Calendar cal = DateHelper.getCalendar();
        cal.setTime(getLastRemind());

        int date_hours;
        int date_mins;
        do {
            cal.add(Calendar.MINUTE, getPeriod());
            date_hours = DateHelper.getHours(cal.getTime());
            date_mins = DateHelper.getMinutes(cal.getTime());
        } while  (cal.getTime().before(date) ||
                !withinHoursFrame(date_hours, date_mins));
        return cal.getTime();

    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPairs() {
        return pairs;
    }

    public void setPairs(String pairs) {
        this.pairs = pairs;
    }

    public Date getLastRemind() {
        return lastRemind;
    }

    public void setLastRemind(Date lastRemind) {
        this.lastRemind = lastRemind;
    }

    public long getCurrentWordPairId() {
        return currentWordPairId;
    }

    public void setCurrentWordPairId(long currentWordPairId) {
        if (this.currentWordPairId != currentWordPairId) this.currentWordPair = null;
        this.currentWordPairId = currentWordPairId;
    }
    public void setCurrentWordTripleId(long currentWordTripleId) {
        if (this.currentWordTripleId != currentWordTripleId) this.currentWordTriple = null;
        this.currentWordTripleId = currentWordTripleId;
    }

    public boolean isGuessFront() {
        return guessFront;
    }

    public void setGuessFront(boolean guessFront) {
        this.guessFront = guessFront;
    }


    public WordPair getCurrentWordPair(){
        if (this.currentWordPair == null) try {
            this.currentWordPair = WordPair.findById(WordPair.class, this.currentWordPairId);
        } catch (Exception e) {
            AppLog.e(TAG, "Unable to getCurrentWordPair: id="+this.currentWordPairId, e );
        }
        return this.currentWordPair;
    }

    public WordTriple getCurrentWordTriple() {
        if (this.currentWordTriple == null) try {
            this.currentWordTriple = WordTriple.findById(WordTriple.class, this.currentWordTripleId);
        } catch (Exception e) {
            AppLog.e(TAG, "Unable to getCurrentWordTriple: id="+this.currentWordTripleId, e );
        }
        return this.currentWordTriple;
    }

    public Word getGuessWord() {
        return getGuessWord(0);
    }
    public Word getGuessWord(int index)  {
        if (isGuessPair()) {
            WordPair wp = getCurrentWordPair();
            if (isGuessFront())
                return wp.getOriginal();
            else
                return wp.getTranslation();
        } else {
            if (isGuessFront()) {
                switch (index) {
                    case 0: return getCurrentWordTriple().getOrgWord1();
                    case 1: return getCurrentWordTriple().getOrgWord2();
                    case 2: return getCurrentWordTriple().getOrgWord3();

                }


            } else
                return getCurrentWordTriple().getTranslWord();

        }
        return null;
    }

    public Word getTranslationWord(){
        return getTranslationWord(-1);
    }
    public Word getTranslationWord(int index) {
        if (isGuessPair()) {
            WordPair wp = getCurrentWordPair();

            if (!isGuessFront())
                return wp.getOriginal();
            else
                return wp.getTranslation();
        }   else
        {
            WordTriple wt = getCurrentWordTriple();
            if(!isGuessFront() ) {
                switch(index) {
                    case 0: return wt.getOrgWord1();
                    case 1: return wt.getOrgWord2();
                    case 2: return wt.getOrgWord3();
                    default: return null;
                }
            } else
                return wt.getTranslWord();
        }



    }

    public boolean isCorrect(String text) {
        return isCorrect(text, -1);
    }
    public boolean isCorrect(String text, int index) {
        Word w = getTranslationWord(index);
        return w.toString().equalsIgnoreCase(text);
    }



    public long setNewPair() {
        List<Long> ar = getWordPairIds();

        if (ar.size() == 0)
            return -1;


        long id = ar.remove(0);

        if (ar.size() > 0) {
            ar.add(id);
            id = ar.get(0);
            this.setPairs(IdsToStr(ar));
        }

        setCurrentWordPairId(id);

        return id;
    }
    public long setNewTriple() {
        List<Long> ar = getWordTripleIds();

        if (ar.size() == 0)
            return -1;


        long id = ar.remove(0);

        if (ar.size() > 0) {
            ar.add(id);
            id = ar.get(0);
            this.setTriples(IdsToStr(ar));
        }

        setCurrentWordTripleId(id);

        return id;
    }
    public void setNewWord() {
        Random r = new Random();
        setGuessFront(r.nextBoolean());
        boolean newGuessPair = r.nextBoolean() && getWordPairIds().size() > 0 || getWordTripleIds().size() == 0;
        setGuessPair(newGuessPair);

        if(newGuessPair)
            setNewPair();
        else
            setNewTriple();

    }


    public String getTranslMask(String s) {
        return getTranslMask(s, -1);

    }
    public String getTranslMask(String s, int index) {
        char[] org_txt = getTranslationWord(index).toString().toCharArray();
        char[] txt = getTranslationWord(index).toString().toLowerCase().toCharArray();
        char[] guess = s.toLowerCase().toCharArray();
        String res = "";
        for (int i = 0; i < txt.length; i++) {
            if (i < s.length()) {
                if (txt[i] == guess[i])
                    res = res + org_txt[i];
                else
                    res = res + "*";
            } else
                res = res + "*";
        }
        return res;
    }

    public int getFinishMin() {
        return finishMin;
    }

    public void setFinishMin(int finishMin) {
        this.finishMin = finishMin;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(int finishHour) {
        this.finishHour = finishHour;
    }

    public int getStartMin() {
        return startMin;
    }

    public void setStartMin(int startMin) {
        this.startMin = startMin;
    }

    public boolean isWordPairUsed(WordPair wp) {
        for(WordPair wordPair: getWordPairs()){
            if (wordPair.getId()== wp.getId())
                return true;
        }
        return false;
    }
    public List<WordPair> getWordPairs() {
        OperatorGroup grp =
                OperatorGroup.clause().and(WordPair_Table.id.in(getWordPairIds()));
        return SQLite.select().from(WordPair.class).where(grp).queryList();
    }
    public List<WordTriple> getWordTriples() {
        OperatorGroup grp =OperatorGroup.clause().and(WordTriple_Table.id.in(getWordTripleIds()));
        return SQLite.select().from(WordTriple.class).where(grp).queryList();
    }


    public int calcRemindsPerDay() {

        long minBetween = (getFinishHour() - getStartHour()) * 60 + getFinishMin() - getStartMin();
        return (int) (minBetween / getPeriod());
    }

    public void doLearnt() {
        if (isGuessPair())
            doLearntPair();
        else
            doLearntTriple();

    }

    private void doLearntTriple() {
        WordTriple oldWt = getCurrentWordTriple();
        oldWt.nextStage();
        oldWt.save();


        String lang = LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang());
        List<WordTriple> list = FilterHelper.querySortByRetentionWT("", 0, 3, lang, "WordTriple.id not in " + FilterHelper.IdsToStr(getWordTripleIds()), false);
        excludeWordTriple(oldWt);

        WordTriple newWt = oldWt;
        if (list.size() > 0) {
            newWt = list.get(0);
        }
        includeWordTriple(newWt);
    }

    private void doLearntPair() {
        WordPair oldWp = getCurrentWordPair();
        oldWp.nextStage();
        oldWp.save();

        String lang = LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang());

        List<WordPair> list = FilterHelper.querySortByRetentionWP("", 0, 3, lang, "WordPair.id not in " + FilterHelper.IdsToStr(getWordPairIds()), false);
        excludeWordPair(oldWp);

        WordPair newWp = oldWp;
        if (list.size() > 0) {
            newWp = list.get(0);
        }

        includeWordPair(newWp);
    }

    private List<Long> getWordPairIds() {
        List<Long> list = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(pairs))
            for(String id: pairs.split(",")) {
                if (!id.isEmpty())
                    list.add(Long.parseLong(id.replaceAll(" ", "")));
            }
        return list;
    }
    private List<Long> getWordTripleIds() {
        List<Long> list = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(triples))
            for(String id: triples.split(",")) {
                if (!id.isEmpty())
                    list.add(Long.parseLong(id.replaceAll(" ", "")));
            }
        return list;
    }
    private String IdsToStr(List<Long> list) {
        String ret ="";
        for(Long id: list) {
            ret = ret + id + ", ";
        }
        if (ret.length() >= 2)
            ret = ret.substring(0, ret.length() - 2);
        return  ret;
    }
    public void includeWordPair(WordPair wp) {
        List<Long> list = getWordPairIds();
        if (list.indexOf(wp.getId()) == -1)
                list.add(wp.getId());

        setPairs(IdsToStr(list));
        ensureCurrentWordPair();

    }

    public void includeWordTriple(WordTriple wt) {
        List<Long> list = getWordTripleIds();
        if (list.indexOf(wt.getId()) == -1)
            list.add(wt.getId());

        setTriples(IdsToStr(list));
        ensureCurrentWordTriple();

    }
    public boolean excludeWordPair(WordPair wp) {
        List<Long> list = getWordPairIds();
        if (list.remove(wp.getId())) {
            setPairs(IdsToStr(list));
            ensureCurrentWordPair();
            return true;
        } else
            return false;



    }
    public boolean excludeWordTriple(WordTriple wt) {
        List<Long> list = getWordTripleIds();
        if (list.remove(wt.getId())) {
            setTriples(IdsToStr(list));
            ensureCurrentWordTriple();
            return true;
        } else
            return false;

    }
    private void ensureCurrentWordPair() {
        List<Long> list =getWordPairIds();
        if ((list.size() > 0) && (list.indexOf(getCurrentWordPairId()) == -1)) {
            setCurrentWordPairId(list.get(0));
        }
        if (list.size() == 0)
            setCurrentWordPairId(NOID);

        if (list.size() >0 && getWordTripleIds().size() == 0)
            setGuessPair(true);
        if (list.size() == 0 && getWordTripleIds().size() > 0) {

            setGuessPair(false);
        }

    }
    private void ensureCurrentWordTriple() {
        List<Long> list = getWordTripleIds();
        if ((list.size() > 0) && (list.indexOf(getCurrentWordTripleId()) == -1)) {
            setCurrentWordTripleId(list.get(0));
        }
        if (list.size() == 0)
            setCurrentWordTripleId(NOID);
        if (list.size() == 0 && getWordPairIds().size() >0)
            setGuessPair(true);

        if  (list.size()  > 0 && getWordPairIds().size() == 0) {
            setGuessPair(false);

        }


    }
    public static Reminder get() {
        long count =SQLite.selectCountOf().from(Reminder.class).count();
        if (count == 0) {
            //create default reminder
            Reminder r = new Reminder();
            r.setStartHour(8);
            r.setStartMin(0);
            r.setFinishHour(20);
            r.setFinishMin(0);
            r.setActive(true);
            r.setPairs("");
            r.setLastRemind(DateHelper.getCalendar().getTime());
            r.setGuessFront(true);
            r.save();

        }
        return SQLite.select().from(Reminder.class).queryList().get(0);
    }


    @Override
    public JSONObject saveToJSON() {
        JSONObject jo = new JSONObject();
        try {
            jo.put("startHour", getStartHour());

            jo.put("startMin", getStartMin());
            jo.put("finishHour", getFinishHour());
            jo.put("finishMin", getFinishMin());
            jo.put("active", isActive());
            jo.put("period", getPeriod());
            JSONArray ja = new JSONArray();
            for(WordPair wp : getWordPairs()) {

                ja.put(wp.saveToJSON());
            }
            jo.put("pairs", ja);

            ja = new JSONArray();
            for (WordTriple wt: getWordTriples()) {
                ja.put(wt.saveToJSON());
            }

            jo.put("triples", ja);
            if ( getCurrentWordPair() != null)
                jo.put("currentPair", getCurrentWordPair().saveToJSON());
            if (getCurrentWordTriple() != null)
                jo.put("currentTriple", getCurrentWordTriple().saveToJSON());

            jo.put("lastRemind", DateHelper.dateToIso8601(getLastRemind()));
            jo.put("guessFront", isGuessFront());
            jo.put("guessPair", isGuessPair());

        } catch (JSONException e) {
            AppLog.e(TAG, "unable to saveToJSON", e);
        }
        return jo;
    }



    public static Reminder loadFromJSON(JSONObject jsonObject) {
        Reminder r = new Reminder();
        try {
            r.setStartHour(jsonObject.optInt("startHour"));
            r.setStartMin(jsonObject.optInt("startMin"));
            r.setFinishHour(jsonObject.optInt("finishHour"));
            r.setFinishMin(jsonObject.optInt("finishMin"));
            r.setPeriod(jsonObject.optInt("period"));
            r.setActive(jsonObject.optBoolean("active"));

            JSONArray ja = jsonObject.getJSONArray("pairs");
            for(int i = 0; i < ja.length(); i++) {
                WordPair wp = WordPair.loadFromJSON(ja.getJSONObject(i));
                if(wp.getId() != -1)
                    r.includeWordPair(wp);
            }

            ja = jsonObject.getJSONArray("triples");
            for(int i = 0; i < ja.length(); i++) {
                WordTriple wt = WordTriple.loadFromJSON(ja.getJSONObject(i));
                if(wt.getId() != -1)
                    r.includeWordTriple(wt);
            }

//            WordPair wp = WordPair.loadFromJSON(jsonObject.optJSONObject("currentPair"));
//            if (wp.getId() != -1)
//                r.setCurrentWordPairId(wp.getId());
//
//            WordTriple wt = WordTriple.loadFromJSON(jsonObject.optJSONObject("currentTriple"));
//            if (wt.getId() != -1)
//                r.setCurrentWordTripleId(wt.getId());

            r.setLastRemind(DateHelper.iso8601ToDate(jsonObject.optString("lastRemind")));
            r.setGuessFront(jsonObject.optBoolean("guessFront"));
            r.setGuessPair(jsonObject.optBoolean("guessPair"));



        } catch (Exception e) {
            AppLog.e(TAG, "unable to loadFromJSON", e);
        }


        return r;
    }

    public boolean isWordUsed(Word word) {
        for(WordPair wp: getWordPairs()) {
            if (wp.isWordUsed(word))
                return  true;
        }
        for(WordTriple wt: getWordTriples())
            if (wt.isWordUsed(word))
                return true;
        return false;
    }

    public String getLearningWordString() {
        StringBuilder wpSb = new StringBuilder();

        for(WordPair wp: getWordPairs()) {
            wpSb.append(String.format("%s, ", wp.toString()));

        }
        if (wpSb.length() > 1)
            wpSb.delete(wpSb.length()-2, wpSb.length());

        StringBuilder wtSb = new StringBuilder();
        for(WordTriple wt: getWordTriples()) {
            wtSb.append(String.format("%s, ", wt.toString()));
        }
        if (wtSb.length() > 1)
            wtSb.delete(wtSb.length()-2, wtSb.length());

        if (isGuessPair())
            return wpSb.toString() + ", " + wtSb.toString();
        else
            return wtSb.toString() + ", " + wpSb.toString();
    }

    public int getPeriod() {
        if (period == 0)
            period = 90;
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isGuessPair() {
        boolean guess =
                (guessPair &&
                getWordPairs().size() > 0 ||
                        getWordTriples().size() ==0);
        guessPair = guess;
        return guessPair;
    }

    public void setGuessPair(boolean guessPair) {
        this.guessPair = guessPair;
    }

    public long getCurrentWordTripleId() {
        return currentWordTripleId;
    }



    public String getTriples() {
        return triples;
    }

    public void setTriples(String triples) {
        this.triples = triples;
    }


    public boolean isPlayGame() {
        return playGame;
    }

    public void setPlayGame(boolean play_game) {
        this.playGame = play_game;
    }


    public void doCorrectAnswer(boolean correct) {
        if(isGuessPair())
            getCurrentWordPair().doCorrectAnswer(correct);
        else
            getCurrentWordTriple().doCorrectAnswer(correct);
    }

    public boolean isWordSet() {
        return getWordPairIds().size() > 0 || getWordTripleIds().size() > 0;
    }

    public boolean trash(Word word) {
        for(WordPair wp : word.getWordPairList())
            if (excludeWordPair(wp)) {
                save();
                return true;
            }
        return false;
    }

    public boolean trash(WordTriple wordTriple) {
        if (excludeWordTriple(wordTriple)) {
            save();
            return true;

        } else return false;


    }
}
