package cc.snakelab.edisco.orm;

import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.RetentionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by snake on 23.01.17.
 */
@Table(database = AppDatabase.class,
        indexGroups =
                {
                        @IndexGroup(number = 1, name = "orgWord1IdIndex"),
                        @IndexGroup(number = 2, name = "orgWord2IdIndex"),
                        @IndexGroup(number = 3, name = "orgWord3IdIndex"),
                        @IndexGroup(number = 4, name = "transWordIdIndex"),
                        @IndexGroup(number = 85, name = "wordTrashedIndex"),
                        @IndexGroup(number = 55, name = "wordUuidIndex"),
                        @IndexGroup(number = 65, name = "wordSyncedIndex"),
                        @IndexGroup(number = 75, name = "wordVersionIndex"),
                        @IndexGroup(number = 95, name = "wordTripleCreatedAtIndex"),
                        @IndexGroup(number = 105, name = "wordTripleUpdatedAtIndex")


                })

public class WordTriple extends BasicRecord {
    private static final String TAG = "WordTriple";
    @Index(indexGroups = 1)
    @Column
    private long orgWord1Id;

    @Index(indexGroups = 2)
    @Column
    private long orgWord2Id;

    @Index(indexGroups = 3)
    @Column
    private long orgWord3Id;

    @Index(indexGroups = 4)
    @Column
    private long transWordId;


    @Column(defaultValue = "0")
    private int stage;
    private Word orgWord1;
    private Word orgWord2;
    private Word orgWord3;
    private Word translWord;

    public WordTriple() {
        super();
    }

    public WordTriple(String org1, String org2, String org3, String transl, String orgLang, String translLang) {
        Word orgWord1 = Word.findOrCreate(org1, orgLang);
        setOrgWord1Id(orgWord1.getId());

        Word orgWord2 = Word.findOrCreate(org2, orgLang);
        setOrgWord2Id(orgWord2.getId());

        Word orgWord3 = Word.findOrCreate(org3, orgLang);
        setOrgWord3Id(orgWord3.getId());

        Word translWord = Word.findOrCreate(transl, translLang);
        setTransWordId(translWord.getId());

    }

    public long getOrgWord1Id() {
        return orgWord1Id;
    }

    public void setOrgWord1Id(long orgWord1Id) {
        this.orgWord1Id = orgWord1Id;
    }

    public long getOrgWord2Id() {
        return orgWord2Id;
    }

    public void setOrgWord2Id(long orgWord2Id) {
        this.orgWord2Id = orgWord2Id;
    }

    public long getOrgWord3Id() {
        return orgWord3Id;
    }

    public void setOrgWord3Id(long orgWord3Id) {
        this.orgWord3Id = orgWord3Id;
    }

    public long getTransWordId() {
        return transWordId;
    }

    public void setTransWordId(long transWordId) {
        this.transWordId = transWordId;
    }

    public Word getOrgWord1() {
        if (orgWord1 == null)
            orgWord1 = findById(Word.class, orgWord1Id);
        return orgWord1;
    }

    public Word getOrgWord2() {
        if (orgWord2 == null)
            orgWord2 = findById(Word.class, orgWord2Id);
        return orgWord2;
    }

    public Word getOrgWord3() {
        if (orgWord3 == null)
            orgWord3 = findById(Word.class, orgWord3Id);
        return orgWord3;
    }

    public Word getTranslWord() {
        if (translWord == null)
            translWord = findById(Word.class, transWordId);
        return translWord;
    }


    public boolean isWordUsed(Word word) {
        long id = word.getId();
        return getOrgWord1Id() == id ||
                getOrgWord2Id() == id ||
                getOrgWord3Id() == id ||
                getTransWordId() == id;
    }

    public void trash() {
        setTrashed(true);
        getOrgWord1().trash();
        getOrgWord2().trash();
        getOrgWord3().trash();
        getTranslWord().trash();
        save();
    }
    public Where<WordTripleHistory> getHistory() {
        return SQLite.select().from(WordTripleHistory.class).where(WordTripleHistory_Table.word_triple_id.eq(getId()));

    }

    public int calcRetention() {
        return calcRetention(DateHelper.getCalendar().getTime());
    }
    public int calcRetention(Date currentDate) {
        WordTripleHistory wth = getHistory().and(WordTripleHistory_Table.correct.eq(true)).orderBy(WordTripleHistory_Table.created_at, false).querySingle();
        Date lastCorrect;
        if (wth != null) {
            lastCorrect = wth.getCreated_at();
        } else
            return 0;
        return RetentionHelper.getRetention(currentDate, getStage(), lastCorrect);


    }


    public void nextStage() {
        setStage(RetentionHelper.nextStage(getStage()));
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s - %s", getOrgWord1().getText(), getOrgWord2().getText(), getOrgWord3().getText(), getTranslWord().getText());
    }

    @Override
    public JSONObject saveToJSON() {
        JSONObject jo = new JSONObject();
        try {


            jo.put("lang", getOrgWord1().getLang());

            jo.put("org1", getOrgWord1().getText());



            jo.put("org2", getOrgWord2().getText());


            jo.put("org3", getOrgWord3().getText());

            JSONObject jt = new JSONObject();
            jt.put("lang", getTranslWord().getLang());
            jt.put("text", getTranslWord().getText());
            jo.put("translation", jt);

        } catch (Exception e) {

        }
        return jo;
    }
    public static WordTriple fromJSON(JSONObject jo) throws Exception {
        WordTriple wt = new WordTriple();
        wt.setUuid(jo.optString("uuid"));
        JSONObject attr = jo.optJSONObject("attributes");
        wt.setVersion(attr.optInt("version"));
        wt.setTrashed(attr.optBoolean("trashed"));
        wt.setStage(attr.optInt("stage"));
        wt.setCreated_at(DateHelper.iso8601UtcToDate(attr.optString("created_at")));


        //front 1
        String uuid = attr.optString("front1_uuid");
        Word word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wt.setOrgWord1Id(word.getId());
        else
            throw new Exception("Unable to find front word 1: "+ uuid);

        //front 2
        uuid = attr.optString("front2_uuid");
        word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wt.setOrgWord2Id(word.getId());
        else
            throw new Exception("Unable to find front word 2: "+ uuid);

        //front 3
        uuid = attr.optString("front3_uuid");
        word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wt.setOrgWord3Id(word.getId());
        else
            throw new Exception("Unable to find front word 3: "+ uuid);

        //back
        uuid = attr.optString("back_uuid");
        word = findByUuid(Word.class, uuid);
        if (StringUtils.isNotNullOrEmpty(uuid) && word != null)
            wt.setTransWordId(word.getId());
        else
            throw new Exception("Unable to find back word: "+ uuid);

        return wt;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jo = new JSONObject();
        try {
            jo.put("uuid", getUuid());
            JSONObject attr = new JSONObject();
            attr.put("front1_uuid", getOrgWord1().getUuid());
            attr.put("front2_uuid", getOrgWord2().getUuid());
            attr.put("front3_uuid", getOrgWord3().getUuid());
            attr.put("back_uuid", getTranslWord().getUuid());

            attr.put("stage", getStage());
            attr.put("version", getVersion());
            attr.put("trashed", isTrashed());
            attr.put("created_at", DateHelper.dateToIso8601Z(getCreated_at()));

            jo.put("attributes", attr);
        } catch (JSONException e) {
            AppLog.e(TAG, "toJSON", e);
        }

        return jo;
    }

    public static WordTriple loadFromJSON(JSONObject jo) {
        try {

            String orgLang = jo.optString("lang");
            String orgText1 = jo.optString("org1");
            String orgText2 = jo.optString("org2");
            String orgText3 = jo.optString("org3");


            JSONObject jt = null;

            jt = jo.getJSONObject("translation");

            String transLang = jt.optString("lang");
            String transText = jt.optString("text");

            return new WordTriple(orgText1, orgText2, orgText3, transText,orgLang, transLang);

        } catch (JSONException e) {
            return null;
        }

    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public void doCorrectAnswer(boolean correct) {
        WordTripleHistory wth = new WordTripleHistory(correct, getId());
        wth.save();

    }


}
