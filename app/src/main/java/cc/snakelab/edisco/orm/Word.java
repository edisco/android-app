package cc.snakelab.edisco.orm;


import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.Table;

import com.raizlabs.android.dbflow.sql.language.OperatorGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.MiscHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by snake on 01.10.16.
 */
@Table(
        database = AppDatabase.class,
        indexGroups =
                {
                        @IndexGroup(number = 1, name = "wordTextIndex"),
                        @IndexGroup(number = 2, name = "wordLangIndex"),
                        @IndexGroup(number = 3, name = "wordPartIndex"),
                        @IndexGroup(number = 85, name = "wordTrashedIndex"),
                        @IndexGroup(number = 55, name = "wordUuidIndex"),
                        @IndexGroup(number = 65, name = "wordSyncedIndex"),
                        @IndexGroup(number = 75, name = "wordVersionIndex"),
                        @IndexGroup(number = 95, name = "wordCreatedAtIndex"),
                        @IndexGroup(number = 105, name = "wordUpdatedAtIndex")

                })
public class Word extends BasicRecord {

    final public static int NONE = 0;
    final public static int NOUN = 1;
    final public static int VERB = 2;
    final public static int ADJECTIVE = 3;
    final public static int ADVERB = 4;


    final public static int MASCULINE = 1;
    final public static int FEMININE = 2;
    final public static int NEUTER = 3;
    final public static int PLURAL = 4;


    final public static String VALID_TEXT = "Text";
    private static final String TAG = "Word";
    @Index(indexGroups = 1)
    @Column
    private String text;

    @Index(indexGroups = 2)
    @Column
    private String lang;
    @Index(indexGroups = 3)
    @Column(defaultValue = "0")
    private int part;

    @Column(defaultValue = "0")
    private int gender;







    private String transcription;
    private String description;

    private List<Word> pairTranslWordList;
    private List<Word> tripleTranslWordList;
    private List<WordPair> wordPairList;
    private List<WordTriple> wordTripleList;

    public Word() {
        super();
    }

    @Override
    public void load() {
        super.load();
        pairTranslWordList = null;
        tripleTranslWordList = null;
        wordPairList = null;
        wordTripleList = null;
    }

    public Word(String text, String lang) {
        super();
        setLang(lang);
        setText(text);
    }
    public Word(long id, String text, String lang) {
        this(text, lang);
        this.setId(id);
    }

    @Override
    public void assign(BasicRecord basicRecord) {
        super.assign(basicRecord);
        Word word = (Word)basicRecord;
        this.text = word.getText();
        this.lang = word.getLang();
        this.part = word.getPart();
        this.setTrashed(word.isTrashed());
        this.gender = word.getGender();

    }

    @Override
    public String toString() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String getTranscription() {
        return transcription;
    }

    public void setTranscription(String text){this.transcription = text; }

    public static Word findOrCreate(String text, String lang) {
        Word w = SQLite.select().from(Word.class).where(Word_Table.text.eq(text)).and(Word_Table.lang.eq(lang)).querySingle();
        if (w == null) {
            w = new Word(text, lang);
            w.save();
        }
        return w;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public List<Word> getPairTranslations() {
        return getPairTranslations(false);
    }
    public List<Word> getPairTranslations(boolean isTrashedIncluded) {
        if (pairTranslWordList == null || pairTranslWordList.isEmpty()) {
            pairTranslWordList = new ArrayList<>();
            List<WordPair> list = getWordPairList(isTrashedIncluded);
            for(int i = 0; i < list.size(); i++) {
                WordPair wp = list.get(i);
                if (wp.getOrgWordId() == this.getId() && wp.getTranslation() != null)
                    pairTranslWordList.add(wp.getTranslation());
                if (wp.getTranslWordId() == this.getId() && wp.getOriginal() != null)
                    pairTranslWordList.add(wp.getOriginal());

            }

        }
        return pairTranslWordList;
    }

    public List<Word> getTripleTranslations() {

        if (tripleTranslWordList == null || tripleTranslWordList.isEmpty()) {
            tripleTranslWordList = new ArrayList<>();

            List<WordTriple> list2 = getWordTripleList();
            for (WordTriple wt : list2) {
                if (wt.getOrgWord1Id() == getId() || wt.getOrgWord2Id() == getId() || wt.getOrgWord3Id() == getId())
                    tripleTranslWordList.add(wt.getTranslWord());
                else {
                    tripleTranslWordList.add(wt.getOrgWord1());
                    tripleTranslWordList.add(wt.getOrgWord2());
                    tripleTranslWordList.add(wt.getOrgWord3());
                }
            }
        }
        return tripleTranslWordList;
    }
    public List<WordTriple> getWordTripleList() {
        if (wordTripleList == null || wordTripleList.isEmpty()) {
            wordTripleList = SQLite.select().from(WordTriple.class)
                    .where(WordTriple_Table.orgWord1Id.eq(getId()))
                    .or(WordTriple_Table.orgWord2Id.eq(getId()))
                    .or(WordTriple_Table.orgWord3Id.eq(getId()))
                    .or(WordTriple_Table.transWordId.eq(getId()))
                    .queryList();
        }
        return wordTripleList;
    }

    //get all word pairs where current word is whether org_word_id or transl_word_id
    public List<WordPair> getWordPairList(boolean isTrashedIncluded) {
        if (wordPairList == null || wordPairList.isEmpty()) {
            Where<WordPair> where = SQLite.select().from(WordPair.class).
                    where(
                            (OperatorGroup.clause().and(WordPair_Table.orgWordId.eq(getId())).or(WordPair_Table.translWordId.eq(getId()))
                            ));
            if (!isTrashedIncluded)
                where = where.and(WordPair_Table.trashed.eq(false));


            wordPairList = where.queryList();
        }
        return wordPairList;
    }
    public List<WordPair> getWordPairList() {
        return getWordPairList(false);
    }

    public static List<Word> getList() {

        return SQLite.select().from(Word.class).where(Word_Table.trashed.eq(false)).orderBy(Word_Table.text, true).queryList();
    }

    public void trash() {
        setTrashed(true);
        setSynced(false);
        for(Word w: getPairTranslations()) {
            if (w.getPairTranslations(false).size() < 2) {
                w.setTrashed(true);
                w.setSynced(false);
                w.save();
            }
        }
        for(WordPair wp: getWordPairList())
        {
            wp.setTrashed(true);
            wp.setSynced(false);
            wp.save();
        }
        save();
    }

    public void untrash() {
        setTrashed(false);
        setSynced(false);
        for(Word w: getPairTranslations(true)) {
            w.setTrashed(false);
            w.setSynced(false);
            w.save();
        }
        for(WordPair wp: getWordPairList(true)) {
            wp.setTrashed(false);
            wp.setSynced(false);
            wp.save();
        }
        save();

    }

    public void addTranslation(String translation, String lang) {
        save();
        WordPair wp = new WordPair(getText(), translation, getLang(), lang);
        wp.save();
        wordPairList = null;
        pairTranslWordList = null;

    }


    public void addTranslations(List<Word> list) {
        for(Word word: list) {
            addTranslation(word.getText(), word.getLang());
        }
    }

    public void mergePairTranslations(List<Word> list, Reminder reminder) {
        save();
        List<Word> oldList = getPairTranslations();
        for(Word word: oldList)
            word.setTrashed(true);

        for(Word word: list) {
            if (!word.isValid())
                continue;
            Word oldWord = MiscHelper.findById(oldList, word.getId());
            //existed word
            if (oldWord != null) {
                oldWord.assign(word);
                oldWord.setTrashed(false);
            } else {
                //looking for existed word with same text and lang
                Word existedWord = SQLite.select().from(Word.class).where(Word_Table.text.eq(word.getText()))
                        .and(Word_Table.lang.eq(word.getLang())).querySingle();
                if (existedWord != null) {
                    word = existedWord;
                }

                //new word
                word.save();
                Word newWord = new Word();
                newWord.assign(word);
                oldList.add(newWord);
                WordPair wp = new WordPair(this, newWord);
                wp.save();

            }

        }
        //delete unused words
        for(Word word: oldList){
            if (word.isTrashed()) {
                word.trash();
                //check if this word is used in reminder, if so remove it from reminder
                reminder.trash(word);

            } else
                word.save();
        }
    }

    public int getRetention() {
        int retention = 0;
        for(WordPair wp: getWordPairList())
            retention = Math.max(retention, wp.calcRetention());
        for(WordTriple wt: getWordTripleList()) {
            retention = Math.max(retention, wt.calcRetention());
        }
        return retention;
    }



    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public static List<Word> findSimilarWords(String word, String lang, int wordAmount) {
        List<Word> list = SQLite.select().from(Word.class).where(Word_Table.text.like(word.charAt(0)+"%")).and(Word_Table.lang.eq(lang)).limit(wordAmount * 3).queryList();
        if (list.size() == 0)
            list = SQLite.select().from(Word.class).where(Word_Table.lang.eq(lang)).limit(wordAmount * 3).queryList();
        HashMap<Integer, Word> hash = new HashMap<>();
        for (Word w: list) {
            int i = MiscHelper.wordDistance(word, w.getText());
            hash.put(i, w);
        }


        List<Integer> intList = new ArrayList<>(hash.keySet());

        Collections.sort(intList, new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer.compareTo(t1);
            }
        });
        int i = 0;
        List<Word> retList = new ArrayList<>();
        while (i < wordAmount && i < intList.size()) {
            int index = intList.get(i);
            retList.add(hash.get(index));
            i++;
        }

        return retList;
    }




    public static Word fromJSON(JSONObject jsonObject) throws ParseException {
        Word word = new Word();
        word.setUuid(jsonObject.optString("uuid"));
        JSONObject attr = jsonObject.optJSONObject("attributes");
        word.setText(attr.optString("text"));
        word.setLang(attr.optString("lang"));
        word.setGender(attr.optInt("gender"));
        word.setPart(attr.optInt("part"));
        word.setVersion(attr.optInt("version"));
        word.setTrashed(attr.optBoolean("trashed"));
        word.setCreated_at(DateHelper.iso8601UtcToDate(attr.optString("created_at")));



        return word;
    }
    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", getUuid());
            JSONObject attr = new JSONObject();
            attr.put("text", getText());
            attr.put("lang", getLang());
            attr.put("gender", getGender());
            attr.put("part", getPart());
            attr.put("version", getVersion());
            attr.put("trashed", isTrashed());
            attr.put("created_at", DateHelper.dateToIso8601Z(getCreated_at()));
            jsonObject.put("attributes", attr);


        } catch (JSONException e) {

            AppLog.e(TAG, "toJSON", e);
        }
        return jsonObject;
    }


    @Override
    public boolean delete() {
        for(WordPair wp: getWordPairList(true)) {
            wp.delete();
        }
        for(WordTriple wt: getWordTripleList()) {
            wt.delete();
        }
        return super.delete();
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        if (StringUtils.isNullOrEmpty(getText().trim())) {
            addError(VALID_TEXT, App.app.getResources().getString(R.string.empty_validation));
            valid = false;
        }
        return valid;
    }

    public boolean isValidIgnoreEmpty() {
        if (getText().trim().isEmpty())
            return true;
        else
            return isValid();
    }
}
