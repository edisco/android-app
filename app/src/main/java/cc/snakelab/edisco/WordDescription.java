package cc.snakelab.edisco;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import android.support.v7.app.AppCompatActivity;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.firebase.analytics.FirebaseAnalytics;
import cc.snakelab.edisco.adapters.TranslationViewAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.Word;

public class WordDescription extends AppCompatActivity {
    static final int UPDATE_WORDPAIR_REQUEST = 2;
    private static final String TAG = "WordDescription";
    TextView mWordTextView;
    Word mWord;
    RecyclerView mRecycleView;
    TranslationViewAdapter mTranslWordAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int progress = 10;
    private NumberProgressBar pbHorizontal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.activity_word_description);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mWord = App.app.getCurrentWord();
        if (mWord == null)
            AppLog.d(TAG, "The word is null");

        //set transcription
        
        mWordTextView = (TextView) findViewById(R.id.current_word);
        mRecycleView = (RecyclerView) findViewById(R.id.translationWords);
        mRecycleView.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        mTranslWordAdapter = new TranslationViewAdapter(mWord);

        mRecycleView.setAdapter(mTranslWordAdapter);


        pbHorizontal = (NumberProgressBar) findViewById(R.id.progressBarLearnt);
        postProgress(progress);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EditWordActivity.class);
                startActivityForResult(intent, UPDATE_WORDPAIR_REQUEST);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (BuildConfig.DEBUG)
            getMenuInflater().inflate(R.menu.word_description, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_word_history:
                startActivity(new Intent(this, RecordViewerActivity.class));
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }
    private void updateUI() {
        mWord.load();
        mTranslWordAdapter.notifyDataSetChanged();
        mWordTextView.setText(String.format("%s (%s)", mWord.getText(), mWord.getLang()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_WORDPAIR_REQUEST && resultCode == RESULT_OK) {
            updateUI();
        }
    }

    private void postProgress(int progress)
    {
        pbHorizontal.setProgress(progress);
        //pbHorizontal.setProgressTextSize(R.dimen.Progressbar_text_size);
    }
}
