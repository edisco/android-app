package cc.snakelab.edisco;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceFragmentCompat;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.Word;

/**
 * Created by snake on 24.02.17.
 */

public class WordSettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final static String LANGUAGE = "language";
    private final static String PART = "part";
    private final static String GEN="gen";
    private ListPreference mLang;
    private ListPreference mPart;
    private ListPreference mGen;
    Word mWord;

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equalsIgnoreCase(LANGUAGE))
            mWord.setLang(LocaleHelper.stringToCodeLang(App.app, sharedPreferences.getString(key, "")));
        if(key.equalsIgnoreCase(PART))
            mWord.setPart(LocaleHelper.StringToWordPart(App.app, sharedPreferences.getString(key, "")));
        if(key.equalsIgnoreCase(GEN))
            mWord.setGender(LocaleHelper.StringToWordGender(App.app, sharedPreferences.getString(key, "")));
        updateUI();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.word_settings);
        mLang = (ListPreference) findPreference(LANGUAGE);
        mPart = (ListPreference) findPreference(PART);
        mGen = (ListPreference) findPreference(GEN);
        //gender - only for german
        if(!App.preference.getCurrentLearningLang().equalsIgnoreCase(getString(R.string.german))) {
            //remove gender preference
            getPreferenceScreen().removePreference(mGen);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        mLang.setValue(LocaleHelper.langCodeToString(App.app, mWord.getLang()));
        mPart.setValue(LocaleHelper.WordPartToString(App.app, mWord.getPart()));
        if (mGen != null)
            mGen.setValue(LocaleHelper.WordGenderToString(App.app, mWord.getGender()));
        updateUI();

    }

    public void putWord(Word word) {
        mWord = word;
    }

    private void updateUI() {
        if (mWord == null)
            return;
        mLang.setSummary(LocaleHelper.langCodeToString(App.app, mWord.getLang()));
        mPart.setSummary(LocaleHelper.WordPartToString(App.app, mWord.getPart()));
        if (mGen != null)
            mGen.setSummary(LocaleHelper.WordGenderToString(App.app, mWord.getGender()));


    }
}
