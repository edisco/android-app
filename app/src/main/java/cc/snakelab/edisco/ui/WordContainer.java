package cc.snakelab.edisco.ui;

import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import cc.snakelab.edisco.adapters.RecycleAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.MiscHelper;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;

import java.util.List;

/**
 * Created by snake on 24.01.17.
 */

public class WordContainer extends BasicWordUIContainer {
    private Word mWord;

    public WordContainer(Word word) {

        mWord = word;
    }

    @Override
    public String getHead() {
        return mWord.getText();
    }

    @Override
    public String getBody() {
        StringBuilder builder = new StringBuilder();

        List<Word> list = mWord.getPairTranslations();
        for(int i=0; i < list.size(); i++) {
            if (list.get(i).getText() != null)
                builder.append(String.format("%s, ", list.get(i).getText()));
        }


        if (builder.length() > 1)
            builder.delete(builder.length() - 2, builder.length());

        return builder.toString();
    }

    @Override
    public int getRetention() {

        return mWord.getRetention();
    }

    @Override
    public BasicRecord getRecord() {
        return mWord;
    }

    @Override
    public void onLongClick(Reminder reminder, TextView view, java.lang.Object adapter) {

        PopupMenu m = new PopupMenu(App.app.getMainActivityContext(), view);

        if (isUsedByReminder(reminder)) {

            for(WordPair wp: mWord.getWordPairList())
                reminder.excludeWordPair(wp);
            reminder.save();

            ((RecycleAdapter) adapter).notifyContainerChanged(this);
        }
        else
            if (buildMenu(m.getMenu(), adapter))
                m.show();
    }

    @Override
    public boolean isSynced() {
        return mWord.isSynced();
    }

    @Override
    public boolean buildMenu(Menu menu, java.lang.Object adapter) {
        final RecycleAdapter adpt= (RecycleAdapter) adapter;
        if (mWord.getWordPairList().size() > 0) {
            if (mWord.getWordPairList().size() == 1) {
                adpt.onWordPairSelected(mWord.getWordPairList().get(0), this);

            } else
            {
                for(final WordPair wp: mWord.getWordPairList()){

                    menu.add(Menu.NONE, Menu.NONE, Menu.NONE, wp.toString()).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            adpt.onWordPairSelected(wp, WordContainer.this);
                            return false;
                        }
                    });
                }
            }
        }
        return true;
    }


    @Override
    public void delete(Reminder reminder) {
        mWord.trash();
        reminder.trash(mWord);
    }

    @Override
    public boolean isUsedByReminder(Reminder reminder) {

        return reminder.isWordUsed(mWord);
    }



    public static void merge(List<BasicWordUIContainer> containerList, List<Word> list) {
        for(Word w: list) {
            if (w.getWordTripleList().size() == 0 && !MiscHelper.isContainRecord(containerList, w))
                containerList.add(new WordContainer(w));
        }
    }
}
