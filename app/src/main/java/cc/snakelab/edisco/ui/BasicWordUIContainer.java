package cc.snakelab.edisco.ui;

import android.view.Menu;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 24.01.17.
 */

public abstract class BasicWordUIContainer {
    public abstract String getHead();
    public abstract String getBody();

    public abstract boolean isUsedByReminder(Reminder reminder);

    public abstract int getRetention();

    public abstract void delete(Reminder reminder);
    public abstract BasicRecord getRecord();

    public abstract boolean buildMenu(Menu menu, java.lang.Object adapter);


    public abstract void onLongClick(Reminder reminder, TextView view, java.lang.Object adapter);
    public abstract boolean isSynced();

    public static void sort(List<BasicWordUIContainer> containerList, final int sortType, final boolean asc) {
        Collections.sort(containerList, new Comparator<BasicWordUIContainer>() {
            @Override
            public int compare(BasicWordUIContainer cont1, BasicWordUIContainer cont2) {

                switch (sortType){
                    case R.id.sort_by_date:
                        return asc ? cont1.getRecord().getCreated_at().compareTo(cont2.getRecord().getCreated_at()) : cont2.getRecord().getCreated_at().compareTo(cont1.getRecord().getCreated_at());
                    case R.id.sort_by_retention:
                        return asc ? cont1.getRetention() - cont2.getRetention() : cont2.getRetention() - cont1.getRetention();
                };
                return 0;
            };

        });

    }
}
