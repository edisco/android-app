package cc.snakelab.edisco.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.adapters.TrashAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;

/**
 * Created by snake on 05.04.17.
 */

public class WordTrashContainer extends BasicWordUIContainer implements PopupMenu.OnMenuItemClickListener {
    Word mWord;
    TrashAdapter mAdapter;
    Context mContext;
    public WordTrashContainer(Word w, TrashAdapter adapter, Context context) {
        mWord = w;
        mAdapter = adapter;
        mContext = context;
    }
    @Override
    public String getHead() {
        return mWord.getText();
    }

    @Override
    public String getBody() {
        return "";
    }

    @Override
    public boolean isUsedByReminder(Reminder reminder) {
        return false;
    }

    @Override
    public int getRetention() {
        return 0;
    }

    @Override
    public void delete(Reminder reminder) {

    }

    @Override
    public BasicRecord getRecord() {
        return mWord;
    }

    @Override
    public boolean buildMenu(Menu menu, Object adapter) {
        return true;
    }

    @Override
    public void onLongClick(Reminder reminder, TextView view, Object adapter) {
        PopupMenu m = new PopupMenu(mContext, view);
        m.inflate(R.menu.trash_item_menu);
        m.setOnMenuItemClickListener(this);
        m.show();
    }

    @Override
    public boolean isSynced() {
        return false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.recover_trash_item:
                mWord.untrash();
                mAdapter.update();
                break;
            case R.id.delete_trash_item:
                mWord.delete();
                mAdapter.update();

                break;


        }
        return false;
    }
}
