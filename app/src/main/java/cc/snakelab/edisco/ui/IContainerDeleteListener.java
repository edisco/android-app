package cc.snakelab.edisco.ui;

import cc.snakelab.edisco.orm.BasicRecord;

/**
 * Created by snake on 25.01.17.
 */
public interface IContainerDeleteListener {
    void onDelete(BasicRecord record);
}
