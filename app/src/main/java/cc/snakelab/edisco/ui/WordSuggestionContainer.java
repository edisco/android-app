package cc.snakelab.edisco.ui;

import android.view.Menu;
import android.widget.TextView;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 11.09.17.
 */

public class WordSuggestionContainer extends BasicWordUIContainer {
    private String mFrontText;
    private String mBackText;

    public WordSuggestionContainer(String frontText, String backText) {
        mFrontText = frontText;
        mBackText = backText;
    }

    @Override
    public String getHead() {
        return mBackText;
    }

    public String getTranslation() {
        return mBackText;
    }
    public String getOriginalText() {
        return mFrontText;
    }
    @Override
    public String getBody() {

        return "";
    }

    @Override
    public boolean isUsedByReminder(Reminder reminder) {
        return false;
    }

    @Override
    public int getRetention() {
        return 0;
    }

    @Override
    public void delete(Reminder reminder) {

    }

    @Override
    public BasicRecord getRecord() {
        return null;
    }

    @Override
    public boolean buildMenu(Menu menu, Object adapter) {
        return false;
    }

    @Override
    public void onLongClick(Reminder reminder, TextView view, Object adapter) {

    }

    @Override
    public boolean isSynced() {
        return false;
    }
}
