package cc.snakelab.edisco.ui;

import android.view.Menu;
import android.widget.TextView;

import cc.snakelab.edisco.adapters.RecycleAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.helper.MiscHelper;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordTriple;

import java.util.List;

/**
 * Created by snake on 01.03.17.
 */

public class WordTripleContainer extends BasicWordUIContainer {
    private WordTriple mWordTriple;
    public WordTripleContainer(WordTriple wordTriple){
        mWordTriple = wordTriple;
    }
    @Override
    public String getHead() {
        return String.format("%s, %s, %s", mWordTriple.getOrgWord1().getText(), mWordTriple.getOrgWord2().getText(), mWordTriple.getOrgWord3().getText());
    }

    @Override
    public String getBody() {
        return mWordTriple.getTranslWord().getText();
    }

    @Override
    public boolean isUsedByReminder(Reminder reminder) {
        return reminder.isWordUsed(mWordTriple.getOrgWord1());
    }

    @Override
    public int getRetention() {
        return mWordTriple.calcRetention();
    }

    @Override
    public void delete(Reminder reminder) {
        mWordTriple.trash();
        reminder.trash(mWordTriple);

    }

    @Override
    public BasicRecord getRecord() {
        return mWordTriple;
    }

    @Override
    public boolean buildMenu(Menu menu, Object adapter) {
        return false;
    }


    @Override
    public void onLongClick(Reminder reminder, TextView view, Object adapter) {
        if (isUsedByReminder(reminder)) {
            reminder.excludeWordTriple(mWordTriple);
        } else {
            reminder.includeWordTriple(mWordTriple);

        }
        reminder.save();
        ((RecycleAdapter) adapter).notifyContainerChanged(this);

    }

    @Override
    public boolean isSynced() {
        return mWordTriple.isSynced();
    }

    public static void merge(List<BasicWordUIContainer> containerList, List<Word> list) {
        String translLang = LocaleHelper.stringToCodeLang(App.app, App.preference.getLang());
        String orgLang = LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang());
        for(Word w: list) {
            for (WordTriple wt: w.getWordTripleList()) {
                if (
                        wt.getOrgWord1().getLang().equalsIgnoreCase(orgLang) &&
                        wt.getOrgWord2().getLang().equalsIgnoreCase(orgLang) &&
                        wt.getOrgWord3().getLang().equalsIgnoreCase(orgLang) &&
                        wt.getTranslWord().getLang().equalsIgnoreCase(translLang) &&
                        !MiscHelper.isContainRecord(containerList, wt))
                    containerList.add(new WordTripleContainer(wt));
            }
        }
    }
}
