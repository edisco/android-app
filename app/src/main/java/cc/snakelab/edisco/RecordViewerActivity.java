package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import cc.snakelab.edisco.orm.WordPairHistory;

/**
 * Created by snake on 12.04.17.
 */

public class RecordViewerActivity extends AppCompatActivity {
    RecordViewerFragment mFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        String klass = getIntent().getStringExtra("record_class");
        Class klass = WordPairHistory.class;
        setContentView(R.layout.activity_record_viewer);
        mFragment = (RecordViewerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        mFragment.putKlass(klass);
    }
}
