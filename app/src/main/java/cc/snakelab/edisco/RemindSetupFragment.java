package cc.snakelab.edisco;

import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.Preference;
import android.util.Log;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 21.02.17.
 */

public class RemindSetupFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final static String remind_start_hours = "remind_start_hours";
    private final static String remind_finish_hours = "remind_finish_hours";
    private final static String remind_active = "remind_active";
    private final static String remind_period = "remind_period";
    private final static String remind_game = "remind_game";
    ListPreference mStartHour;
    ListPreference mFinishHour;
    ListPreference mPeriod;
    SwitchPreference mActive;
    SwitchPreference mPlayGame;
    Preference mWords;
    Reminder mReminder;
    OnUpdateListiner mListiner;



    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        mStartHour.setValue(mReminder.getStartHour()+"");
        mFinishHour.setValue(mReminder.getFinishHour()+"");
        mActive.setChecked(mReminder.isActive());
        mPlayGame.setChecked(mReminder.isPlayGame());
        mWords.setSummary(mReminder.getLearningWordString());
        mPlayGame.setVisible(BuildConfig.DEBUG);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.remind_settings);
        mStartHour = (ListPreference) findPreference(remind_start_hours);

        mFinishHour = (ListPreference) findPreference(remind_finish_hours);
        mActive = (SwitchPreference) findPreference(remind_active);
        mPlayGame = (SwitchPreference) findPreference(remind_game);

        mWords = findPreference("remind_words");
        mReminder = App.app.getReminder();
        mPeriod = (ListPreference) findPreference(remind_period);
        updateUI();

    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        Log.d("", "");
    }

    private void updateUI() {
        mStartHour.setSummary(mReminder.getStartHour() + "");
        mFinishHour.setSummary(mReminder.getFinishHour()+ "");
        mPeriod.setSummary(LocaleHelper.PeriodToSettingsPeriod(mReminder.getPeriod()));
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences preferences, String key) {
        if(key.equalsIgnoreCase(remind_start_hours))
            mReminder.setStartHour(Integer.parseInt(preferences.getString(key, mReminder.getStartHour()+"")));
        if(key.equalsIgnoreCase(remind_finish_hours))
            mReminder.setFinishHour(Integer.parseInt(preferences.getString(key, mReminder.getFinishHour()+"")));
        if(key.equalsIgnoreCase(remind_active))
            mReminder.setActive(preferences.getBoolean(key, mReminder.isActive()));
        if(key.equalsIgnoreCase(remind_period))
            mReminder.setPeriod(LocaleHelper.SettingsPeriodToPeriod(preferences.getString(key, "")));
        if(key.equalsIgnoreCase(remind_game))
            mReminder.setPlayGame(preferences.getBoolean(key, mReminder.isPlayGame()));
        mReminder.save();
        mListiner.onUpdate();
        updateUI();
    }
    public void setListiner(OnUpdateListiner listiner) {
        mListiner = listiner;
    }
    public interface OnUpdateListiner {
        void onUpdate();
    }
}
