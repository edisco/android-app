package cc.snakelab.edisco;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.firebase.analytics.FirebaseAnalytics;
import cc.snakelab.edisco.api.Api;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.api.EdiscoApi;

import org.json.JSONException;

import java.net.ConnectException;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, Api.ApiResultListener {

    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 9001;
    private String TAG = "LoginActivity";
    private EdiscoApi mApi;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mApi = new EdiscoApi(this, App.preference.getApiServerUrl(), "", BuildConfig.EDISCO_TEST_TOKEN);
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestId()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        setContentView(R.layout.activity_login);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        if (App.preference.isSingedIn())
            finish();
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // ApiResult returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            App.preference.setEmail(acct.getEmail());
            if (acct.getPhotoUrl() != null)
                App.preference.setProfilePhotoUrl(acct.getPhotoUrl().toString());
            mApi.signIn(acct.getEmail(), acct.getIdToken());
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//            startMainActivity(true);
        } else {
            // Signed out, show unauthenticated UI.
            if (result.getStatus() != null) {
                AppLog.d(TAG, result.getStatus().toString());
                App.app.toast(getString(R.string.unableToSignInCode, result.getStatus().getStatusCode()));
            } else {
                AppLog.d(TAG, "unable to sign in");
                App.app.toast(getString(R.string.unableToSignIn));

            }
        }
    }

    private void startMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onResponse(Api.ApiResult response) {
        if (response.getMethodId() == EdiscoApi.SIGN_IN) {
            if (response.isOk()) {
                String token = null;
                boolean new_user=false;
                try {
                    token = response.getJson().getJSONObject("meta").optString("token", null);

                } catch (JSONException e) {
                    AppLog.e(TAG, "unable to parse json for token", e);
                }
                try {
                    new_user = response.getJson().getJSONObject("meta").optBoolean("new", false);
                } catch (JSONException e) {
                    AppLog.e(TAG, "unable to parse json for new", e);
                }

                App.preference.setApiToken(token);
                App.preference.setNewUser(new_user);
                startMainActivity();
            } else {
                AppLog.e(TAG, "Unable to sign in", response.getException());
                if (response.getException() instanceof ConnectException || response.getException() instanceof com.android.volley.TimeoutError)
                    App.app.toast(getString(R.string.unableToConnectCloud));
                else
                    App.app.toast(getString(R.string.unableToSignInCode, 404));
            }
        }

    }
}
