package cc.snakelab.edisco.helper;


import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;

/**
 * Created by snake on 01.11.16.
 */

public class DateHelper {

    public static Calendar getCalendar() {
        return GregorianCalendar.getInstance();

    }
    public static Calendar getUTCCalendar(){
        Calendar utcCalendar = GregorianCalendar.getInstance();
        utcCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utcCalendar;
    }

    public static int getUTCHours(Date date) {
        Calendar calendar = DateHelper.getUTCCalendar();
        calendar.setTime(date);   // assigns calendar to given date
        return calendar.get(Calendar.HOUR_OF_DAY);
    }
    public static int getUTCMinutes(Date date) {
        Calendar calendar = DateHelper.getUTCCalendar();
        calendar.setTime(date);   // assigns calendar to given date
        return calendar.get(Calendar.MINUTE);
    }

    public static int getHours(Date date) {
        Calendar calendar = DateHelper.getCalendar();
        calendar.setTime(date);   // assigns calendar to given date
        return calendar.get(Calendar.HOUR_OF_DAY);
    }
    public static int getMinutes(Date date) {
        Calendar calendar = DateHelper.getCalendar();
        calendar.setTime(date);   // assigns calendar to given date
        return calendar.get(Calendar.MINUTE);
    }

    public static Date convertToTimeZone(Date date, String timeZone) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getTimeZone(timeZone));
        return cal.getTime();

    }

    public static Date iso8601ToDate(String txt) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return df.parse(txt);
    }
    public static String dateToIso8601(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return df.format(date);
    }

    public static Date iso8601ZToDate(String txt) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");

        return df.parse(txt);
    }

    public static String dateToIso8601Z(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
        return df.format(date);
    }
    public static Date iso8601UtcToDate(String txt) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.parse(txt);
    }

    public static Date DBIso8601ToDate(String txt) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.parse(txt);
    }

    public static String dateToDBIso8601(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);
    }


    public static int getDaysBetween(Date date1, Date date2) {
        return Math.round(Math.abs(date1.getTime() - date2.getTime()) / (24*60*60*1000));
    }

    public static String getReadableFormat(Date datetime) {
        return getReadableFormat(datetime, getCalendar().getTime(), App.app);
    }
    public static String getReadableFormat(Date datetime, Date now, Context context) {
        Calendar cal = getCalendar();
        Calendar nowCal = getCalendar();
        nowCal.setTime(now);
        cal.setTime(datetime);
        if (cal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR) && cal.get(Calendar.DAY_OF_YEAR) == nowCal.get(Calendar.DAY_OF_YEAR))
            return context.getString(R.string.today);
        //yday
        cal.add(Calendar.DATE, +1);
        if (cal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR) && (cal.get(Calendar.DAY_OF_YEAR)) == nowCal.get(Calendar.DAY_OF_YEAR))
            return context.getString(R.string.yesterday);
//        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        DateFormat df = new SimpleDateFormat("MMM d");
        return df.format(datetime);
    }
}
