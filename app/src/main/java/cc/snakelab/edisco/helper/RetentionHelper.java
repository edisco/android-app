package cc.snakelab.edisco.helper;


import java.util.Date;

/**
 * Created by snake on 10.03.17.
 */

public class RetentionHelper {

    public static final int STAGE_0 = 0; //t=0
    public static final int STAGE_2 = 1; //t=2
    public static final int STAGE_14 = 2; //t=14
    public static final int STAGE_60 = 3; //t=60

    public static int getRetention(Date currentDate, int stage, Date lastCorrect) {
        if (stage == STAGE_0)
            return 0;
        else
            return (int) (Math.exp(-getT(currentDate, lastCorrect)/getS(stage))*100);
    }

    private static double getS(int stage) {
        switch (stage) {
            case STAGE_0: return 1;
            case STAGE_2: return 1.804;
            case STAGE_14: return 12.628;
            case STAGE_60: return 54.119;
            default: return 1;
        }
    }

    private static int getT(Date currentDate, Date lastCorrect) {
//        WordPairHistory wph = getHistory().and(WordPairHistory_Table.correct.eq(true)).orderBy(WordPairHistory_Table.created_at, false).querySingle();
//        //WordPairHistory.where(WordPairHistory.class, "word_pair_id = "+getId()+" and correct = 1", null, null, "created_at desc");
//        if (wph != null) {
//            return DateHelper.getDaysBetween(date, wph.getCreated_at());
//        }

        return DateHelper.getDaysBetween(currentDate, lastCorrect);

    }
    public static int nextStage(int stage) {
        switch (stage) {
            case RetentionHelper.STAGE_0: {return RetentionHelper.STAGE_2;}
            case RetentionHelper.STAGE_2: {return RetentionHelper.STAGE_14;}
            case RetentionHelper.STAGE_14: {return RetentionHelper.STAGE_60;}
            case RetentionHelper.STAGE_60: {return RetentionHelper.STAGE_60;}
            default: return (STAGE_0);

        }
    }

}
