package cc.snakelab.edisco.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.Word;

import java.util.Locale;

/**
 * Created by shen on 14.01.2017.
 */

public class LocaleHelper {
    //    https://uk.wikipedia.org/wiki/ISO_639
    final public static String RU ="ru";
    final public static String UK ="uk";
    final public static String EN ="en";
    final public static String DE ="de";

    final public static String TAG="LocaleHelper";

    public void setAppLocale(Context context, String langMark)
    {
        Locale current = getCurrentLocale(context);
        Locale choosen = new Locale(langMark);

        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocale(choosen);
            res.updateConfiguration(conf, dm);
        }
        else {
            conf.locale = choosen;
        }

    }



    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getCurrentLocale(Context contex){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return contex.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            return contex.getResources().getConfiguration().locale;
        }
    }
    public static String getCurrentLang(Context context) {
        return getCurrentLocale(context).getLanguage();
    }

    public static String stringToCodeLang(Context context, String lang)  {
        if (context.getResources().getString(R.string.english).equalsIgnoreCase(lang))
            return EN;
        if (context.getResources().getString(R.string.russian).equalsIgnoreCase(lang))
            return RU;
        if (context.getResources().getString(R.string.ukrainian).equalsIgnoreCase(lang))
            return UK;
        if (context.getResources().getString(R.string.german).equalsIgnoreCase(lang))
            return DE;
        AppLog.e(TAG, "stringToCodeLang lang="+lang, new Exception("Not implemented"));
        return EN;

    }

    public static String langCodeToString(Context context, String lang) {
        if (EN.equalsIgnoreCase(lang))
            return context.getResources().getString(R.string.english);
        if (RU.equalsIgnoreCase(lang))
            return context.getResources().getString(R.string.russian);
        if (UK.equalsIgnoreCase(lang))
            return context.getResources().getString(R.string.ukrainian);
        if (DE.equalsIgnoreCase(lang))
            return context.getResources().getString(R.string.german);
        AppLog.e(TAG, "langCodeToString lang="+lang, new Exception("Not implemented"));
        return context.getResources().getString(R.string.english);
    }

    public static String WordPartToString(Context context, int part) {
        switch (part) {
            case Word.NONE: return context.getResources().getString(R.string.none);
            case Word.NOUN: return context.getResources().getString(R.string.noun);
            case Word.ADVERB: return context.getResources().getString(R.string.adverb);
            case Word.ADJECTIVE: return context.getResources().getString(R.string.adjective);
            case Word.VERB: return context.getResources().getString(R.string.verb);
            default: return context.getResources().getString(R.string.none);
        }
    }
    public static int StringToWordPart(Context context, String text) {
        if  (context.getResources().getString(R.string.none).equalsIgnoreCase(text))
            return Word.NONE;
        if  (context.getResources().getString(R.string.noun).equalsIgnoreCase(text))
            return Word.NOUN;
        if  (context.getResources().getString(R.string.verb).equalsIgnoreCase(text))
            return Word.VERB;
        if  (context.getResources().getString(R.string.adverb).equalsIgnoreCase(text))
            return Word.ADVERB;
        if  (context.getResources().getString(R.string.adjective).equalsIgnoreCase(text))
            return Word.ADJECTIVE;
        return Word.NONE;
    }

    public static int StringToWordGender(Context context, String text) {
        if  (context.getResources().getString(R.string.noun).equalsIgnoreCase(text))
            return Word.NOUN;
        if  (context.getResources().getString(R.string.der).equalsIgnoreCase(text))
            return Word.MASCULINE;
        if  (context.getResources().getString(R.string.die).equalsIgnoreCase(text))
            return Word.FEMININE;
        if  (context.getResources().getString(R.string.das).equalsIgnoreCase(text))
            return Word.NEUTER;
        if  (context.getResources().getString(R.string.die_plural).equalsIgnoreCase(text))
            return Word.PLURAL;
        return Word.NONE;
    }
    public static String WordGenderToString(Context context, int gender) {
        switch (gender) {

            case Word.MASCULINE: return context.getResources().getString(R.string.der);
            case Word.FEMININE: return context.getResources().getString(R.string.die);
            case Word.NEUTER: return context.getResources().getString(R.string.das);
            case Word.PLURAL: return context.getResources().getString(R.string.die_plural);
            default: return context.getResources().getString(R.string.none);
        }
    }

    public static int SettingsPeriodToPeriod(String text) {
        if  (App.app.getResources().getString(R.string.remind_period_15).equalsIgnoreCase(text))
            return 15;
        if  (App.app.getResources().getString(R.string.remind_period_30).equalsIgnoreCase(text))
            return 30;
        if  (App.app.getResources().getString(R.string.remind_period_45).equalsIgnoreCase(text))
            return 45;
        if  (App.app.getResources().getString(R.string.remind_period_60).equalsIgnoreCase(text))
            return 60;
        if  (App.app.getResources().getString(R.string.remind_period_90).equalsIgnoreCase(text))
            return 90;
        if  (App.app.getResources().getString(R.string.remind_period_120).equalsIgnoreCase(text))
            return 120;
        if  (App.app.getResources().getString(R.string.remind_period_240).equalsIgnoreCase(text))
            return 240;

        return 90;
    }

    public static String PeriodToSettingsPeriod(int period) {
        switch (period) {
            case 15: return App.app.getResources().getString(R.string.remind_period_15);
            case 30: return App.app.getResources().getString(R.string.remind_period_30);
            case 45: return App.app.getResources().getString(R.string.remind_period_45);
            case 60: return App.app.getResources().getString(R.string.remind_period_60);
            case 90: return App.app.getResources().getString(R.string.remind_period_90);
            case 120: return App.app.getResources().getString(R.string.remind_period_120);
            case 240: return App.app.getResources().getString(R.string.remind_period_240);
            default: return App.app.getResources().getString(R.string.remind_period_90);
        }
    }
}
