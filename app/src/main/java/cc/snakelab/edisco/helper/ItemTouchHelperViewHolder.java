package cc.snakelab.edisco.helper;


public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}

