package cc.snakelab.edisco.helper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.api.EdiscoApi;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.AppDatabase;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTripleHistory;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.recievers.SyncReceiver;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by snake on 23.12.16.
 */

public class AppHelper {
    private static final String TAG = "AppHelper";

    public static void toggleSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

//        if (!imm.isActive())
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        imm.showSoftInput(view,InputMethodManager.SHOW_FORCED);
    }
    public static void showSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view.requestFocus())
            imm.showSoftInput(view,InputMethodManager.SHOW_IMPLICIT);
    }
    public static boolean isTest() {
        boolean result;
        try {
            Class.forName("cc.snakelab.edisco.main.EmulatorTest");

            result = true;
        } catch (final Exception e) {
            result = false;
        }

        try {
            Class.forName("android.support.test.espresso.Espresso");
            result = result || true;
        } catch (final Exception e) {
        }
        AppLog.d(TAG, "isTest="+result);
        return result;
    }
    public static String getDBFilename() {
        return isTest() ? "test_" + AppDatabase.NAME : AppDatabase.NAME;
    }
    public static void focusAndKeyboard(View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) App.app.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }
    public static void delayedFocusAndKeyboard(final View view) {
        view.requestFocus();
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                AppHelper.showSoftInput(view);
            }
        }, 200);
    }

    public static void hideSoftInput(AppCompatActivity activity) {
        if (activity == null)
            return;
        View view =activity.getCurrentFocus();
        if(view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Account createSyncAccount(Context context) {
        Account acc = new Account("Cloud", context.getString(R.string.accountType));
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */

        if (accountManager.addAccountExplicitly(acc, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            AppLog.d(TAG, "added account");
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
            AppLog.d(TAG, "unable to added account");
        }

        return acc;
    }

    public static void sync(Context context) {
        // Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */

        ContentResolver.requestSync(createSyncAccount(context), context.getString(R.string.authorityProvider), settingsBundle);
//        context.getContentResolver().notifyChange(Uri.parse("content://" + context.getString(R.string.authorityProvider) + "/"), null, false);
    }
    public static int Dp2Px(int dp) {
        final float scale = App.app.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) App.app.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static void db2cloud() {

        for(BasicRecord record: SQLite.select().from(Word.class).queryList())
            record.setCloudNew();
        for(BasicRecord record: SQLite.select().from(WordPair.class).queryList())
            record.setCloudNew();
        for(BasicRecord record: SQLite.select().from(WordTriple.class).queryList())
            record.setCloudNew();
        for(BasicRecord record: SQLite.select().from(WordPairHistory.class).queryList())
            record.setCloudNew();
        for(BasicRecord record: SQLite.select().from(WordTripleHistory.class).queryList())
            record.setCloudNew();
    }
    public static String syncParams2Str(Bundle params) {
        int num = params.getInt(SyncReceiver.RECORDS_NUM, -1);
        int id = params.getInt(SyncReceiver.API_METHOD, -1);

        switch (id) {
            case EdiscoApi.GET_USER_WORDS:
                return App.app.getString(R.string.sync_get_user_words, num);
            case EdiscoApi.GET_USER_WORD_PAIRS:
                return App.app.getString(R.string.sync_get_user_word_pairs, num);
            case EdiscoApi.GET_USER_WORD_TRIPLES:
                return App.app.getString(R.string.sync_get_user_word_triples, num);
            case EdiscoApi.GET_USER_WORD_PAIR_HISTORIES:
                return App.app.getString(R.string.sync_get_user_word_pair_histories, num);
            case EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES:
                return App.app.getString(R.string.sync_get_user_word_triple_histories, num);
        }
        return "...";

    }
}
