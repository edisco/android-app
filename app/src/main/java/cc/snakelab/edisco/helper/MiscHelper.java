package cc.snakelab.edisco.helper;

import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordContainer;
import cc.snakelab.edisco.ui.WordTripleContainer;

import java.util.List;

/**
 * Created by snake on 15.02.17.
 */

public class MiscHelper {
    public static <T extends BasicRecord> T findById(List<T> list, long id) {
        for(T record: list) {
            if (record.getId() == id)
                return  record;
        }
        return null;
    }
    public static int wordDistance(String S1, String S2){
        int m = S1.length(), n = S2.length();
        int[] D1;
        int[] D2 = new int[n + 1];

        for(int i = 0; i <= n; i ++)
            D2[i] = i;

        for(int i = 1; i <= m; i ++) {
            D1 = D2;
            D2 = new int[n + 1];
            for(int j = 0; j <= n; j ++) {
                if(j == 0) D2[j] = i;
                else {
                    int cost = (S1.charAt(i - 1) != S2.charAt(j - 1)) ? 1 : 0;
                    if(D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost)
                        D2[j] = D2[j - 1] + 1;
                    else if(D1[j] < D1[j - 1] + cost)
                        D2[j] = D1[j] + 1;
                    else
                        D2[j] = D1[j - 1] + cost;
                }
            }
        }
        return D2[n];

    }

    public static String getSimilarChars(String word, String lang, int wordAmount) {
        List<Word> list = Word.findSimilarWords(word, lang, wordAmount);
        String ret = word;
        for(Word w: list)
            for (int i = 0; i < w.getText().length(); i++)
                if(ret.indexOf(w.getText().charAt(i)) == -1)
                    ret += w.getText().charAt(i);
        return ret;
    }

    public static boolean isContainRecord(List<BasicWordUIContainer> list, BasicRecord record) {
        for(BasicWordUIContainer c: list) {
            if (c.getRecord().getClass().equals(record.getClass()) && c.getRecord().getId() == record.getId() )
                return true;
        }
        return false;

    }
}
