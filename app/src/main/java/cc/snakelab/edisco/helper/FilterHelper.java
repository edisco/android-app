package cc.snakelab.edisco.helper;

import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.OperatorGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.structure.database.FlowCursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.AppDatabase;
import cc.snakelab.edisco.orm.BasicRecord;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordContainer;
import cc.snakelab.edisco.ui.WordSuggestionContainer;
import cc.snakelab.edisco.ui.WordTripleContainer;

/**
 * Created by snake on 15.06.17.
 */

public class FilterHelper {
    private static String TAG="FilterHelper";

    public static int fetchContainers(List<BasicWordUIContainer> list, String currentQuery, int offset, int limit ) {
        list.clear();
        List<Word> wordList;
        do  {
            AppLog.d(TAG, String.format("'%s': offset=%d, limit=%d", currentQuery, offset, limit));
            wordList = query(currentQuery, offset, limit);
            WordContainer.merge(list, wordList);
            WordTripleContainer.merge(list, wordList);
            if (App.preference.getSortingTypeId() ==  R.id.sort_by_date)
                BasicWordUIContainer.sort(list, R.id.sort_by_date, !App.preference.isSortingDesc());

            AppLog.d(TAG, "list.size="+list.size());
            offset += limit;
        } while (list.size() < limit && wordList.size() != 0);


        AppLog.d(TAG, String.format("wordList.size=%d", wordList.size()));
        return offset;
    }
    public static void addSuggestionIfNeeded(List<BasicWordUIContainer> list, String frontText, String backText) {
        boolean inList = false;
        if (StringUtils.isNullOrEmpty(frontText) || StringUtils.isNullOrEmpty(backText) ||
                frontText.equalsIgnoreCase(backText))
            return;
        for(BasicWordUIContainer cont: list)
            if (cont instanceof WordSuggestionContainer && cont.getHead().equalsIgnoreCase(App.app.getString(R.string.suggestion_word_text, frontText, backText)))
                inList = true;

        if (!inList) {
            AppLog.d(TAG, String.format("Added suggestion ", frontText, backText));
            list.add(new WordSuggestionContainer(frontText, backText));
        }
    }

    private static List<Word> querySortByDate(String search, int offset, int limit, String lang, String nativeLang) {
        Where<Word> where = SQLite.select().from(Word.class).where(Word_Table.trashed.eq(false));

        if (StringUtils.isNotNullOrEmpty(search))
            where = where.and(OperatorGroup.clause().and(Word_Table.lang.eq(lang))
                    .or(Word_Table.lang.eq(nativeLang))).and(Word_Table.text.like(search+"%"));
        else
            where = where.and(Word_Table.lang.eq(lang));
        where = where.orderBy(Word_Table.created_at, !App.preference.isSortingDesc());
        return where.offset(offset).limit(limit).queryList();
    }
    private static List<Word> querySortByRetention(String search, int offset, int limit, String lang, String nativeLang, boolean descSort) {
        FlowCursor cur = FlowManager.getWritableDatabase(AppDatabase.class).rawQuery(
                "select Word.id, Word.text, coalesce(wt.retention, wp.retention, -10000) as retention1 from Word \n" +
                "left join \n" +
                "\t(\n" +
                "\t\tselect WordTriple.id, orgWord1Id, orgWord2Id, orgWord3Id, transWordId, last_correct, (strftime(\"%s\", 'now') - strftime(\"%s\", last_correct)) / 60/60/24/-(stage + 1.0) as retention from WordTriple\t\t\t\t\t\t\t\t\t\t\n" +
                "\t\tleft join (\t\t\t\t\t\t\t\t\t\t\n" +
                "\t\t\tselect word_triple_id, max(created_at) as last_correct from WordTripleHistory where correct = 1 group by word_triple_id) \n" +
                "\t\tas wth \ton wth.word_triple_id = WordTriple.id\t\t\t\t\t\t\t\t\t\n" +
                ") as wt on (wt.orgWord1Id = Word.id or wt.orgWord2Id = Word.id or wt.orgWord3Id = Word.id or wt.transWordId = Word.id)\n" +
                "left join (\n" +
                "\tselect WordPair.id, WordPair.orgWordId, WordPair.translWordId, last_correct, (strftime(\"%s\", 'now') - strftime(\"%s\", last_correct)) / 60/60/24/-(stage + 1.0) as retention from WordPair\n" +
                "\tleft join (\n" +
                "\t\tselect word_pair_id, max(created_at) as last_correct from WordPairHistory where correct = 1 group by word_pair_id\n" +
                "\t) as wph on wph.word_pair_id = WordPair.id\n" +
                ") as wp on (wp.orgWordId = Word.id or wp.translWordId = Word.id)\n" +
                "where Word.trashed = 0 \n" +
                " and (Word.lang = \""+lang+"\""+
                        (StringUtils.isNotNullOrEmpty(search)? " or Word.lang = \"" + nativeLang+"\"": "")+
                        ") "+
                        (StringUtils.isNullOrEmpty(search) ? "" : "and Word.text like \"" + search + "%\" ") +
                "group by Word.id having retention1 = max(retention1) order by retention1 " + (descSort? "desc" : "asc") + ", Word.id "+
                "limit " + limit+" offset "+offset
                , null

        );
        return cursor2List(cur, Word.class);
    }

    public static List<WordPair> querySortByRetentionWP(String search, int offset, int limit, String lang, String andWhere, boolean descSort) {
        FlowCursor cur = FlowManager.getWritableDatabase(AppDatabase.class).rawQuery(
                "select WordPair.id, WordPair.orgWordId, WordPair.translWordId, last_correct, coalesce((strftime(\"%s\", 'now') - strftime(\"%s\", last_correct)) / 60/60/24/-(stage + 1.0), -100000) as retention, \n" +
                " Word.text, Word.lang" +
                " from WordPair " +
                " left join (select word_pair_id, max(created_at) as last_correct from WordPairHistory where correct = 1 group by word_pair_id) as wph on wph.word_pair_id = WordPair.id " +
                " left join Word on WordPair.orgWordId = Word.id" +

                " where WordPair.trashed = 0 " +
                (StringUtils.isNotNullOrEmpty(search)? " and Word.text like \""+search+"%\"": "")+
                " and Word.lang = \"" + lang + "\" " +
                " and " +andWhere +
                " order by retention " + (descSort? "desc" : "asc")
               + " limit " + limit+" offset "+offset
                ,
                null);
        return cursor2List(cur, WordPair.class);
    }

    public static List<WordTriple> querySortByRetentionWT(String search, int offset, int limit, String lang, String andWhere, boolean descSort) {
        FlowCursor cur = FlowManager.getWritableDatabase(AppDatabase.class).rawQuery(
                "select WordTriple.id, orgWord1Id, orgWord2Id, orgWord3Id, transWordId, last_correct, coalesce((strftime(\"%s\", 'now') - strftime(\"%s\", last_correct)) / 60/60/24/-(stage + 1.0), -100000) as retention \n" +
                        " , Word.id, Word.text\n" +
                        "from WordTriple\n" +
                        "\n" +
                        "left join (\n" +
                        "\tselect word_triple_id, max(created_at) as last_correct from WordTripleHistory where correct = 1 group by word_triple_id)\n" +
                        "as wth \ton wth.word_triple_id = WordTriple.id\n" +
                        "left join Word on Word.id = orgWord1Id\n" +
                        " where WordTriple.trashed = 0 " +
                        (StringUtils.isNotNullOrEmpty(search)? " and Word.text like \""+search+"%\"": "")+
                        " and Word.lang = \"" + lang + "\" " +
                        " and " +andWhere +
                        " order by retention " + (descSort? "desc" : "asc")
                        + " limit " + limit+" offset "+offset
                ,
                null);
        return cursor2List(cur, WordTriple.class);
    }


    private static <T extends BasicRecord> List<T> cursor2List(FlowCursor cur, Class<T> klas) {
        cur.moveToFirst();
        List<T> list = new ArrayList<>();
        while (!cur.isAfterLast()) {
            list.add(BasicRecord.findById(klas, cur.getInt(0)));
            cur.moveToNext();
        }
        return list;
    }

    public static List<Word> query(String search, int offset, int limit) {
        String lang = LocaleHelper.stringToCodeLang(App.app, App.preference.getCurrentLearningLang());
//        String lang = App.preference.getCurrentLearningLang();
        String nativeLang = LocaleHelper.stringToCodeLang(App.app, App.preference.getLang());
        int sorId = App.preference.getSortingTypeId();
        switch (sorId) {
            case R.id.sort_by_date:
                return querySortByDate(search, offset, limit, lang, nativeLang);

            case R.id.sort_by_retention:
                return querySortByRetention(search, offset, limit, lang, nativeLang, App.preference.isSortingDesc());

        }
        return querySortByDate(search, offset, limit, lang, nativeLang);

    }
    public static void sortByRetention(List<Word> list) {

        Collections.sort(list, new Comparator<Word>() {
            @Override
            public int compare(Word word1, Word word2) {

                if (App.preference.isSortingDesc())
                    return (int) ((word2.getRetention() - word1.getRetention()) * 1000  - word2.getId() + word1.getId());
                else
                    return (int) ((word1.getRetention() - word2.getRetention()) * 1000 + word1.getId() - word2.getId());

            }
        });
    }

    public static String IdsToStr(List<Long> ids) {
        String grp = "(";
        for(Long i : ids) {
            grp = grp + i + ",";
        }
        grp = grp.substring(0, grp.length() - 1);
        grp = grp + ")";
        return grp;
    }
}
