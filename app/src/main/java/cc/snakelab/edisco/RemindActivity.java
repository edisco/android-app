package cc.snakelab.edisco;


import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.dialogs.GuessCorrectDialogFragment;
import cc.snakelab.edisco.dialogs.GuessHelpDialogFragment;
import cc.snakelab.edisco.dialogs.GuessIncorrectDialogFragment;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 03.11.16.
 */

public class RemindActivity extends AppCompatActivity implements RemindFragment.IOnGuessListener, GuessHelpDialogFragment.GuessHelpListener, GuessIncorrectDialogFragment.GuessIncorrectDialogListener,  GuessCorrectDialogFragment.GuessDialogListener, View.OnClickListener {
    private static final String TAG = "RemindActivity";
    private FirebaseAnalytics mFirebaseAnalytics;
    private AdView mAdView;


    Button checkButton;
    Button helpButton;
    Reminder reminder;
    App app;
    int mMaxAttempts = 3;
    int mAttempt = 1;
    RemindFragment mRemindFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(R.layout.activity_remind);
        reminder = App.app.getReminder();

        App.app.initSpeak(reminder.getGuessWord(0).getLang());

        if (!reminder.isWordSet()) {
            App.app.toast(getString(R.string.no_words_for_learning));
            finish();
        }

        if (reminder.isPlayGame() && reminder.isGuessPair())
            mRemindFragment = new RemindHtmlFragment();
        else {
            if (reminder.isGuessPair())
                mRemindFragment = new RemindWordPairFragment();
            else {
                if (!reminder.isGuessFront())
                    mRemindFragment = new RemindWordTripleTranslFragment();
                else
                    mRemindFragment = new RemindWordTripleOrgFragment();
            }
        }
        mRemindFragment.setListener(this);



        checkButton = (Button) findViewById(R.id.check_guess);
        checkButton.setOnClickListener(this);

        helpButton = (Button) findViewById(R.id.help_guess);
        helpButton.setOnClickListener(this);



        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        // Display the fragment as the main content.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, mRemindFragment)
                .commit();

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        app = App.app;
        app.setReminderVisible(true);
        reminder = App.app.getReminder();
        mRemindFragment.start(reminder);
        if(App.preference.isSpeakEnabled())
            mRemindFragment.speak();

    }


    @Override
    protected void onStop() {
        App.app.setReminderVisible(false);
        App.app.destroySpeak();
        super.onStop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(cc.snakelab.edisco.R.menu.reminder_menu, menu);
        if (App.preference.isSpeakEnabled())
            menu.findItem(R.id.action_speak).setIcon(R.drawable.ic_speaker_on);
        else
            menu.findItem(R.id.action_speak).setIcon(R.drawable.ic_speaker_off);

        menu.findItem(R.id.action_disable_speak).setVisible(App.preference.isSpeakEnabled());
        menu.findItem(R.id.action_enable_speak).setVisible(!App.preference.isSpeakEnabled());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_speak:
                mRemindFragment.speak();
                return true;
            case R.id.action_disable_speak:
                App.preference.setSpeakEnabled(false);
                invalidateOptionsMenu();
                return true;
            case R.id.action_enable_speak:
                App.preference.setSpeakEnabled(true);
                invalidateOptionsMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    public void handleGuessText() {
        reminder.setLastRemind(DateHelper.getCalendar().getTime());
        reminder.save();
        String txt = getEnteredText();


        if (reminder.isCorrect(txt, mRemindFragment.getWordIndex()))
            guessCorrect();
        else
            guessIncorrect();

    }
    private void handleHelpGuess() {
        reminder.setLastRemind(DateHelper.getCalendar().getTime());
        reminder.save();
        AppHelper.hideSoftInput(this);
        DialogFragment dlg = new GuessHelpDialogFragment();
        Bundle args = new Bundle();

        args.putString(GuessHelpDialogFragment.GUESS_WORD, reminder.getTranslationWord(mRemindFragment.getWordIndex()).toString());

        dlg.setArguments(args);

        dlg.show(getSupportFragmentManager(), getString(R.string.guess_help));
    }

    private String getEnteredText() {
        return mRemindFragment.getEnteredText();
    }



    private void guessIncorrect() {
        String mask = reminder.getTranslMask(getEnteredText(), mRemindFragment.getWordIndex());
        mRemindFragment.setMask(mask);
//        AppHelper.hideSoftInput(this);
        DialogFragment dlg = new GuessIncorrectDialogFragment();
        Bundle args = new Bundle();
        args.putInt(GuessIncorrectDialogFragment.ATTEMPTS, mAttempt);
        args.putInt(GuessIncorrectDialogFragment.MAX_ATTEMPTS, mMaxAttempts);
        dlg.setArguments(args);

        dlg.show(getSupportFragmentManager(), getString(R.string.guess_incorrect));


        mAttempt++;


    }


    private void guessCorrect() {
        if (!mRemindFragment.isFinish()) {
            mRemindFragment.next();
            if(App.preference.isSpeakEnabled())
                mRemindFragment.speak();
            return;
        }
        mRemindFragment.setTranslText(reminder.getTranslMask(getEnteredText(), mRemindFragment.getWordIndex()));
        AppHelper.hideSoftInput(this);
        DialogFragment dlg = new GuessCorrectDialogFragment();
        dlg.show(getSupportFragmentManager(), getString(R.string.guess_correct));
        reminder.doCorrectAnswer(true);

    }

    @Override
    public void OnGuessCorrectOkClick(DialogFragment dialog) {
        reminder.setNewWord();
        reminder.save();

        finish();

    }

    @Override
    public void OnGuessLearntClick(DialogFragment dialog) {
        reminder.doLearnt();
        reminder.setNewWord();
        reminder.save();

        finish();
    }

    @Override
    public void onStart() {
        super.onStart();

//        FirebaseAppIndex.getInstance().start(getIndexable());
//        FirebaseUserActions.getInstance().start(getAction());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.check_guess) {
            handleGuessText();
        }
        if (view.getId() == R.id.help_guess) {
            handleHelpGuess();
        }

    }

    @Override
    public void OnGuessIncorrectOkClick(DialogFragment dialog) {
        mRemindFragment.guessIncorrect();

        if (mAttempt > mMaxAttempts) {

            reminder.doCorrectAnswer(false);
            finish();
        }
//        AppHelper.delayedFocusAndKeyboard(g?Z?ue);


    }


    @Override
    public void OnGuessHelpOkClick(DialogFragment dialog) {


        finish();
    }

}
