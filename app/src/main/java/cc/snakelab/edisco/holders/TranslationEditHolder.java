package cc.snakelab.edisco.holders;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.adapters.TranslationEditAdapter;
import cc.snakelab.edisco.orm.Word;

import java.util.List;

/**
 * Created by snake on 02.02.17.
 */

public class TranslationEditHolder extends RecyclerView.ViewHolder {
    public EditText mText;
    public ImageView mCloseIcon;
    private List<Word> mWordList;
    private TextView.OnEditorActionListener mActionListiner;
    private HolderListiner mHolderListiner;
    private TranslationEditAdapter mAdapter;

    public TranslationEditHolder(TranslationEditAdapter adapter, View itemView, List<Word> list, TextView.OnEditorActionListener listener) {
        super(itemView);
        mWordList = list;
        mActionListiner = listener;
        mAdapter = adapter;

        mText = (EditText) itemView.findViewById(R.id.edit_translation);
        mCloseIcon = (ImageView) itemView.findViewById(R.id.remove_transl_word);

        mHolderListiner = new HolderListiner();
        mText.addTextChangedListener(mHolderListiner);

        mCloseIcon.setOnClickListener(mHolderListiner);

        mText.setOnEditorActionListener(mActionListiner);
    }

    public void updatePosition(int position) {
        mHolderListiner.updatePosition(position);
        if (isLast()) {
            mText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }

    }

    public boolean isLast() {
        return mHolderListiner.getPosition() == mWordList.size() - 1;
    }

    private class HolderListiner implements TextWatcher, View.OnClickListener {

        private int position;
        public void updatePosition(int position) {
            this.position = position;
        }
        public int getPosition() { return position;}
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Word word = mWordList.get(position);
            word.setText(charSequence.toString());
            word.setSynced(false);
            word.setVersion(word.getVersion() + 1);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void onClick(View view) {
            if (mWordList.size() > 2) {
                mWordList.remove(position);
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
