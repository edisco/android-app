package cc.snakelab.edisco.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.adapters.TrashAdapter;
import cc.snakelab.edisco.ui.BasicWordUIContainer;

/**
 * Created by snake on 05.04.17.
 */
public class TrashViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

    public TextView firstTextView;
    public TextView secondTextView;

    public BasicWordUIContainer container;
    TrashAdapter mAdapter;

    public TrashViewHolder(View v, TrashAdapter adaper) {
        super(v);
        v.setOnLongClickListener(this);
        mAdapter = adaper;
        firstTextView = (TextView) v.findViewById(R.id.tv_word_head1);
        secondTextView = (TextView) v.findViewById(R.id.tv_word_body1);
    }

    @Override
    public boolean onLongClick(View view) {
        container.onLongClick(null, firstTextView, mAdapter);
        return false;
    }
}
