package cc.snakelab.edisco.holders;

import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 22.05.17.
 */

public class LoadingFooterViewHolder extends RecycleViewHolder {
    public ImageView mProgress;
    public LoadingFooterViewHolder(View itemView) {
        super(itemView);
        mProgress = (ImageView) itemView.findViewById(R.id.progress);

    }
    public void startAnim() {
        ((AnimationDrawable) mProgress.getBackground()).start();
    }

}
