package cc.snakelab.edisco.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 12.04.17.
 */

public class RecordHolder extends RecyclerView.ViewHolder {
    public TextView mText;
    public RecordHolder(View itemView) {
        super(itemView);
        mText = (TextView) itemView.findViewById(R.id.text);
    }
}
