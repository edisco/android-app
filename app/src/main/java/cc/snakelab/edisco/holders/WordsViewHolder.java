package cc.snakelab.edisco.holders;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.WordDescription;

import cc.snakelab.edisco.adapters.RecycleAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.ItemTouchHelperViewHolder;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.ui.BasicWordUIContainer;

/**
 * Created by snake on 05.11.16.
 */

// класс view holder-а с помощью которого мы получаем ссылку на каждый элемент
// отдельного пункта списка
public class WordsViewHolder extends RecycleViewHolder implements
        View.OnClickListener, View.OnLongClickListener, View.OnCreateContextMenuListener, ItemTouchHelperViewHolder {


    private static final String TAG = "WordsViewHolder";
    // наш пункт состоит только из одного TextView
    public TextView mIcon;
    public TextView mWordHead;
    public TextView mWordBody;
    public TextView mPercents;
    public TextView mUnsyncedIcon;
    public TextView mDate;

    public BasicWordUIContainer mWordContainer;
    private RecycleAdapter mAdapter;
    private Reminder mReminder;

    public WordsViewHolder(View v, RecycleAdapter adapter, Reminder reminder) {
        super(v);

        mReminder = reminder;
        mAdapter = adapter;
        mIcon = (TextView) v.findViewById(R.id.tv_icon);
        mWordHead = (TextView) v.findViewById(R.id.tv_word_head);
        mWordBody = (TextView) v.findViewById(R.id.tv_word_body);
        mPercents = (TextView) v.findViewById(R.id.tv_percent);
        mUnsyncedIcon = (TextView) v.findViewById(R.id.tv_unsynced);
        mDate = (TextView) v.findViewById(R.id.tv_date);


//        v.setOnCreateContextMenuListener(this);
        v.setOnClickListener(this);
        v.setOnLongClickListener(this);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo)
    {
//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) contextMenuInfo;

//        Context context = App.app.getApplicationContext();

//        mWordContainer.buildMenu(menu);
//        PopupMenu m = new PopupMenu(context, view);


//        menu.setHeaderTitle(context.getString(R.string.edit));
//
//        menu.add(Menu.FIRST, R.id.add_to_reminder_item, Menu.NONE, context.getString(R.string.add_to_reminder)).setOnMenuItemClickListener(this);
//        menu.add(Menu.FIRST, R.id.rem_from_reminder_item, Menu.NONE, context.getString(R.string.remove_from_reminder)).setOnMenuItemClickListener(this);


    }


    @Override
    public void onClick(View view) {

        if (mWordContainer.getRecord() instanceof Word) {
            Intent wordDescriptionIntent = new Intent(view.getContext(), WordDescription.class);
            App.app.setCurrentWord((Word) mWordContainer.getRecord());
            view.getContext().startActivity(wordDescriptionIntent);
        } else {
            App.app.toast("not implemented");
        }
    }

    @Override
    public boolean onLongClick(View view) {

        mWordContainer.onLongClick(mReminder, mWordHead, mAdapter);
        return true;
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }



}
