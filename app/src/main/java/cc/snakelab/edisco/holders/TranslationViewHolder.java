package cc.snakelab.edisco.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 31.01.17.
 */

public class TranslationViewHolder extends RecyclerView.ViewHolder {
    public TextView mPart;
    public TextView mTranslation;
    public TranslationViewHolder(View itemView) {
        super(itemView);
        mPart = (TextView) itemView.findViewById(R.id.langPart);
        mTranslation = (TextView) itemView.findViewById(R.id.textTranslation);

    }
}
