package cc.snakelab.edisco.holders;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cc.snakelab.edisco.AddWordActivity;
import cc.snakelab.edisco.MainActivity;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.adapters.RecycleAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordSuggestionContainer;

import static cc.snakelab.edisco.AddWordActivity.SUGGESTION_BACK_TEXT;

import static cc.snakelab.edisco.AddWordActivity.SUGGESTION_FRONT_TEXT;
import static cc.snakelab.edisco.MainActivity.ADD_WORDPAIR_REQUEST;


/**
 * Created by snake on 11.09.17.
 */

public class SuggestionWordHolder extends RecycleViewHolder implements View.OnClickListener {
    private static String TAG= "SuggestionWordHolder";
    public TextView mText;
    public WordSuggestionContainer mContainer;
    private RecycleAdapter mAdapter;
    public SuggestionWordHolder(View itemView, RecycleAdapter adapter) {
        super(itemView);
        mText = (TextView) itemView.findViewById(R.id.text);
        mAdapter = adapter;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(App.app.getMainActivityContext(), AddWordActivity.class);
        intent.putExtra(SUGGESTION_FRONT_TEXT, mContainer.getOriginalText());
        intent.putExtra(SUGGESTION_BACK_TEXT, mContainer.getTranslation());
        ((FragmentActivity) App.app.getMainActivityContext()).startActivityForResult(intent, ADD_WORDPAIR_REQUEST);


    }

    public void setContainer(BasicWordUIContainer container) {
        mContainer = (WordSuggestionContainer) container;
        mText.setText(mContainer.getHead());
    }
}
