package cc.snakelab.edisco.showcase;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.view.View;

import com.github.amlcurran.showcaseview.ShowcaseDrawer;

/**
 * Created by snake on 28.04.17.
 */

public class RectangleShowcaseView implements ShowcaseDrawer {
    private final float width;
    private final float height;
    private final Paint eraserPaint;
    private final Paint basicPaint;
    private int eraseColour;
    private final RectF renderRect;

    public RectangleShowcaseView(View view) {
        width = view.getWidth();
        height = view.getHeight();
        PorterDuffXfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);
        eraserPaint = new Paint();
        eraseColour = 0x070707;//App.app.getResources().getColor(R.color.custom_showcase_bg);
        eraserPaint.setColor(eraseColour);
        eraserPaint.setAlpha(0);
        eraserPaint.setXfermode(xfermode);
        eraserPaint.setAntiAlias(true);
        basicPaint = new Paint();

        renderRect = new RectF();
    }

    @Override
    public void setShowcaseColour(int color) {
//        showcaseDrawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
//        eraserPaint.setColor(color);
    }

    @Override
    public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
        Canvas bufferCanvas = new Canvas(buffer);
        renderRect.left = x - width / 2f;
        renderRect.right = x + width / 2f;
        renderRect.top = y - height / 2f;
        renderRect.bottom = y + height / 2f;
        bufferCanvas.drawRect(renderRect, eraserPaint);
    }

    @Override
    public int getShowcaseWidth() {
        return (int) width;
    }

    @Override
    public int getShowcaseHeight() {
        return (int) height;
    }

    @Override
    public float getBlockedRadius() {
        return width;
    }

    @Override
    public void setBackgroundColour(int backgroundColor) {
        this.eraseColour = backgroundColor;

    }

    @Override
    public void erase(Bitmap bitmapBuffer) {
        bitmapBuffer.eraseColor(eraseColour);
    }

    @Override
    public void drawToCanvas(Canvas canvas, Bitmap bitmapBuffer) {
        canvas.drawBitmap(bitmapBuffer, 0, 0, basicPaint);
    }

}

