package cc.snakelab.edisco.api;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import cc.snakelab.edisco.core.AppLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * Created by snake on 01.10.16.
 */
public class Api {
    private static String TAG= "API";
    ApiResultListener mListener;

    public Api(ApiResultListener listener) {
        mListener = listener;


    }

    public interface ApiResultListener extends Response.Listener<ApiResult>, Response.ErrorListener {

    }
    public class ApiRequest extends Request<ApiResult>
    {
        private ApiResultListener mListener;

        private int mMethodId;
        private JSONObject mParams;

        public ApiRequest(int method, String url, ApiResultListener listener, int methodId) {
            super(method, url, listener);
            mListener = listener;
            mParams = new JSONObject();
            mMethodId = methodId;
        }

        @Override
        public byte[] getBody() throws AuthFailureError {
            try {
                return mParams.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return super.getBody();
        }

        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }

//        @Override
//        public String getUrl() {
//            if (getMethod() == Method.GET && getParams() != null && getParams().size() >0) {
//                StringBuilder encodedParams = new StringBuilder(super.getUrl()+"?");
//
//                try {
//                    for (Map.Entry<String, String> entry : getParams().entrySet()) {
//                        encodedParams.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
//                        encodedParams.append('=');
//                        encodedParams.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
//                        encodedParams.append('&');
//                    }
//
//                } catch (Exception e) {
//
//                }
//                return encodedParams.toString();
//            }
//            else
//                return super.getUrl();
//        }

        @Override
        protected Response<ApiResult> parseNetworkResponse(NetworkResponse response) {
            AppLog.d(TAG, "statusCode: " + response.statusCode);
            try {
                String json = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers));

                return Response.success(new ApiResult(response.statusCode, json, mMethodId),
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (Exception e) {
                return Response.error(new VolleyError(e.getMessage()));
            }

        }

        @Override
        public void deliverError(VolleyError volleyError) {
            AppLog.e(TAG, "deliveryError: " + volleyError.getMessage());
            Throwable exception;
            int statusCode = -1;
            try {
                if (volleyError.networkResponse != null) {
                    statusCode = volleyError.networkResponse.statusCode;
                    String data = new String(
                            volleyError.networkResponse.data,
                            HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));

                    if (data.toLowerCase().contains("wrong token"))
                        exception = new WrongTokenException("Wrong Token");
                    else
                        exception = new Exception(
                            String.format("%d: %s",
                                    volleyError.networkResponse.statusCode,
                                    data
                            ));
                } else {
                    exception = volleyError;

                }
            } catch (Exception e) {
                exception = e;
            }
            AppLog.e(TAG, "error", exception);
            mListener.onResponse(new ApiResult(statusCode, exception, mMethodId));
        }


        @Override
        protected void deliverResponse(ApiResult response) {
            AppLog.d(TAG, "deliveryResponse: " + response.toString());
            if (response.getException() != null)
                AppLog.e(TAG, "response error", response.getException());
            mListener.onResponse(response);

        }

        public JSONObject getPostObject() {
            return mParams;
        }
    }

    public class ApiResult {
        private String mResultValue;
        private Throwable mException;
        private JSONObject mResultJson;
        private int mMethodId;
        private int mStatusCode;
        private int mTotalCount;
        private int mPage;
        private Date mLastTimestamp;

        public ApiResult(int statusCode, String resultValue, int methodId) {
            mResultValue = resultValue;
            mResultJson = null;
            mMethodId = methodId;
            mStatusCode = statusCode;
            mLastTimestamp = new Date(0);
            try {
                mResultJson = new JSONObject(resultValue);

                mTotalCount = -1;
                mPage = -1;
                if (mResultJson.optJSONObject("meta") != null) {

                    if (mResultJson.optJSONObject("meta").optJSONObject("pagination") != null) {
                        JSONObject jo = mResultJson.optJSONObject("meta").optJSONObject("pagination");
                        mTotalCount = jo.optInt("total_count", -1);
                        mPage = jo.optInt("page", -1);
                    }

                    if (mResultJson.optJSONObject("meta").optLong("timestamp", 0) != 0)
                        mLastTimestamp = new Date(mResultJson.optJSONObject("meta").optLong("timestamp", 0));
                }




            } catch (JSONException e) {
                mException = e;
                mListener.onResponse(this);
            }
        }
        public ApiResult(int statusCode, Throwable exception, int methodId) {
            mResultJson = null;
            mResultValue = null;
            mMethodId = methodId;
            mStatusCode = statusCode;
            mException = exception;


        }

        public boolean isOk() {
            return mException == null && mResultJson != null;
        }
        public String getResult() {
            return mResultValue;
        }
        public JSONObject getJson() {
            return mResultJson;
        }
        public Throwable getException() {
            return mException;
        }

        public int getMethodId() {
            return mMethodId;
        }

        public void setMethodId(int mMethodId) {
            this.mMethodId = mMethodId;
        }

        public int getStatusCode() {
            return mStatusCode;
        }

        public int getTotalCount() {
            return mTotalCount;
        }

        public int getPage() {
            return mPage;
        }
        public JSONArray getData() {
            if( mResultJson == null)
                return null;
            return mResultJson.optJSONArray("data");
        }

        public Date getLastTimestamp() {
            return mLastTimestamp;
        }
    }



    public class WrongTokenException extends Exception {

        public WrongTokenException(String msg) {
            super(msg);
        }
    };
}
