package cc.snakelab.edisco.api;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTripleHistory;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by snake on 14.03.17.
 */

public class EdiscoApi extends Api {
    private static final String TAG = "EdiscoApi";

    static final public int SIGN_IN = 1;
    static final public int GET_USER_WORDS = 2;
    static final public int UPDATE_USER_WORD = 3;
    static final public int CREATE_USER_WORD = 4;
    static final public int DELETE_USER_WORD = 5;

    public static final int GET_USER_WORD_PAIRS = 6;
    public  static final int CREATE_USER_WORD_PAIR = 7;
    public static final int UPDATE_USER_WORD_PAIR = 8;
    public static final int DELETE_USER_WORD_PAIR = 9;
    public static final int GET_USER_WORD_TRIPLES = 10;
    public static final int UPDATE_USER_WORD_TRIPLE = 11;
    public static final int DELETE_USER_WORD_TRIPLE = 12;
    public static final int GET_USER_WORD_PAIR_HISTORIES = 13;
    public static final int CREATE_USER_WORD_PAIR_HISTORY = 14;
    public static final int UPDATE_USER_WORD_PAIR_HISTORIES = 15;
    public static final int DELETE_USER_WORD_PAIR_HISTORY = 16;
    public static final int GET_USER_WORD_TRIPLE_HISTORIES = 17;
    public static final int CREATE_USER_WORD_TRIPLE_HISTORY = 18;
    public static final int UPDATE_USER_WORD_TRIPLE_HISTORIES = 19;
    public static final int DELETE_USER_WORD_TRIPLE_HISTORY = 20;
    public static final int WIZARD_SETUP = 21;
    public static final int TRANSLATION_SUGGESTIONS = 22;

    String mBaseUrl;
    RequestQueue mQueue;
    String mToken;
    String mEmail;

    public EdiscoApi(ApiResultListener listener, String url, String email, String ediscoToken) {
        super(listener);
        mBaseUrl = url;
        mEmail = email;
        mToken = ediscoToken;
        mQueue = App.app.getRequestQueue();
        AppLog.d(TAG, "Remote Host: " + url);
    }
    public void signIn(String email, String idToken) {
        mEmail = email;
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("sign_in"), mListener, SIGN_IN);
        try {
            request.getPostObject().put("email", email);
            request.getPostObject().put("google_token", idToken);
        } catch (Exception e) {
            AppLog.e(TAG, "unable to send signIn", e);
        }
        mQueue.add(request);



    }


    public void getUserWords() {
        getUserWords(-1, -1, -1, null);
    }
    public void getUserWords(long msecSinceEpoch) {
        getUserWords(-1, -1, msecSinceEpoch, null);
    }
    public void getUserWords(int page, int per) {
        getUserWords(page, per, -1, null);
    }
    public void getUserWords(int page, int per, String uuid) {
        getUserWords(page, per, -1, uuid);
    }
    public void getUserWord(String uuid) {
        getUserWords(-1, -1, -1, uuid);
    }
    public void getUserWords(int page, int per, long msecSinceEpoch, String uuid) {
        Map<String, String> params = getQueryParams(page, per, msecSinceEpoch, uuid);

        ApiRequest request = new ApiRequest(
                Request.Method.GET,
                getUrl("user_words", params),
                mListener,
                GET_USER_WORDS);
        mQueue.add(request);

    }
    public void updateUserWord(Word word) {
        Map<String, String> params = getQueryParams();
//        params.put("uuid", word.getUuid());
        ApiRequest request = new ApiRequest(Request.Method.PUT, getUrl("user_words/"+word.getUuid(),  params), mListener, UPDATE_USER_WORD);


        try {
            request.getPostObject().put("data", word.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWord", e);
        }
        mQueue.add(request);

    }

    private Map<String, String> getQueryParams() {
        return getQueryParams(-1, -1, -1, null);
    }
    private Map<String, String> getQueryParams(int page, int per, String uuid) {
        return getQueryParams(page, per, -1, uuid);
    }

    private Map<String, String> getQueryParams(int page, int per, long msecSinceEpoch, String uuid) {
        Map<String, String> params = new HashMap<>();

        params.put("email", mEmail);
        params.put("token", mToken);
        if (page != -1)
            params.put("page", page + "");
        if (per != -1)
            params.put("per", per + "");
        if (uuid != null)
            params.put("uuid", uuid);
        if (msecSinceEpoch != -1)
            params.put("timestamp",  msecSinceEpoch + "");

        return params;
    }

    private String getUrl(String path) {
        return getUrl(path, null);
    }
    private String getUrl(String path, Map<String, String> params) {
        StringBuilder encodedParams = new StringBuilder(mBaseUrl+"/api/"+path);
        if (params != null && params.size()> 0) {
            encodedParams.append("?");
            try {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    encodedParams.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    encodedParams.append('=');
                    encodedParams.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                    encodedParams.append('&');
                }

            } catch (Exception e) {

            }
            return encodedParams.toString();
        }
        return encodedParams.toString();

    }

    public void createUserWord(Word word) {
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("user_words", getQueryParams()), mListener, CREATE_USER_WORD);
        try {
            request.getPostObject().put("data", word.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWord", e);
        }
        mQueue.add(request);
    }

    public void deleteUserWord(String uuid) {
        Map<String, String > params = getQueryParams();
//        params.put("uuid", uuid);
        ApiRequest request = new ApiRequest(Request.Method.DELETE, getUrl("user_words/"+uuid, params), mListener, DELETE_USER_WORD);
        mQueue.add(request);
    }
    private JSONObject userWordToJSON(String uuid, String text, String lang, Integer version) {
        JSONObject ret = new JSONObject();
        JSONObject attr = new JSONObject();
        try {
            ret.put("uuid", uuid);
            attr.put("text", text);
            attr.put("lang", lang);
            attr.put("version", version);
            ret.put("attributes", attr);

        } catch (JSONException e) {
            AppLog.e(TAG, "unable to userWordToJSON", e);
        }
        return ret;
    }

    public void getUserWordPairs() {
        getUserWordPairs(-1, -1, -1, null);
    }
    public void getUserWordPairs(long msecSinceEpoch) {
        getUserWordPairs(-1, -1, msecSinceEpoch, null);
    }
    public void getUserWordPairs(int page, int per, long msecSinceEpoch, String uuid) {


        ApiRequest request = new ApiRequest(
                Request.Method.GET,
                getUrl("user_word_pairs",  getQueryParams(page, per, msecSinceEpoch, uuid)),
                mListener,
                GET_USER_WORD_PAIRS);
        mQueue.add(request);

    }

    public void createUserWordPair(WordPair wordPair) {
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("user_word_pairs", getQueryParams()), mListener, CREATE_USER_WORD_PAIR);
        try {
            request.getPostObject().put("data", wordPair.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send createUserWordPair", e);
        }
        mQueue.add(request);
    }

    public void updateUserWordPair(WordPair wordPair) {
        ApiRequest request = new ApiRequest(Request.Method.PUT, getUrl("user_word_pairs/"+wordPair.getUuid(), getQueryParams()), mListener, UPDATE_USER_WORD_PAIR);
        try {
            request.getPostObject().put("data", wordPair.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWordPair", e);
        }
        mQueue.add(request);

    }

    public void deleteUserWordPair(WordPair wp) {
        ApiRequest request = new ApiRequest(Request.Method.DELETE, getUrl("user_word_pairs/"+wp.getUuid(), getQueryParams()), mListener, DELETE_USER_WORD_PAIR);
        mQueue.add(request);
    }
    public void getUserWordTriples() {
        getUserWordTriples(-1, -1, -1, null);
    }

    public void getUserWordTriples(long msecSinceEpoch) {
        getUserWordTriples(-1, -1, msecSinceEpoch, null);
    }

    public void getUserWordTriples(int page, int per, long msecSinceEpoch, String uuid) {
        ApiRequest request = new ApiRequest(
                Request.Method.GET,
                getUrl("user_word_triples",  getQueryParams(page, per, msecSinceEpoch, uuid)),
                mListener,
                GET_USER_WORD_TRIPLES);
        mQueue.add(request);
    }

    public void updateUserWordTriple(WordTriple wt) {
        ApiRequest request = new ApiRequest(Request.Method.PUT, getUrl("user_word_triples/"+wt.getUuid(), getQueryParams()), mListener, UPDATE_USER_WORD_TRIPLE);
        try {
            request.getPostObject().put("data", wt.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWordTriple", e);
        }
        mQueue.add(request);
    }

    public void deleteUserWordTriple(WordTriple wt) {
        ApiRequest request = new ApiRequest(Request.Method.DELETE, getUrl("user_word_triples/"+wt.getUuid(), getQueryParams()), mListener, DELETE_USER_WORD_TRIPLE);
        mQueue.add(request);
    }

    public void getUserWordPairHistories() {
        getUserWordPairHistories(-1, -1, null);
    }

    public void getUserWordPairHistories(int page, int per, String uuid) {
        ApiRequest request = new ApiRequest(
                Request.Method.GET,
                getUrl("user_word_pair_histories",  getQueryParams(page, per, uuid)),
                mListener,
                GET_USER_WORD_PAIR_HISTORIES);
        mQueue.add(request);
    }

    public void createUserWordPairHistory(WordPairHistory wph) {
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("user_word_pair_histories", getQueryParams()), mListener, CREATE_USER_WORD_PAIR_HISTORY);
        try {
            request.getPostObject().put("data", wph.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send createUserWordPairHistory", e);
        }
        mQueue.add(request);
    }

    public void updateUserWordPairHistory(WordPairHistory wph) {
        ApiRequest request = new ApiRequest(Request.Method.PUT, getUrl("user_word_pair_histories/"+wph.getUuid(), getQueryParams()), mListener, UPDATE_USER_WORD_PAIR_HISTORIES);

        try {
            request.getPostObject().put("data", wph.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWordPairHistories", e);
        }
        mQueue.add(request);
    }

    public void deleteUserWordPairHistory(WordPairHistory wph) {
        ApiRequest request = new ApiRequest(Request.Method.DELETE, getUrl("user_word_pair_histories/"+wph.getUuid(), getQueryParams()), mListener, DELETE_USER_WORD_PAIR_HISTORY);
        mQueue.add(request);
    }

    public void getUserWordTripleHistories() {
        getUserWordTripleHistories(-1, -1, null);
    }
    public void getUserWordTripleHistories(int page, int per, String uuid) {
        ApiRequest request = new ApiRequest(
                Request.Method.GET,
                getUrl("user_word_triple_histories",  getQueryParams(page, per, uuid)),
                mListener,
                GET_USER_WORD_TRIPLE_HISTORIES);
        mQueue.add(request);
    }

    public void createUserWordTripleHistory(WordTripleHistory wth) {
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("user_word_triple_histories", getQueryParams()), mListener, CREATE_USER_WORD_TRIPLE_HISTORY);
        try {
            request.getPostObject().put("data", wth.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send createUserWordTripleHistory", e);
        }
        mQueue.add(request);
    }

    public void updateUserWordTripleHistory(WordTripleHistory wth) {
        ApiRequest request = new ApiRequest(Request.Method.PUT, getUrl("user_word_triple_histories/"+wth.getUuid(), getQueryParams()), mListener, UPDATE_USER_WORD_TRIPLE_HISTORIES);

        try {
            request.getPostObject().put("data", wth.toJSON());
        } catch (JSONException e) {
            AppLog.e(TAG, "unable to send updateUserWordTripleHistories", e);
        }
        mQueue.add(request);
    }

    public void deleteUserWordTripleHistory(WordTripleHistory wth) {
        ApiRequest request = new ApiRequest(Request.Method.DELETE, getUrl("user_word_triple_histories/"+wth.getUuid(), getQueryParams()), mListener, DELETE_USER_WORD_TRIPLE_HISTORY);
        mQueue.add(request);
    }

    public void wizardSetup(String learningLang, String nativeLang) throws JSONException {


        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("wizard_setup", getQueryParams()), mListener, WIZARD_SETUP);
        JSONObject jo = new JSONObject();
        jo.put("learning_lang", learningLang);
        jo.put("native_lang", nativeLang);

        request.getPostObject().put("data", jo);
        adjust(request, 120000);
        mQueue.add(request);
    }
//https://techstricks.com/avoid-multiple-requests-when-using-volley/
    private void adjust(ApiRequest request, int timeout) {
        request.setRetryPolicy(new DefaultRetryPolicy(timeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void translationSuggestion(String text, String learningLang, String nativeLang) throws JSONException {
        ApiRequest request = new ApiRequest(Request.Method.POST, getUrl("translation_suggestions", getQueryParams()), mListener, TRANSLATION_SUGGESTIONS);
        JSONObject jo = new JSONObject();
        jo.put("text", text);
        jo.put("native_lang", nativeLang);
        jo.put("learning_lang", learningLang);
        request.getPostObject().put("data", jo);
        mQueue.add(request);
    }
}
