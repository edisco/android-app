package cc.snakelab.edisco.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import cc.snakelab.edisco.core.App;

public class ReminderService extends Service {
    final static String TAG = App.APPTAG+"RemindService";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "created");

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "destroyed");
        super.onDestroy();

    }


}
