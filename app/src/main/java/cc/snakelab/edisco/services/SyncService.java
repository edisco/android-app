package cc.snakelab.edisco.services;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.firebase.FirebaseApp;

import cc.snakelab.edisco.adapters.SyncAdapter;

/**
 * Created by snake on 24.03.17.
 */

public class SyncService extends Service {
    // Storage for an instance of the sync adapter
    private static SyncAdapter mSyncAdapter = null;
    // RecycleAdapter to use as a thread-safe lock
    private static final Object mSyncAdapterLock = new Object();

    @Override
    public void onCreate() {
        /*
         * Create the sync adapter as a singleton.
                * Set the sync adapter as syncable
                * Disallow parallel syncs
         */
        synchronized (mSyncAdapterLock) {
            if (mSyncAdapter == null) {
//                FirebaseApp.initializeApp(this);
                mSyncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
         /*
         * Get the object that allows external processes
         * to call onPerformSync(). The object is created
         * in the base class code when the SyncAdapter
         * constructors call super()
         */
        return mSyncAdapter.getSyncAdapterBinder();
    }
}
