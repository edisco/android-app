package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.google.firebase.analytics.FirebaseAnalytics;
import cc.snakelab.edisco.adapters.TrashAdapter;

public class TrashActivity extends AppCompatActivity
{
    private RecyclerView mTrashedRecycleView;
    private TrashAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ItemTouchHelper mItemTouchHelper;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.activity_trash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTrashedRecycleView = (RecyclerView) findViewById(R.id.archivewordlist);
        mLayoutManager = new LinearLayoutManager(this);
        mTrashedRecycleView.setLayoutManager(mLayoutManager);


        mAdapter = new TrashAdapter(this);
        mTrashedRecycleView.setAdapter(mAdapter);

        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
