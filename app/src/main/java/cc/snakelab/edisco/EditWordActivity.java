package cc.snakelab.edisco;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.orm.Word;

/**
 * Created by snake on 14.02.17.
 */

public class EditWordActivity extends AppCompatActivity implements WordEditFragment.OnWordFinishListener  {

    WordEditFragment mEditFragment;
    Word mWord;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.activity_edit_word_pair);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mWord = App.app.getCurrentWord();
        mEditFragment = (WordEditFragment) getSupportFragmentManager().findFragmentById(R.id.edit_word_fragment);
        mEditFragment.putWord(mWord);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save_word) {
            mEditFragment.save();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save_word, menu);

        return true;
    }

    @Override
    public void onWordSave(Word word) {
        setResult(RESULT_OK);
        finish();
    }
}
