package cc.snakelab.edisco.intro;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.raizlabs.android.dbflow.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;

/**
 * Created by snake on 17.05.17.
 */

public class LangSelect extends Fragment implements IntroWizardNG.WizardClickListener {
    String[] langArray;
    Spinner spinnerLernt;
    Spinner spinnerMy;
    IntroWizardNG mIntroWizard;
    Bundle mSettings;

    public static LangSelect newInstance(IntroWizardNG introWizard) {
        LangSelect ls = new LangSelect();
        ls.setActivity(introWizard);
        return ls;
    }

    public void setActivity(IntroWizardNG introWizardNG) {
        mIntroWizard = introWizardNG;
        setArguments(introWizardNG.mSettings);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_2, container, false);
        setup(view);
        return view;
    }
    private void setup(View view) {
        langArray =  getResources().getStringArray(R.array.array_of_languages);

        spinnerLernt = (Spinner) view.findViewById(R.id.langs);
        if (spinnerLernt == null)
            return;
        spinnerMy = (Spinner) view.findViewById(R.id.mylang_spinner);
//        android.R.layout.simple_spinner_item
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.intro_spinner, langArray);
        adapter.setDropDownViewResource(R.layout.intro_spinner_dropdown);

        List<String> list = Arrays.asList(langArray);
        spinnerLernt.setAdapter(adapter);
        spinnerMy.setAdapter(adapter);
        String lang = LocaleHelper.getCurrentLang(App.app);


        spinnerMy.setSelection(list.indexOf(LocaleHelper.langCodeToString(App.app,lang)));
        String targetLang = getString(R.string.english);
        if (lang.equalsIgnoreCase(LocaleHelper.EN))
            targetLang = getString(R.string.german);
        spinnerLernt.setSelection(list.indexOf(targetLang));

    }

    @Override
    public void onShowPage() {
        mIntroWizard.mPrev.setVisibility(View.INVISIBLE);
//        mIntroWizard.mNext.setText(R.string.next);
    }

    @Override
    public void onPrevClick() {

    }

    @Override
    public void onNextClick() {
        String learningLang = spinnerLernt.getSelectedItem().toString();
        String myLang = spinnerMy.getSelectedItem().toString();
        if(StringUtils.isNotNullOrEmpty(learningLang) & StringUtils.isNotNullOrEmpty(myLang)) {

            List<String> langs = new ArrayList<>();
            langs.add(learningLang);
            App.preference.setLearningLangsList(langs);
            App.preference.setCurrentLearningLang(learningLang);
            App.preference.setLang(myLang);
            getArguments().putString("myLang", myLang);
            getArguments().putString("learningLang", learningLang);


        }

    }
}
