package cc.snakelab.edisco.intro;


import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;

import cc.snakelab.edisco.MainActivity;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.api.Api;
import cc.snakelab.edisco.api.EdiscoApi;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.recievers.SyncReceiver;

/**
 * Created by snake on 17.05.17.
 */

public class Syncing extends Fragment implements IntroWizardNG.WizardClickListener, SyncReceiver.OnSyncDoneListener, Api.ApiResultListener {
    private static final String TAG = "Syncing";
    private EdiscoApi mApi;
    private IntroWizardNG mIntroWizard;
    private SyncReceiver mSyncReceiver;
//    private ProgressBar mFinalProgressBar;
//    private TextView mDoneText;
    private ImageView mProgress;
    private TextView mProgressText;

    public static Syncing newInstance(IntroWizardNG introWizard){
        Syncing syncing = new Syncing();
        syncing.setActivityWizard(introWizard);
        return syncing;
    }

    public void setActivityWizard(IntroWizardNG introWizard) {
        mIntroWizard = introWizard;
        setArguments(introWizard.mSettings);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_4, container, false);
        setup(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppLog.d(TAG, "resume");

        IntentFilter filter = new IntentFilter(SyncReceiver.ON_SYNC_DONE_ACTION);
        filter.addAction(SyncReceiver.ON_SYNC_PROGRESS_ACTION);
        filter.addAction(SyncReceiver.ON_SYNC_ERROR_ACTION);
        mSyncReceiver = new SyncReceiver(this);
        getContext().registerReceiver(mSyncReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(mSyncReceiver);
        AppLog.d(TAG, "pause");
    }

    private void setup(View view) {
        mApi = new EdiscoApi(
                this,
                App.preference.getApiServerUrl(),
                App.preference.getEmail(),
                App.preference.getApiToken());
        AppLog.d(TAG, "api token="+App.preference.getApiToken());
        mProgress = (ImageView) view.findViewById(R.id.progressBar);
        mProgressText = (TextView) view.findViewById(R.id.textProgress);

//        mFinalProgressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
//        mDoneText = (TextView) view.findViewById(R.id.sync_done);

    }

    private void startMainActivity() {
        Intent intent = new Intent(App.app, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onShowPage() {
        mIntroWizard.mNext.setVisibility(View.INVISIBLE);
        mProgress.setVisibility(View.VISIBLE);

        if (mProgress.getBackground() instanceof AnimationDrawable)
            ((AnimationDrawable) mProgress.getBackground()).start();
//        mIntroWizard.mNext.setText(R.string.done);
//        mDoneText.setVisibility(View.INVISIBLE);
//        mFinalProgressBar.setVisibility(View.VISIBLE);
        mProgress.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mProgressText.setText(R.string.sync_create_indexes);
                    App.app.createIndexesIfNeeded();

                } catch (Exception e) {
                    AppLog.e(TAG, "unable to create indexes");
                }
                String myLang = getArguments().getString("myLang");
                String learningLang = getArguments().getString("learningLang");
                try {
                    AppLog.d(TAG, "sending wizard setup");

                    mApi.wizardSetup(LocaleHelper.stringToCodeLang(getContext(), learningLang), LocaleHelper.stringToCodeLang(getContext(), myLang));
                } catch (JSONException e) {
                    AppLog.e(TAG, "unable to api wizard setup", e);
                    startMainActivity();
                }
            }
        });


    }

    @Override
    public void onPrevClick() {
    }

    @Override
    public void onNextClick() {
        startMainActivity();

    }

    @Override
    public void onSyncDone() {

        try {
            try {
                mProgress.setImageResource(R.drawable.ic_spinner_ok);
                mProgress.setBackground(null);
//                mProgress.setBackgroundResource(R.drawable.ic_spinner_ok);
//                ((AnimationDrawable) mProgress.getBackground()).start();
            } finally {
                mIntroWizard.mNext.setVisibility(View.VISIBLE);
                mProgressText.setVisibility(View.INVISIBLE);

            }
        } catch (Exception e) {
            AppLog.e(TAG, "onSyncDone", e);
            startMainActivity();
        }
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncError(String msg) {
        try {
            try {
                mProgress.setImageResource(R.drawable.ic_error);
                mProgress.setBackground(null);
            } finally {
                mIntroWizard.mNext.setVisibility(View.VISIBLE);
                mProgressText.setText(msg);

            }
        } catch (Exception e) {
            AppLog.e(TAG, "onSyncDone", e);
            startMainActivity();
        }

    }

    @Override
    public void onSyncProgress(final Bundle params) {
        mProgressText.post(new Runnable() {
            @Override
            public void run() {
                mProgressText.setText(AppHelper.syncParams2Str(params));
            }
        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Api.ApiResult response) {
        if (response.isOk() && response.getMethodId() == EdiscoApi.WIZARD_SETUP)
            mProgressText.post(new Runnable() {
                @Override
                public void run() {
                    mProgressText.setText(R.string.sync_populating_db);
                }
            });
        if (AppHelper.isNetworkAvailable())
            AppHelper.sync(getContext());
        else
            startMainActivity();
    }

}
