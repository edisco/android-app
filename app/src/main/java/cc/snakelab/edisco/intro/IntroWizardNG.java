package cc.snakelab.edisco.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import cc.snakelab.edisco.R;

/**
 * Created by snake on 17.05.17.
 */

public class IntroWizardNG extends AppCompatActivity implements View.OnClickListener {
    private IntroAdapter mAdapter;
    private ViewPager mViewPager;
    public Button mPrev;
    public ImageButton mNext;
    public Bundle mSettings;
    private CountingIdlingResource mCounter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        mPrev = (Button) findViewById(R.id.goto_prev);
        mNext = (ImageButton) findViewById(R.id.goto_next);
        mPrev.setOnClickListener(this);
        mNext.setOnClickListener(this);

        mSettings = new Bundle();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new IntroAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        ((WizardClickListener) getCurrentFragment()).onShowPage();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goto_prev:
                ((WizardClickListener) getCurrentFragment()).onPrevClick();
                doPrevPage();
                break;
            case R.id.goto_next:
                ((WizardClickListener) getCurrentFragment()).onNextClick();
                doNextPage();
                break;
        }
    }
    private void doNextPage() {
        int i = mViewPager.getCurrentItem();
        if (i >=0 && i < mAdapter.getCount() - 1) {
            mViewPager.setCurrentItem(i + 1);
            ((WizardClickListener) getCurrentFragment()).onShowPage();
        }

    }

    private void doPrevPage() {
        int i = mViewPager.getCurrentItem();
        if (i >0 && i < mAdapter.getCount()) {
            mViewPager.setCurrentItem(i - 1);
            ((WizardClickListener) getCurrentFragment()).onShowPage();
        }
    }

    private Fragment getCurrentFragment() {
        return mAdapter.getItem(mViewPager.getCurrentItem());
    }

    public void incrementIdleCounter() {
        if (mCounter != null)
            mCounter.increment();
    }

    public void decrementIdleCounter() {
        if (mCounter != null)
            mCounter.decrement();
    }

    private static class IntroAdapter extends FragmentPagerAdapter {
        List<Fragment> mList;
        IntroWizardNG mIntroWizard;
        public IntroAdapter(FragmentManager fm, IntroWizardNG introWizard) {
            super(fm);
            mList = new ArrayList<>();
            mIntroWizard = introWizard;
            while (mList.size() < getCount())
                mList.add(null);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment = mList.get(position);
            if (fragment == null) {
                switch (position) {
                    case 0:
                        fragment = LangSelect.newInstance(mIntroWizard);
                        break;
                    case 1:
                        fragment = Syncing.newInstance(mIntroWizard);
                        break;
                }
            }
            mList.set(position, fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public interface WizardClickListener {
        void onShowPage();
        void onPrevClick();
        void onNextClick();
    }
    public CountingIdlingResource getIdleCounter(boolean createIfNeeded) {
        if (mCounter == null && createIfNeeded)
            mCounter = new CountingIdlingResource("Intro");
        return mCounter;
    }
}
