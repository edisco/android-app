package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import cc.snakelab.edisco.helper.MiscHelper;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 06.03.17.
 */

public class RemindHtmlFragment extends RemindFragment {
    WebView web;
    String mEnteredText;
    Reminder mReminder;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remind_html, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        web = (WebView) view.findViewById(R.id.web);
        WebSettings webSettings = web.getSettings();
        webSettings.setJavaScriptEnabled(true);


    }

    @Override
    public void start(Reminder reminder) {
        mReminder = reminder;
        mEnteredText = "";
        web.addJavascriptInterface(new ReminderWebInterface(this, reminder), "android");
        web.loadUrl("file:///android_asset/index.html");
//        web.evaluateJavascript("game.start('test');", null);
//        web.loadUrl(String.format("javascript:game.start({word: '%s', chars: '%s', title: '%s'});",
//                reminder.getTranslationWord().getText(),
//                "abc",
//                reminder.getGuessWord().getText()));

    }
    @Override
    public String getEnteredText() {
        return mEnteredText;
    }

    @Override
    public String getEnteredText2() {
        return null;
    }

    @Override
    public String getEnteredText3() {
        return null;
    }

    @Override
    public void setMask(String mask) {

    }

    @Override
    public void setTranslText(String translMask) {

    }

    @Override
    public void guessIncorrect() {

    }

    @Override
    public void setListener(IOnGuessListener listener) {

    }

    @Override
    public int getWordIndex() {
        return -1;
    }

    @Override
    public void next() {

    }

    @Override
    public void speak() {

    }

    @Override
    public boolean isFinish() {
        return true;
    }

    public void setEnteredText(String text) {
        mEnteredText = text;

    }

    public class ReminderWebInterface {
        RemindHtmlFragment mFragment;
        Reminder mReminder;
        ReminderWebInterface(RemindHtmlFragment fragment, Reminder reminder) {

            mFragment = fragment;
            mReminder = reminder;
        }
        @JavascriptInterface
        public String getWord() {
            return mReminder.getTranslationWord().getText();

        }
        @JavascriptInterface
        public String getTitle() {
            return mReminder.getGuessWord().getText();
        }
        @JavascriptInterface
        public String getChars() {
            String chars = MiscHelper.getSimilarChars(
                    mReminder.getTranslationWord().getText(),
                    mReminder.getTranslationWord().getLang(),
                    3
                    );
            return chars;
        }
        @JavascriptInterface
        public void updateEnteredText(String text) {
            mFragment.setEnteredText(text);
        }

    }
}
