package cc.snakelab.edisco;

import android.support.v4.app.Fragment;

import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 02.03.17.
 */
public abstract class RemindFragment extends Fragment {
    public abstract void start(Reminder reminder);

    public abstract String getEnteredText();
    public abstract String getEnteredText2();
    public abstract String getEnteredText3();

    public abstract void setMask(String mask);

    public abstract void setTranslText(String translMask);

    public abstract void guessIncorrect();
    public abstract void setListener(IOnGuessListener listener);

    public abstract int getWordIndex();

    public abstract void next();
    public abstract void speak();
    public abstract boolean isFinish();

    public interface IOnGuessListener {
        void handleGuessText();
    }
}
