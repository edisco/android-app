package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cc.snakelab.edisco.adapters.RecordViewAdapter;

/**
 * Created by snake on 12.04.17.
 */

public class RecordViewerFragment extends Fragment {
    Class mKlass;

    RecyclerView mRecycleView;
    RecordViewAdapter mAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_record_viewer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecycleView = (RecyclerView) view.findViewById(R.id.record_table);
        mAdapter = new RecordViewAdapter();
        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public void putKlass(Class klass) {
        mKlass = klass;
        mAdapter.update(klass);

    }
}
