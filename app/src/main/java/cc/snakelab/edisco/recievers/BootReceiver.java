package cc.snakelab.edisco.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.services.ReminderService;

/**
 * Created by shen on 22.11.2016.
 */

public class BootReceiver extends BroadcastReceiver {
    final static String TAG = App.APPTAG+"BootReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d(TAG, "Looks like system just started");
            context.startService(new Intent(context, ReminderService.class));

        }
        if(intent.getAction() != null && intent.getAction() .equals("notification_cancelled"))
        {
            Log.d(TAG, "canceled");
        }
    }
}
