package cc.snakelab.edisco.recievers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import cc.snakelab.edisco.core.AppLog;

/**
 * Created by snake on 30.03.17.
 */

public class SyncReceiver extends BroadcastReceiver {
    public static final String ON_SYNC_DONE_ACTION = "cc.snakelab.edisco.on_sync_done_action";
    public static final String ON_SYNC_START_ACTION = "cc.snakelab.edisco.on_sync_start_action";
    public static final String ON_SYNC_ERROR_ACTION = "cc.snakelab.edisco.on_sync_error";
    public static final String ON_SYNC_PROGRESS_ACTION = "cc.snakelab.edisco.on_sync_progress";
    public static final String API_METHOD = "API_METHOD";
    public static final String ERROR = "ERROR";
    public static final String RECORDS_NUM = "RECORDS_NUM";

    private static final String TAG = "SyncReceiver";

    OnSyncDoneListener mListener;
    public SyncReceiver(OnSyncDoneListener listener) {
        super();
        mListener = listener;

    }
    @Override
    public void onReceive(Context context, Intent intent) {
        AppLog.d(TAG, "OnReceive %s", intent.toString());
        if (intent.getAction().equals(ON_SYNC_DONE_ACTION))
            mListener.onSyncDone();
        if (intent.getAction().equals(ON_SYNC_START_ACTION))
            mListener.onSyncStart();
        if (intent.getAction().equals(ON_SYNC_ERROR_ACTION)) {
            mListener.onSyncError(intent.getStringExtra(ERROR));
        }
        if (intent.getAction().equals(ON_SYNC_PROGRESS_ACTION)) {
            mListener.onSyncProgress(intent.getExtras());
        }
    }
    public interface OnSyncDoneListener  {
        void onSyncDone();
        void onSyncStart();
        void onSyncError(String msg);
        void onSyncProgress(Bundle params);

    }
}
