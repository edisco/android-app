package cc.snakelab.edisco.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cc.snakelab.edisco.core.Notifier;
import cc.snakelab.edisco.core.App;

/**
 * Created by snake on 01.12.16.
 */

public class AlarmReceiver extends BroadcastReceiver {
    final static String TAG = App.APPTAG+"AlarmReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received intent: "+intent.toString());
        Notifier.notifyIfNeeded(context);
    }
}
