package cc.snakelab.edisco.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Reminder;

/**
 * Created by snake on 09.05.17.
 */

public class NotifyDeleteReceiver extends BroadcastReceiver {
    private static final String TAG = "NotifyDeleteReceiver";
    public static final String ON_DELETE_NOTIFY = "cc.snakelab.edisco.on_delete_notify";

    @Override
    public void onReceive(Context context, Intent intent) {
        AppLog.d(TAG, "user deleted remind notify");
        Reminder r = Reminder.get();
        r.setLastRemind(DateHelper.getCalendar().getTime());
        r.save();
    }
}
