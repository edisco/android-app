package cc.snakelab.edisco;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.adapters.TranslationEditAdapter;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.Word_Table;

import java.util.List;

/**
 * Created by snake on 02.02.17.
 */

public class WordEditFragment extends Fragment implements TextView.OnEditorActionListener, View.OnClickListener {
    EditText mOrgWord;

    TranslationEditAdapter mAdapter;
    RecyclerView mRecycler;
    Word mWord;
    OnWordFinishListener mListiner;
    LinearLayoutManager mRecycleLayoutManager;

    WordSettingsFragment mWordSettingsFragment;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOrgWord = (EditText) view.findViewById(R.id.frontWord);


        mRecycler = (RecyclerView) view.findViewById(R.id.edit_translation_words);
        mAdapter = new TranslationEditAdapter(this);
        mRecycler.setAdapter(mAdapter);
        mRecycleLayoutManager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(mRecycleLayoutManager);

        mWordSettingsFragment = new WordSettingsFragment();
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.word_settings, mWordSettingsFragment)
                .commit();


    }

    @Override
    public void onResume() {
        super.onResume();
        if(mWord != null) {
            mOrgWord.setText(mWord.getText());

        }
        if(StringUtils.isNullOrEmpty(mWord.getText()))
            AppHelper.delayedFocusAndKeyboard(mOrgWord);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWordFinishListener)
            mListiner = (OnWordFinishListener) context;


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_word, container, false);

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_DONE) {
            save();
            return true;
        }
        return false;
    }



    public void save()
    {
        String value = mOrgWord.getText().toString();
        //validation
        if(StringUtils.isNullOrEmpty(value.trim())) {
            mOrgWord.setError(getString(R.string.empty_validation));
            mOrgWord.requestFocus();
            return;
        }
        //find trashed same word
        Word trashedWord = SQLite.select().from(Word.class)
                .where(Word_Table.text.like(value))
                .and(Word_Table.id.notEq(mWord.getId()))
                .and(Word_Table.trashed.eq(true)).querySingle();

        if (trashedWord != null)
            mWord.setId(trashedWord.getId());

        List<Word> list = SQLite.select().from(Word.class).
                where(Word_Table.text.like(value)).
                and(Word_Table.lang.eq(mWord.getLang())).
                and(Word_Table.id.notEq(mWord.getId())).
                and(Word_Table.trashed.eq(false)).
                queryList();

        if (list.size() > 0){
            Word word = list.get(0);
            String txt = String.format("%s (%s)", word.getText(), word.getLang());
            mOrgWord.setError(getString(R.string.already_existed_validation, txt));
            mOrgWord.requestFocus();
            return;
        }

        if (!mAdapter.isValid()) {
//            mRecycler.requestFocus();
            return;
        }

        mWord.setText(value);
        List<Word> newList = mAdapter.getTranslations();
        mWord.setSynced(false);
        mWord.setTrashed(false);
        mWord.setVersion(mWord.getVersion() + 1);
        mWord.mergePairTranslations(newList, App.app.getReminder());
        mListiner.onWordSave(mWord);

        mAdapter.finish();


    }

    public void putWord(Word word){
        mWord = word;
        mAdapter.putWord(mWord);
        if (StringUtils.isNotNullOrEmpty(mWord.getText()))
            mAdapter.focusFirstTranslation();

        mWordSettingsFragment.putWord(mWord);
    }

    @Override
    public void onClick(View view) {
//        if (view == mAddButton)
        {

            mRecycler.requestFocus();
            mRecycler.scrollToPosition(mAdapter.addWord());

        }


    }



    public interface OnWordFinishListener {
        void onWordSave(Word word);


    }
}
