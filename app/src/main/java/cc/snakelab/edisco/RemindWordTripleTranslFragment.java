package cc.snakelab.edisco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.AppHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.WordTriple;

/**
 * Created by snake on 03.03.17.
 */

public class RemindWordTripleTranslFragment extends RemindFragment implements TextView.OnEditorActionListener {
    TextView mTransl;
    TextView mGuess;
    TextView mTripleText;
    EditText mEditGuess;
    TextView mFrontLangText;
    TextView mBackLangText;
    Reminder mReminder;
    int mWordIndex;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditGuess = (EditText) view.findViewById(R.id.editGuess);
        mEditGuess.setOnEditorActionListener(this);

        mTransl  = (TextView) view.findViewById(R.id.transText);
        mGuess = (TextView) view.findViewById(R.id.guessText);
        mTripleText = (TextView) view.findViewById(R.id.tripleText);
        mFrontLangText = (TextView) view.findViewById(R.id.frontLang);
        mBackLangText = (TextView) view.findViewById(R.id.backLang);
        mWordIndex = 0;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remind_word_pair, container, false);
    }


    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }

    @Override
    public void start(Reminder reminder) {
        mReminder = reminder;
        WordTriple wt = reminder.getCurrentWordTriple();
        mEditGuess.setText("");
        mTripleText.setText(String.format("%d/%d", mWordIndex+1, 3));
        mGuess.setText(reminder.getGuessWord(mWordIndex).getText());
        mTransl.setText(reminder.getTranslMask("", mWordIndex));
        mFrontLangText.setText(reminder.getGuessWord().getLang());
        mBackLangText.setText(reminder.getTranslationWord(mWordIndex).getLang());
        AppHelper.delayedFocusAndKeyboard(mEditGuess);

    }

    @Override
    public String getEnteredText() {

        return mEditGuess.getText().toString();
    }

    @Override
    public String getEnteredText2() {
        return null;
    }

    @Override
    public String getEnteredText3() {
        return null;
    }

    @Override
    public void setMask(String mask) {
        mTransl.setText(mask);

    }

    @Override
    public void setTranslText(String translMask) {

    }

    @Override
    public void guessIncorrect() {

    }

    @Override
    public void setListener(IOnGuessListener listener) {

    }

    @Override
    public int getWordIndex() {
        return mWordIndex;
    }

    @Override
    public void next() {
        mWordIndex++;
        start(mReminder);
    }

    @Override
    public void speak() {
        App.app.speak(
                mReminder.getGuessWord(mWordIndex).toString(),
                mReminder.getGuessWord(mWordIndex).getLang());
    }


    @Override
    public boolean isFinish() {
        return mWordIndex >= 2;
    }

}
