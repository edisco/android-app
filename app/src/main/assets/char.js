function getCharPositionX(index) {
    return index * (CHAR_WIDTH + 10) + CHAR_WIDTH / 2;
}
function getCharPositionY() {
    return WORD_Y;
}
function isCharPositionValid(x,y, params) {
    if (x < 0 || x > getAppWidth() - CHAR_WIDTH || y < 0 || y > getAppHeight() - CHAR_HEIGHT || y < WORD_Y + CHAR_HEIGHT)
        return false;
    for(var i = 0; i < game.chars.length; i++) {
        var ch = game.chars[i];
        var hit = hitTestRectangle({x: x, y: y, width: CHAR_WIDTH, height: CHAR_HEIGHT}, ch);
        if (hit)
            return false;
    }
    return true;
}

function createChar(char, x, y) {

    var container = new PIXI.Container();
    container.x = x;
    container.y = y;
    container.pivot.x = this.CHAR_WIDTH / 2;
    container.pivot.y = this.CHAR_HEIGHT/ 2;

    var rectangle = new PIXI.Graphics();
    rectangle.lineStyle(2, 0x000000, 1);
    rectangle.beginFill(0x000000,0);
    rectangle.drawRect(0, 0, this.CHAR_WIDTH, this.CHAR_HEIGHT);
    rectangle.endFill();
    container.addChild(rectangle);

    var style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 24,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
        wordWrapWidth: 440
    });
    var text = new PIXI.Text(char, {fontSize: 24});
    text.x = this.CHAR_WIDTH / 2 - text.width / 2;
    container.addChild(text);
    container.char = char;
    container.charIndex = -1;

    return container;
}
console.log("char.js loaded");