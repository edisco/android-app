var APP_WIDTH = 360;
var APP_HEIGHT = 720
var CHAR_WIDTH=32;
var CHAR_HEIGHT=32;
var WORD_Y = 74;

function getAppWidth() {
    return APP_WIDTH;
}
function getAppHeight() {
    return APP_HEIGHT;
}
//Use Pixi's built-in `loader` object to load an image
//PIXI.loader
//  .add("images/ballblue.png")
//  .load(setup);

var game = {
    wordChars: [],
    chars: [],
    drag: null,

    init: function() {
        this.app = new PIXI.Application(getAppWidth(), getAppHeight(), {view:  document.getElementById("canvas"), transparent: true});
        //Add the canvas to the HTML document
//        document.body.appendChild(this.app.view);



    },
    clearStage: function() {
        for(var i = this.app.stage.children.length - 1; i >= 0; i--) {
            this.app.stage.removeChild(stage.children[i]);
        };
    },
    populateChars: function(chars, seed) {
        for(ch in this.chars) {
            this.app.stage.removeChild(ch);

        }
        this.chars = [];

         for (var i = 0, len = chars.length; i < len; i++) {
            var x = this.random(0, 200);
            var y = this.random(0, 200);
            while(!isCharPositionValid(x,y)) {
                var x = this.random(0, 200);
                var y = this.random(0, 200);
            }
            var char = createChar(chars[i], x, y);
            this.chars.push(char);
            this.createDragAndDropFor(char);
            this.app.stage.addChild(char);
        }

    },
    random: function(min, max) {
        return Math.random() * (max - min) + min
    },
    start: function(params) {
            console.log("game word: "+ params.word);
            console.log("game chars: "+params.chars);
            this.clearStage();
            this.word = params.word;
            this.wordChars = new Array(this.word.length);
            this.populateChars(params.chars);



            this.app.stage.addChild(createTitle(params.title, 0, 0));
            this.app.stage.addChild(createWordPlace(this.word, 0, WORD_Y));



    },
    createDragAndDropFor: function(target){
        target.interactive = true;
        target.on("pointerdown", function(e){
            console.log("char:" + this.char);

            game.drag = target;
            game.drag.pivot.y = CHAR_HEIGHT *2;
            game.data = e.data;
        });
        up = function(e) {
            console.log("pointerup");
            if (game.drag) {
//                game.drag.pivot.y = CHAR_HEIGHT / 2;
                if (game.drag.charIndex >= 0)
                    game.wordChars[game.drag.charIndex] = null;
                game.drag.charIndex = null;
                var index = getWordCharIndex(game.drag.x, game.drag.y);
                if (index >= 0) {
                    game.wordChars[index] = game.drag.char;
                    game.drag.charIndex = index;

                }

            }

            updateAndroid();
            game.drag = null;
            game.data = null;
        }
        target.on("pointerup", up)
        target.on("pointerupoutside", up)

        target.on("pointermove", function(e){
            if(game.drag && game.data) {
                var newPosition = game.data.getLocalPosition(this.parent);
                var pos = getSnapPosition(newPosition.x, newPosition.y);
                if (isSnapable(newPosition.x, newPosition.y) && !!pos && !isOccupied(game.drag, pos.x, pos.y)){
                    game.drag.x = pos.x;
                    game.drag.y = pos.y;
                    game.drag.pivot.y = CHAR_HEIGHT / 2;

                } else {
                    game.drag.x = newPosition.x;
                    game.drag.y = newPosition.y;
                    game.drag.pivot.y = CHAR_HEIGHT *2;
                }
            }
      })
    }

}


function createTitle(text) {
    var basicText = new PIXI.Text(text, {fill: "black"});
    basicText.x = APP_WIDTH/2 - basicText.width / 2;
    return basicText;
}

function createWordPlace(word, x, y) {
    var container = new PIXI.Container();

    for (var i = 0, len = word.length; i < len; i++) {
      var char = createChar(" ", getCharPositionX(i), 0);
      container.addChild(char);
    }
    container.x = x;
    container.y = y;
    container.alpha = 0.5;
    return container;

}
function isAndroid() {
    return (typeof android !== 'undefined');
}
function updateAndroid() {
    var text = "";
    for(var i = 0; i < game.wordChars.length; i++) {
        var ch = game.wordChars[i];
        if (typeof ch != "undefined" && ch != null)
            text = text + game.wordChars[i];
        else
            text = text + " ";
    }
    console.log("enteredText: " + text);
    if (isAndroid()){
        console.log("update entered text");
        android.updateEnteredText(text);
    }

}
game.init();
if (isAndroid())
    game.start({word: android.getWord(), chars: android.getChars(), title: android.getTitle()})
else
    game.start({word:"hamster", chars: "hamster", title: "what is хомяк?"});