
function isSnapable(x,y) {
    return !!(getSnapPosition(x,y));
}
function isOccupied(char, x, y) {
    var index = getWordCharIndex(x, y);
    if (index >= 0)
        for(var i = 0; i < game.chars.length; i++) {
            if(char !== game.chars[i] && index == game.chars[i].charIndex)
                return true;
        }
    return false;
}
function getSnapPosition(x, y) {
    for (var i = 0, len = game.word.length; i < len; i++) {
        var dy = y-getCharPositionY();
        var dx = x- getCharPositionX(i);

        if (Math.sqrt(dx*dx + dy*dy) < 30)
            return {x: getCharPositionX(i), y: getCharPositionY()};
    }
    return null;


}
function getWordCharIndex(x, y) {

    for(var i=0; i< game.word.length; i++) {
        if (getCharPositionX(i) == x && getCharPositionY() == y)
            return i;
    }
    return -1;

}
