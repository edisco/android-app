package cc.snakelab.edisco.ui;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cc.snakelab.edisco.MainActivity;
import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.intro.IntroWizardNG;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class IntroWizardTest {
    CountingIdlingResource mIdleCounter;
    @Rule
    public ActivityTestRule<IntroWizardNG> mActivityTestRule = new ActivityTestRule<>(IntroWizardNG.class);
    @Before
    public void setup() {
        App.preference.setShowWizard(true);
        mIdleCounter = mActivityTestRule.getActivity().getIdleCounter(true);
        registerIdlingResources(mIdleCounter);

    }
    @After
    public void tearDown(){
        if (mIdleCounter != null)
            unregisterIdlingResources(mIdleCounter);
    }
    @Test
    public void introWizardTest() throws InterruptedException {

        onView(withId(R.id.mylang_spinner)).check(matches(isDisplayed()));
        onView(withId(R.id.langs)).check(matches(isDisplayed()));
        Thread.sleep(100);
        onView(withId(R.id.goto_next)).perform(click());

        onView(withId(R.id.progressBar2)).check(matches(not(isDisplayed())));
        onView(withId(R.id.goto_next)).check(matches(isDisplayed()));


    }

}
