package cc.snakelab.edisco.main;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.AppDatabase;

import org.junit.After;
import org.junit.Before;


/**
 * Created by snake on 19.01.17.
 */


public class EmulatorTest {
    DatabaseWrapper mDb;
    String mLearnLang;
    String mLang;
    @Before
    public void setUp() {
        mDb = FlowManager.getDatabase(AppDatabase.NAME).getWritableDatabase();
        mDb.beginTransaction();
        mLearnLang = App.preference.getCurrentLearningLang();
        mLang = App.preference.getLang();

        App.preference.setCurrentLearningLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.EN));
        App.preference.setLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.RU));


    }
    @After
    public void tearDown(){
        App.preference.setCurrentLearningLang(mLearnLang);
        App.preference.setLang(mLang);

        mDb.endTransaction();

    }
}
