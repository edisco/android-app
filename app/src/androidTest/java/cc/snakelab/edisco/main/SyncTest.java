package cc.snakelab.edisco.main;

import android.support.test.runner.AndroidJUnit4;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.core.WordMerger;
import cc.snakelab.edisco.core.WordPairMerger;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.Word_Table;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertNull;

/**
 * Created by snake on 27.03.17.
 */

@RunWith(AndroidJUnit4.class)
public class SyncTest extends EmulatorTest{
    JSONObject mUserWord;
    Word mWord;
    long mWordCount;
    WordMerger mWordMerger;
    WordPairMerger mWordPairMerger;
    @Override
    public void setUp() {
        super.setUp();
        try {
            mUserWord = new JSONObject("{" +
                    "type: \"UserWord\", " +
                    "uuid: \"uuid1\"," +
                    "attributes: {" +
                    "text: \"word1\", " +
                    "lang: \"en\", " +
                    "created_at: \"2017-04-25 11:33:59.123 UTC\","+
                    "version: 1}}"
                    );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mWord = new Word();
        mWordCount = SQLite.selectCountOf().from(Word.class).count();
        mWordMerger = new WordMerger();
        mWordPairMerger = new WordPairMerger();

    }

    @Test
    public void jsonRoutines() throws ParseException {
        Word uw = Word.fromJSON(mUserWord);
        assertEquals(1, uw.getVersion());
        assertEquals("word1", uw.getText());
        assertEquals("en", uw.getLang());
        assertEquals("uuid1", uw.getUuid());

    }
    @Test
    public void syncNewWord() {
        mWord.setText("new");
        mWord.setLang("en");
        mWord.setVersion(18);
        mWord.setUuid("uuid1");

        mWordMerger.merge(mWord);
        assertEquals(mWordCount + 1, SQLite.selectCountOf().from(Word.class).count());
        mWord = Word.findByUuid(Word.class, "uuid1");
        assertEquals("new", mWord.getText());
        assertEquals("en", mWord.getLang());
        assertEquals(18, mWord.getVersion());
        assertEquals(true, mWord.isSynced());
    }
    @Test
    public void syncNewWord2() {
        mWord.setText("new");
        mWord.setLang("en");
        Date date =mWord.getCreated_at();
        mWord.save();

        mWord = new Word();
        mWord.setText("new");
        mWord.setLang("en");
        mWord.setUuid("uuid1");
        mWord.setCreated_at(date);
        mWordMerger.merge(mWord);
        assertEquals(mWordCount + 1, SQLite.selectCountOf().from(Word.class).count());
        List<Word> list = SQLite.select().from(Word.class).where(Word_Table.text.eq("new"))
                .and(Word_Table.lang.eq("en")).queryList();
        assertEquals(1, list.size());
        assertEquals("uuid1", list.get(0).getUuid());
        assertEquals(true, list.get(0).isSynced());


    }
    @Test
    public void syncExistedWordSameVers() {
        mWord = Word.findById(Word.class, 1);
        mWord.setText("test_text");
        int ver = mWord.getVersion();
        mWordMerger.merge(mWord);
        assertEquals(mWordCount, SQLite.selectCountOf().from(Word.class).count());
        mWord = Word.findById(Word.class, 1);
        assertNotSame("test_text", mWord.getText());
        assertEquals(false, mWord.isSynced());
        assertEquals(ver+1, mWord.getVersion());


    }
    @Test
    public void syncExistedWord() {
        mWord = Word.findById(Word.class, 1);
        mWord.setText("new text");
        int ver = mWord.getVersion();
        mWord.setVersion(ver + 1);
        mWordMerger.merge(mWord);
        assertEquals(mWordCount, SQLite.selectCountOf().from(Word.class).count());
        mWord = Word.findById(Word.class, 1);
        assertNotSame("new text", mWord.getText());
        assertEquals(true, mWord.isSynced());
        assertEquals(ver + 1, mWord.getVersion());

    }
    @Test
    public void syncDeleteWord() {
        mWord = Word.findById(Word.class, 1);
        mWord.setTrashed(true);
        int ver = mWord.getVersion();
        mWord.setVersion(ver + 1);
        mWordMerger.merge(mWord);
        assertEquals(mWordCount, SQLite.selectCountOf().from(Word.class).count());
        mWord = Word.findById(Word.class, 1);
        assertEquals(true, mWord.isTrashed());
        assertEquals(true, mWord.isSynced());
        assertEquals(ver + 1, mWord.getVersion());


    }
    @Test
    public void mergeWordPairExisted() {
        WordPair wp = WordPair.findById(WordPair.class, 1);
        wp.setStage(2);
        int version = wp.getVersion() + 1;
        wp.setVersion(wp.getVersion() + 1);
        mWordPairMerger.merge(wp);
        wp = WordPair.findById(WordPair.class, 1);
        assertEquals(2, wp.getStage());
        assertEquals(true, wp.isSynced());
        assertEquals(version, wp.getVersion());
    }
}
