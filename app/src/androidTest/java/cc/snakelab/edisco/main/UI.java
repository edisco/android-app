package cc.snakelab.edisco.main;

import android.support.test.runner.AndroidJUnit4;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordContainer;
import cc.snakelab.edisco.ui.WordTripleContainer;

import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 19.05.17.
 */

@RunWith(AndroidJUnit4.class)
public class UI  extends EmulatorTest {

    @Test
    public void mergeWordTriples() {
        List<Word> list = SQLite.select().from(Word.class).where(Word_Table.text.like("fl%")).queryList();
        assertEquals(3, list.size());
        List<BasicWordUIContainer> containerList = new ArrayList<>();
        WordTripleContainer.merge(containerList, list);
        assertEquals(1, containerList.size());
    }

    @Test
    public void mergeWordPairs() {
        List<Word> list = new ArrayList<>();
        list.add(Word.findById(Word.class, 1));
        list.add(Word.findById(Word.class, 1));

        assertEquals(2, list.size());
        List<BasicWordUIContainer> containerList = new ArrayList<>();
        WordContainer.merge(containerList, list);
        assertEquals(1, containerList.size());
    }
    @Test
    public void sortingUIContainers() {
        List<Word> list = new ArrayList<>();
        List<BasicWordUIContainer> containerList = new ArrayList<>();
        list.add(Word.findById(Word.class, 2));
        list.add(Word.findOrCreate("cut", LocaleHelper.EN));

        assertEquals(0, list.get(0).getRetention());
        assertEquals(100, list.get(1).getRetention());
        WordContainer.merge(containerList, list);
        WordTripleContainer.merge(containerList, list);

        assertEquals(2, list.size());
        BasicWordUIContainer.sort(containerList, R.id.sort_by_retention, false);
        assertEquals(100, containerList.get(0).getRetention());
        assertEquals(0, containerList.get(1).getRetention());

        BasicWordUIContainer.sort(containerList, R.id.sort_by_retention, true);
        assertEquals(0, containerList.get(0).getRetention());
        assertEquals(100, containerList.get(1).getRetention());
    }
}
























