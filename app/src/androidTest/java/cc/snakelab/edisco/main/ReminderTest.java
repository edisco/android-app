package cc.snakelab.edisco.main;



import android.support.test.runner.AndroidJUnit4;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.helper.RetentionHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordTriple;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static cc.snakelab.edisco.orm.BasicRecord.NOID;
import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by snake on 31.10.16.
 */
@RunWith(AndroidJUnit4.class)
public class ReminderTest extends EmulatorTest {
    Reminder reminder;
    TimeZone timeZone;

    private WordPair populateWordPair(){
        WordPair wp = new WordPair("egg", "яйцо", "en", "ru");
        wp.setStage(RetentionHelper.STAGE_14);
        wp.save();
        WordPairHistory wph = new WordPairHistory(true, wp.getId());

        Calendar calender = DateHelper.getCalendar();
        calender.add(Calendar.DAY_OF_YEAR, -14);
        wph.setCreated_at(calender.getTime());
        wph.save();
        return wp;
    }

    @Override
    public void tearDown() {
        super.tearDown();
        TimeZone.setDefault(timeZone);
    }

    @Before
    public void setUp() {
        super.setUp();

        reminder = Reminder.get();
        timeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+2"));
    }

    @Test
    public void TestDate() throws ParseException {

        TimeZone.setDefault(TimeZone.getTimeZone("GMT+2"));
        Date date = DateHelper.iso8601ZToDate("2017-04-13 13:07:59.123 UTC");
        assertEquals("2017-04-13 15:07:59.123 +0200", DateHelper.dateToIso8601Z(date));
    }
    @Test
    public void TestReminder() throws Exception {
        assertEquals(8, reminder.getStartHour());
        assertEquals(0, reminder.getStartMin());
        assertEquals(20, reminder.getFinishHour());
        assertEquals(0, reminder.getFinishMin());
        assertEquals(8, reminder.calcRemindsPerDay());
        assertEquals(true, reminder.isActive());
        assertEquals("1, 3", reminder.getPairs());
        assertEquals("1, 3", reminder.getTriples());
        assertEquals(true, reminder.isGuessPair());
        assertEquals("2016-10-31 10:32:01.000 +0200", DateHelper.dateToIso8601Z(reminder.getLastRemind()));
        assertEquals(1, reminder.getCurrentWordPairId());
        assertEquals(true, reminder.isGuessFront());

        reminder.setActive(false);
        reminder.setLastRemind(DateHelper.iso8601ZToDate("2016-12-09 16:32:01.0 +0200"));
        reminder.save();
        reminder = Reminder.get();
        assertEquals("2016-12-09 16:32:01.000 +0200", DateHelper.dateToIso8601Z(reminder.getLastRemind()));
    }
    @Test
    public void TestSetNewPair(){
        assertEquals("1, 3", reminder.getPairs());
        long id = reminder.getCurrentWordPair().getId();
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        assertNotEquals(id, reminder.getCurrentWordPair().getId());
        assertNotEquals(null, reminder.getCurrentWordPair());
        assertEquals(1, reminder.getWordPairs().size());
        id = reminder.getCurrentWordPair().getId();
        reminder.setNewPair();
        assertEquals(id, reminder.getCurrentWordPair().getId());

    }
    @Test
    public void TestSetNewPair2() {
        WordPair wp = WordPair.findById(WordPair.class, 2);
        reminder.includeWordPair(wp);
        assertEquals("1, 3, 2", reminder.getPairs());
        assertEquals(1, reminder.getCurrentWordPair().getId());
        reminder.setNewPair();
        assertEquals(3, reminder.getCurrentWordPair().getId());
        assertEquals("3, 2, 1", reminder.getPairs());
        reminder.setNewPair();
        assertEquals(2, reminder.getCurrentWordPair().getId());
        assertEquals("2, 1, 3", reminder.getPairs());
        reminder.setNewPair();
        assertEquals(1, reminder.getCurrentWordPair().getId());
        assertEquals("1, 3, 2", reminder.getPairs());


    }

    @Test
    public void TestIsTime() throws Exception {
            //lastRemind = "2016-10-31 10:32:01.0 GMT+2
            assertEquals(true, reminder.isTime(DateHelper.iso8601ToDate("2016-10-31 12:02:01.1 +02")));
            assertEquals(false, reminder.isTime(DateHelper.iso8601ToDate("2016-10-31 09:32:00.0 +02")));
            assertEquals(false, reminder.isTime(DateHelper.iso8601ToDate("2016-10-31 12:02:00.0 +02")));
            assertEquals(false, reminder.isTime(DateHelper.iso8601ToDate("2016-10-31 20:00:01.0 +02")));
    }

    @Test
    public void TestGuess() throws Exception {
        Word w =reminder.getGuessWord();
        assertEquals(1, w.getId());
        assertEquals("cat", w.toString());
        w = reminder.getTranslationWord();
        assertEquals(2, w.getId());
        assertEquals("кот", w.toString());

        assertEquals(false, reminder.isCorrect("cat"));
        assertEquals(true, reminder.isCorrect("кот"));
        reminder.setNewPair();
        reminder.setGuessFront(true);
        w =reminder.getGuessWord();

        assertEquals(5, w.getId());
        assertEquals("hamster", w.toString());
        w = reminder.getTranslationWord();
        assertEquals(6, w.getId());
        assertEquals("хомяк", w.toString());
        assertEquals(true, reminder.isCorrect("хомяк"));
    }

    @Test
    public void TestSave() throws Exception {
        assertEquals("cat", reminder.getGuessWord().toString());
        reminder.setNewPair();
        reminder.setGuessFront(true);
        assertEquals("hamster", reminder.getGuessWord().toString());
        Reminder r2 = Reminder.get();
        assertEquals("cat", r2.getGuessWord().toString());
        reminder.save();
        r2 = Reminder.get();
        assertEquals("hamster", r2.getGuessWord().toString());
    }

    @Test
    public void TestGuessMask() throws Exception {
        assertEquals("к*т", reminder.getTranslMask("кат"));
        assertEquals("к*т", reminder.getTranslMask("катяра"));
        assertEquals("кот", reminder.getTranslMask("котяра"));
        assertEquals("ко*", reminder.getTranslMask("Ко"));
        assertEquals("кот", reminder.getTranslMask("КоТ"));
    }

    @Test
    public void TestReminderPairs() throws Exception {
        assertEquals(2, reminder.getWordPairs().size());
    }

    @Test
    public void TestNextRemind() throws ParseException {
        assertEquals("2016-10-31 12:02:01.000 +0200", DateHelper.dateToIso8601Z(reminder.calcNextRemind()));
        assertEquals("2016-10-31 13:32:01.000 +0200", DateHelper.dateToIso8601Z(reminder.calcNextRemind(DateHelper.iso8601ToDate("2016-10-31 12:03:01.000 +02"))));
        Date date = DateHelper.iso8601ZToDate("2016-10-31 19:11:02.123 +0200");
        reminder.setLastRemind(date);
        assertEquals("2016-11-01 08:41:02.123 +0200", DateHelper.dateToIso8601Z(reminder.calcNextRemind(date)));
    }

    @Test
    public void TestIncludeExcludeWordPair() {
        WordPair wp = new WordPair("жопа", "ass", "rus", "eng");
        wp.save();
        reminder.includeWordPair(wp);
        assertEquals(3, reminder.getWordPairs().size());
        assertEquals("1, 3, 6", reminder.getPairs());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        assertEquals(2, reminder.getWordPairs().size());
        assertEquals("3, 6", reminder.getPairs());
        wp = WordPair.findById(WordPair.class, 2);
        reminder.excludeWordPair(wp);
        assertEquals("3, 6", reminder.getPairs());

    }

    @Test
    public void TestdoLearnt() {
        populateWordPair();
        reminder.getCurrentWordPair().doCorrectAnswer(true);
        assertEquals(0, WordPair.findById(WordPair.class, 1).calcRetention());
        reminder.doLearnt();
        assertEquals(3, reminder.getCurrentWordPairId());

        WordPair wp = WordPair.findById(WordPair.class, 1);
        assertNotEquals(RetentionHelper.STAGE_0, wp.getStage());
        assertEquals(100, wp.calcRetention());



        assertEquals(100, WordPair.findById(WordPair.class, 2).calcRetention());
        assertEquals(33, WordPair.findById(WordPair.class, 6).calcRetention());
        assertEquals("3, 6", reminder.getPairs());
    }

    @Test
    public void TestDoLearnt2() {
        populateWordPair();
        assertEquals("1, 3", reminder.getPairs());
        reminder.doLearnt();
        assertEquals("3, 6", reminder.getPairs());


    }
    @Test
    public void IncludeWP() {
        reminder.setPairs("");
        WordPair wp = WordPair.findById(WordPair.class, 1);
        reminder.includeWordPair(wp);
        assertEquals("1", reminder.getPairs());
    }
    @Test
    public void IsWordUsed() {
        assertEquals(true, reminder.isWordUsed(Word.findById(Word.class, 1)));
        assertEquals(true, reminder.isWordUsed(Word.findById(Word.class, 2)));
        assertEquals(false, reminder.isWordUsed(Word.findById(Word.class, 3)));
        assertEquals(false, reminder.isWordUsed(Word.findById(Word.class, 4)));
    }
    @Test
    public void Triple() {
        assertEquals(2, reminder.getWordTriples().size());
        assertEquals(1, reminder.getCurrentWordTripleId());
        WordTriple wt = WordTriple.findById(WordTriple.class, 2);
        reminder.includeWordTriple(wt);
        assertEquals("1, 3, 2", reminder.getTriples());
        wt = WordTriple.findById(WordTriple.class, 1);
        reminder.excludeWordTriple(wt);
        assertEquals("3, 2", reminder.getTriples());
        assertEquals(3, reminder.getCurrentWordTripleId());
        reminder.setNewTriple();
        assertEquals("2, 3", reminder.getTriples());
        assertEquals(false, reminder.isWordUsed(wt.getOrgWord1()));
        wt = WordTriple.findById(WordTriple.class, 2);
        assertEquals(true, reminder.isWordUsed(wt.getOrgWord1()));

    }

    @Test
    public void Triple2() {
        assertEquals(true, reminder.isGuessPair());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        assertEquals("", reminder.getPairs());
        assertEquals(false, reminder.isGuessPair());
    }

    @Test
    public void Triple3() {
        reminder.setGuessPair(false);
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        assertEquals("", reminder.getTriples());
        assertEquals(true, reminder.isGuessPair());
    }
    @Test
    public void TripleGuess() {
        reminder.setGuessPair(false);
        String text = reminder.getCurrentWordTriple().getTranslWord().getText();

        assertEquals(true, reminder.isCorrect(text));
    }
    @Test
    public void TripleAndPair() {
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
//        reminder.includeWordPair(WordPair.findById(WordPair.class, 1));
        reminder.includeWordTriple(WordTriple.findById(WordTriple.class,1));
        assertEquals(false, reminder.isGuessPair());
    }
    @Test
    public void GuessNotifierTriple() {
        reminder.setGuessPair(false);
        assertNotNull(reminder.getGuessWord());

    }
    @Test
    public void TripleLearnt() {
        reminder.setGuessPair(false);
        assertEquals("1, 3", reminder.getTriples());
        assertEquals(1, reminder.getCurrentWordTripleId());
        reminder.getCurrentWordTriple().doCorrectAnswer(true);
        reminder.doLearnt();
        assertEquals(100, WordTriple.findById(WordTriple.class, 1).calcRetention());
        assertEquals(100, WordTriple.findById(WordTriple.class, 2).calcRetention());


        assertEquals("3, 4", reminder.getTriples());

    }
    @Test
    public void RemovePairFromReminder() {
        assertEquals(1, reminder.getCurrentWordPair().getId());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        assertEquals(3, reminder.getCurrentWordPair().getId());
        reminder.excludeWordPair(reminder.getCurrentWordPair());
        assertEquals(null, reminder.getCurrentWordPair());
        assertEquals(NOID, reminder.getCurrentWordPairId());
        assertFalse(reminder.isGuessPair());
        assertEquals(1, reminder.getCurrentWordTripleId());
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        assertEquals(3, reminder.getCurrentWordTripleId());
        reminder.excludeWordTriple(reminder.getCurrentWordTriple());
        assertEquals(null, reminder.getCurrentWordTriple());
        assertEquals(NOID, reminder.getCurrentWordTripleId());


    }
    @Test
    public void removePairFromReminder2() {
        reminder.setPairs("4");
        reminder.setCurrentWordPairId(4);
        reminder.save();
        assertEquals(4, reminder.getCurrentWordPairId());
        // 8 net
        // 9 чистый доход
        // 10 сеть
        Word word = Word.findById(Word.class, 8);
        List<Word> transList = new ArrayList<>(word.getPairTranslations());
        assertEquals(2, transList.size());
        assertEquals(9, transList.get(0).getId());
        transList.remove(0);
        word.mergePairTranslations(transList, reminder);

        word = Word.findById(Word.class, 8);
        assertEquals(1, word.getPairTranslations().size());
        assertEquals("", Reminder.get().getPairs());


    }
}


