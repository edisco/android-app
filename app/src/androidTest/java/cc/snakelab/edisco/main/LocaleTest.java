package cc.snakelab.edisco.main;

import android.content.ReceiverCallNotAllowedException;
import android.support.test.runner.AndroidJUnit4;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.LocaleHelper;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.RegEx;

import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 23.02.17.
 */

@RunWith(AndroidJUnit4.class)
public class LocaleTest extends EmulatorTest {
    @Test
    public void Test1() {
        String locale =LocaleHelper.getCurrentLang(App.app);
        assert("en" == locale || "ru" == locale);
        assertEquals("en", LocaleHelper.stringToCodeLang(App.app, App.app.getResources().getString(R.string.english)));
        assertEquals("uk", LocaleHelper.stringToCodeLang(App.app, App.app.getResources().getString(R.string.ukrainian)));
        assertEquals("ru", LocaleHelper.stringToCodeLang(App.app, App.app.getResources().getString(R.string.russian)));

        assertEquals(App.app.getResources().getString(R.string.russian), LocaleHelper.langCodeToString(App.app, "ru"));
        assertEquals(App.app.getResources().getString(R.string.english), LocaleHelper.langCodeToString(App.app, "en"));
        assertEquals(App.app.getResources().getString(R.string.ukrainian), LocaleHelper.langCodeToString(App.app, "uk"));
    }
    @Test
    public void TestDateHelper() throws ParseException {


        Date now = DateHelper.iso8601ToDate("2017-05-30 12:36:30.555 +3");
        assertEquals(App.app.getString(R.string.today), DateHelper.getReadableFormat(DateHelper.iso8601ToDate("2017-05-30 00:36:30.555 +3"), now, App.app));
        assertEquals(App.app.getString(R.string.yesterday), DateHelper.getReadableFormat(DateHelper.iso8601ToDate("2017-05-29 00:36:30.555 +3"), now, App.app));
        String may28 = DateHelper.getReadableFormat(DateHelper.iso8601ToDate("2017-05-28 00:36:30.555 +3"), now, App.app);
        assertEquals(true, may28.contains("28"));
    }

}
