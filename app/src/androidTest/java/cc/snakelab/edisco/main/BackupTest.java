package cc.snakelab.edisco.main;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;

import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.Backup;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordTriple;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.TimeZone;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by snake on 27.01.17.
 */

@RunWith(AndroidJUnit4.class)
public class BackupTest extends EmulatorTest {
    Context mContext;
    @Override
    public void setUp() {
        super.setUp();
        mContext = App.app;

    }

    @Test
    public void TestBackup() throws Exception {
        mContext.deleteFile("backup.json");
        Backup.save();
        String[] list = mContext.fileList();
        boolean b = false;
        for(String str: list) {
            b = b || str.equalsIgnoreCase("backup.json");
        }

        assertEquals(true, b);

    }
    @Test
    public void TestLoad() throws Exception {
        Backup.save();
        long wpSize = SQLite.select().from(WordPair.class).queryList().size();
        long wSize = SQLite.select().from(Word.class).queryList().size();
//        SQLite.delete().from(WordPair.class);

        Delete.table(WordPair.class);

        assertEquals(0, SQLite.select().from(WordPair.class).queryList().size());
        Backup.load();
        assertEquals(wpSize, SQLite.select().from(WordPair.class).queryList().size());
        List<Word> list = SQLite.select().from(Word.class).queryList();
        assertEquals(wSize, list.size());

    }

    @Test
    public void TestLoadTriple() throws Exception {
        Backup.save();
        long size = SQLite.selectCountOf().from(WordTriple.class).count();
        assertNotEquals(0, size);
        Delete.table(WordTriple.class);
        assertEquals(0, SQLite.selectCountOf().from(WordTriple.class).count());
        Backup.load();
        assertNotEquals(0, SQLite.selectCountOf().from(WordTriple.class).count());
        WordTriple wt = SQLite.select().from(WordTriple.class).queryList().get(0);
        assertEquals("fly", wt.getOrgWord1().getText());
        assertEquals("flew", wt.getOrgWord2().getText());
        assertEquals("flown", wt.getOrgWord3().getText());
        assertEquals("летать", wt.getTranslWord().getText());
        assertEquals(LocaleHelper.RU, wt.getTranslWord().getLang());
        assertEquals(LocaleHelper.EN, wt.getOrgWord1().getLang());


    }

    @Test
    public void TestSaveReminder() throws Exception {
        TimeZone timeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+2"));
        try {
            Backup.save();
            Delete.table(Reminder.class);
            assertEquals(0, SQLite.selectCountOf().from(Reminder.class).count());
            Backup.load();
            assertEquals(1, SQLite.selectCountOf().from(Reminder.class).count());

            Reminder reminder = Reminder.get();
            assertEquals(8, reminder.getStartHour());
            assertEquals(0, reminder.getStartMin());
            assertEquals(20, reminder.getFinishHour());
            assertEquals(0, reminder.getFinishMin());
            assertEquals(8, reminder.calcRemindsPerDay());
            assertEquals(true, reminder.isActive());
            assertEquals("1, 3", reminder.getPairs());
            assertEquals("2016-10-31 10:32:01.000 +0200", DateHelper.dateToIso8601Z(reminder.getLastRemind()));
            assertEquals(1, reminder.getCurrentWordPairId());
            assertEquals(true, reminder.isGuessFront());
        }finally {
            TimeZone.setDefault(timeZone);
        }

    }
}
