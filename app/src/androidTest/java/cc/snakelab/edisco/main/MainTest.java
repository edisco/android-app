package cc.snakelab.edisco.main;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.helper.MiscHelper;
import cc.snakelab.edisco.helper.RetentionHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordPair_Table;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTriple_Table;
import cc.snakelab.edisco.orm.Word_Table;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by snake on 01.10.16.
 */


@RunWith(AndroidJUnit4.class)
public class MainTest extends EmulatorTest{

    @Test
    public void TestWordPairs() throws Exception {

        List<WordPair> wp = SQLite.select().from(WordPair.class).queryList();

        assertEquals(5, wp.size());
        WordPair wordPair = new WordPair(2, 1);
        wordPair.save();
        assertNotEquals(-1, wp.get(0).getId());
        assertNotEquals(-1, wp.get(1).getId());
        assertNotEquals(-1, wordPair.getId());
        assertEquals(6,  SQLite.select().from(WordPair.class).queryList().size());
        wordPair.delete();
        assertEquals(5,  SQLite.select().from(WordPair.class).queryList().size());
    }
    @Test
    public void TestWordPair() {
        int count = SQLite.select().from(WordPair.class).queryList().size();
        WordPair wp = new WordPair("дом", "house", "rus", "eng");
        assertEquals(count,  SQLite.select().from(WordPair.class).queryList().size());
        wp.save();
        assertEquals(count + 1,  SQLite.select().from(WordPair.class).queryList().size());
    }

    @Test
    public void TestWords() throws Exception {
        List<Word> w =  Word.getList();
        int wordSize = w.size();
        assertTrue(w.size() > 0);
        Word word =new Word(-1, "hello", "eng");
        word.save();
        assertNotEquals(-1, word.getId());
        assertEquals(wordSize+ 1,  Word.getList().size());
        word.delete();
        assertEquals(wordSize,  Word.getList().size());
        word = new Word("new word", "eng");
        word.trash();
        assertEquals(wordSize,  Word.getList().size());
        assertEquals(wordSize + 2,  SQLite.select().from(Word.class).queryList().size());

    }
    @Test
    public void TestWord2() {
        Word w = new Word("type", "eng");
        assertEquals(0, w.getWordPairList().size());
        int wCount = Word.getList().size();
        int wpCount = SQLite.select().from(WordPair.class).queryList().size();
        w.addTranslation("вводить", "rus");
        assertEquals(1, w.getPairTranslations().size());
        assertEquals(wCount + 2, Word.getList().size());
        assertEquals(wpCount + 1, SQLite.select().from(WordPair.class).queryList().size());
        assertEquals(1, w.getWordPairList().size());
        w.addTranslation("тип", "rus");
        assertEquals(2, w.getWordPairList().size());
        assertEquals(wCount + 2 + 1, Word.getList().size());
        assertEquals(wpCount + 1+1, SQLite.select().from(WordPair.class).queryList().size());
        assertEquals(2, w.getPairTranslations().size());



    }
    @Test
    public void TestGetTranslations() {
        Word w = Word.findById(Word.class, 1);
        assertEquals(1, w.getPairTranslations().size());
        assertEquals("cat", w.getText());
        w = w.getPairTranslations().get(0);
        assertEquals("кот", w.getText());
        assertEquals("cat", w.getPairTranslations().get(0).getText());
    }
    @Test
    public void TestWordPairhasLearnt() {
        WordPair wp = SQLite.select().from(WordPair.class).queryList().get(0);
        assertEquals(false, wp.hasLearnt());
        wp.doCorrectAnswer(true);
        assertEquals(false, wp.hasLearnt());
        wp.doCorrectAnswer(false);
        assertEquals(false, wp.hasLearnt());
    }

    @Test
    public void TestWordPairhasLearnt2() {
        WordPair wp = SQLite.select().from(WordPair.class).queryList().get(0);
        WordPairHistory wph = new WordPairHistory(true, wp.getId());
        Calendar cal = DateHelper.getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        cal.add(Calendar.MINUTE, 1);
        wph.setCreated_at(cal.getTime());
        //by default user has 8 reminds per day.
        // hasLearnt evaluates that amount of correct answers are >=90% for last day and
        // there are 4 or more answers(50% of 8)
        wph.save();//1

        wph.setId(-1);
        wph.save();//2
        wph.setId(-1);
        wph.save();//3
        assertEquals(false, wp.hasLearnt());
        wph.setId(-1);
        wph.save();//4

        assertEquals(true, wp.hasLearnt());
    }

    @Test
    public void TestWordTripleHistory() {
        WordTriple wt = SQLite.select().from(WordTriple.class).where(WordTriple_Table.id.eq((long) 2)).querySingle();

        assertEquals(100, wt.calcRetention());
    }

    @Test
    public void TestIso8601() throws ParseException {
        //date in ISO8601 format in utc timezone
        String txt = "2016-12-30 13:29:30.555 +0";
        TimeZone timeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+2"));
        Date date = DateHelper.iso8601ZToDate(txt);
        assertEquals(15, DateHelper.getHours(date));
        assertEquals("2016-12-30 15:29:30.555 +0200", DateHelper.dateToIso8601Z(date));
        TimeZone.setDefault(timeZone);
    }
    @Test
    public void TestDateHelper() {
        Calendar cal = DateHelper.getCalendar();
        Date date = cal.getTime();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        assertEquals(1, DateHelper.getDaysBetween(date, cal.getTime()));
    }

    @Test
    public void TestWordPairHistory() {

        List<WordPair> wpList = SQLite.select().from(WordPair.class).queryList();
        int size = SQLite.select().from(WordPairHistory.class).queryList().size();
        WordPairHistory wph = new WordPairHistory(true, wpList.get(0).getId());
        wph.save();
        assertEquals(size + 1, SQLite.select().from(WordPairHistory.class).queryList().size());
        wph.delete();
        assertEquals(size, SQLite.select().from(WordPairHistory.class).queryList().size());

    }

    @Test
    public void TestWordPairRating() {
        WordPair wp = new WordPair("ass", "жопа", "eng", "rus");
        wp.save();
        assertEquals(0, wp.calcRetention());
        wp.doCorrectAnswer(true);
        wp.setStage(RetentionHelper.STAGE_2);
        assertEquals(100, wp.calcRetention());
        Calendar cal = DateHelper.getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, 2);

        assertEquals(33, wp.calcRetention(cal.getTime()));

        wp.setStage(RetentionHelper.STAGE_14);
        cal.add(Calendar.DAY_OF_YEAR, 12);
        assertEquals(33, wp.calcRetention(cal.getTime()));

        wp.setStage(RetentionHelper.STAGE_60);
        cal.add(Calendar.DAY_OF_YEAR, 45);
        assert(wp.calcRetention(cal.getTime()) == 33 || wp.calcRetention(cal.getTime()) == 34);


    }
    @Test
    public void TestWordPairRating2() {
        WordPairHistory wph = new WordPairHistory(true, 1);
        wph.setCreated_at(new Date(0));
        wph.save();

        wph = new WordPairHistory(true, 1);
        wph.save();

        WordPair wp = WordPair.findById(WordPair.class, 1);
        wp.setStage(RetentionHelper.STAGE_2);

        assertEquals(100, wp.calcRetention());
    }
    @Test
    public void TestWordPairStages() {
        WordPair wp = new WordPair("ass", "жопа", "eng", "rus");
        assertEquals(RetentionHelper.STAGE_0, wp.getStage());
        wp.nextStage();
        assertEquals(RetentionHelper.STAGE_2, wp.getStage());
        wp.nextStage();
        assertEquals(RetentionHelper.STAGE_14, wp.getStage());
        wp.nextStage();
        assertEquals(RetentionHelper.STAGE_60, wp.getStage());
        wp.nextStage();
        assertEquals(RetentionHelper.STAGE_60, wp.getStage());


    }
    @Test
    public void TestWordPairGetAll() {
        WordPair wp = WordPair.findById(WordPair.class, 1);
        wp.nextStage();
        wp.save();
        List<WordPair> list = SQLite.select().from(WordPair.class).queryList();
        Collections.sort(list, new Comparator<WordPair>() {
            @Override
            public int compare(WordPair wordPair, WordPair t1) {
                return (int) (wordPair.getId() - t1.getId());
            }
        });
        assertEquals(1, list.get(0).getId());
        assertEquals(RetentionHelper.STAGE_2, list.get(0).getStage());

    }

    @Test
    public void TestWordPairDeleted() {
        List<WordPair> list = SQLite.select().from(WordPair.class).queryList();
        int count = list.size();
        WordPair wp = list.get(0);
        wp.setTrashed(true);
        wp.save();
//        assertEquals(count - 1, WordPair.where(WordPair.class, "deleted = 0", null, null, null).size());

    }
    @Test
    public void TestWordTriple() {
        List<WordTriple> list = SQLite.select().from(WordTriple.class).queryList();

        WordTriple wt = list.get(0);
        assertEquals("fly", wt.getOrgWord1().getText());
        assertEquals("flew", wt.getOrgWord2().getText());
        assertEquals("flown", wt.getOrgWord3().getText());
        assertEquals("летать", wt.getTranslWord().getText());
        Word w  = wt.getOrgWord2();
        assertEquals("летать", w.getTripleTranslations().get(0).getText());
        w = wt.getTranslWord();
        assertEquals("fly", w.getTripleTranslations().get(0).getText());
        assertEquals("flew", w.getTripleTranslations().get(1).getText());
        assertEquals("flown", w.getTripleTranslations().get(2).getText());
    }
    @Test
    public void getWordList() {
        List<Word> list = Word.getList();
        assertTrue(list.size() > 0);
    }
    @Test
    public void mergeWords() {
        Word word = Word.findById(Word.class, 1);
        long size = SQLite.selectCountOf().from(WordPair.class).where(WordPair_Table.trashed.eq(false)).count();
        List<Word> newTrans = new ArrayList<Word>(word.getPairTranslations());
        //add
        newTrans.add(new Word("1", "uk"));
        word.mergePairTranslations(newTrans, App.app.getReminder());
        assertEquals(size + 1, SQLite.selectCountOf().from(WordPair.class).where(WordPair_Table.trashed.eq(false)).count());
        word = Word.findById(Word.class, 1);
        assertEquals(2, word.getPairTranslations().size());

        //del
        newTrans.remove(0);
        word.mergePairTranslations(newTrans, App.app.getReminder());
        assertEquals(size, SQLite.selectCountOf().from(WordPair.class).where(WordPair_Table.trashed.eq(false)).count());

        //start
        Word newWord = newTrans.get(0);
        newWord.setText("2");
        word.mergePairTranslations(newTrans, App.app.getReminder());
        word = Word.findById(Word.class, 1);
        assertEquals("2", word.getPairTranslations().get(0).getText());

    }
    @Test
    public void deleteOnlyOneTranslation() {
        Word word = Word.findById(Word.class, 8); // net
        List<Word> newTrans = new ArrayList<Word>(word.getPairTranslations());
        newTrans.remove(0);
        word.mergePairTranslations(newTrans, App.app.getReminder());
        word = Word.findById(Word.class, 8);
        assertTrue(!word.isTrashed());


    }
    @Test
    public void mergeWords2() {
        Word w = new Word("snake", "eng");
        List<Word> list = new ArrayList<>();
        list.add(new Word("змей", "rus"));
        long wpSize = SQLite.selectCountOf().from(WordPair.class).where(WordPair_Table.trashed.eq(false)).count();
        long wSize = SQLite.selectCountOf().from(Word.class).where(Word_Table.trashed.eq(false)).count();
        w.mergePairTranslations(list, App.app.getReminder());
        assertEquals(wpSize + 1, SQLite.selectCountOf().from(WordPair.class).where(WordPair_Table.trashed.eq(false)).count());
        assertEquals(wSize + 2, SQLite.selectCountOf().from(Word.class).where(Word_Table.trashed.eq(false)).count());

    }
    @Test
    public void mergeWords3() {
        //two different words with same translation
        //kat (de)
        Word w = Word.findById(Word.class, 11);
        List<Word> list = new ArrayList<>();
        //кот уже есьб в бд
        list.add(new Word("кот", LocaleHelper.RU));
        long wpSize = SQLite.selectCountOf().from(WordPair.class).count();
        long wSize = SQLite.selectCountOf().from(Word.class).count();
        w.mergePairTranslations(list, App.app.getReminder());
        assertEquals(wpSize + 1, SQLite.selectCountOf().from(WordPair.class).count());
        assertEquals(wSize, SQLite.selectCountOf().from(Word.class).count());


    }
    @Test
    public void mergeEmptyWords() {
        Word word = Word.findById(Word.class, 1);
        long wpSize = SQLite.selectCountOf().from(WordPair.class).count();
        List<Word> list = new ArrayList<>(word.getPairTranslations());
        list.add(new Word("", LocaleHelper.RU));
        list.add(new Word("  ", LocaleHelper.RU));
        word.mergePairTranslations(list, App.app.getReminder());
        assertEquals(wpSize, SQLite.selectCountOf().from(WordPair.class).count());
    }
    @Test
    public void wordDistance(){
        assertEquals(1, MiscHelper.wordDistance("cat", "cot"));
        assertEquals(7, MiscHelper.wordDistance("aaapppp", ""));
        assertEquals(7, MiscHelper.wordDistance("elephant", "hippo"));
        assertEquals(7, MiscHelper.wordDistance("hippo", "elephant"));

    }
    @Test
    public void findSimilarWord() {
        List<Word> list = Word.findSimilarWords("cat", LocaleHelper.EN, 3);
        assertEquals("cat", list.get(0).getText());
        assertEquals("cut", list.get(1).getText());
    }
    @Test
    public void getSimilarChars() {
        assertEquals("catu", MiscHelper.getSimilarChars("cat", LocaleHelper.EN, 3));
        assertEquals("собакаеть", MiscHelper.getSimilarChars("собака", LocaleHelper.RU, 3));
    }


}
