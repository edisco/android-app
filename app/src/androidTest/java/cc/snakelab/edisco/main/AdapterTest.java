package cc.snakelab.edisco.main;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.OperatorGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.FlowCursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import cc.snakelab.edisco.R;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.helper.FilterHelper;
import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.orm.AppDatabase;
import cc.snakelab.edisco.orm.Reminder;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.Word_Table;
import cc.snakelab.edisco.ui.BasicWordUIContainer;
import cc.snakelab.edisco.ui.WordContainer;
import cc.snakelab.edisco.ui.WordTripleContainer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by snake on 15.06.17.
 */

@RunWith(AndroidJUnit4.class)

public class AdapterTest extends EmulatorTest {

    List<Word> mList;

    @Override
    public void setUp() {
        super.setUp();
        App.preference.setLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.RU));
        App.preference.setCurrentLearningLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.EN));

    }

    @Test
    public void testSort1() throws InterruptedException {
        mList = FilterHelper.query("", 0, 100);
        assertEquals(true, mList.size() > 0 );

    }


    @Test
    public void testSortByRetention2() {
        int bakId = App.preference.getSortingTypeId();
        try {
            App.preference.setSortingTypeId(R.id.sort_by_retention);
            mList = FilterHelper.query("", 0, 100);
            String lang = App.preference.getCurrentLearningLang();
            String nativeLang =  LocaleHelper.stringToCodeLang(App.app, App.preference.getLang());
            List<Word> listWord = SQLite.select().from(Word.class).where(Word_Table.trashed.eq(false)).and(OperatorGroup.clause().and(Word_Table.lang.eq(lang))
                    .or(Word_Table.lang.eq(nativeLang))).orderBy(Word_Table.text, true).queryList();
            FilterHelper.sortByRetention(listWord);
//            assertEquals(listWord.size(), mList.size());
            for(Word w : mList) {
                Log.d("mList", String.format("%d %s %d", w.getId(), w.getText(), w.getRetention()));
            }
//            for(Word w : listWord) {
//                Log.d("listWord", String.format("%d %s %d", w.getId(), w.getText(), w.getRetention()));
//            }
            int ret = 0;
            for(int i=0; i< mList.size(); i++) {
                assert(mList.get(i).getRetention() >= ret);
                ret = mList.get(i).getRetention();
            }
            assertEquals(true, mList.size() > 0);

        }finally {
            App.preference.setSortingTypeId(bakId);
        }
    }

    @Test
    public void testSortByRetention2Asc() {
        int bakId = App.preference.getSortingTypeId();
        boolean bakAsc = App.preference.isSortingDesc();
        try {
            App.preference.setSortingTypeId(R.id.sort_by_retention);
            App.preference.setSortingDesc(false);
            mList = FilterHelper.query("", 0, 100);
            List<Word> listWord = FilterHelper.query("", 0, 100);
            FilterHelper.sortByRetention(listWord);
            assertEquals(listWord.size(), mList.size());
            for(Word w : mList) {
                Log.d("mList", String.format("%d %s %d", w.getId(), w.getText(), w.getRetention()));
            }
            for(Word w : listWord) {
                Log.d("listWord", String.format("%d %s %d", w.getId(), w.getText(), w.getRetention()));
            }
            for(int i=mList.size() - 1; i > mList.size() - 4; i--) {
                assertEquals(mList.get(i).getId(), listWord.get(i).getId());
            }
            assertEquals(true, mList.size() > 0);

        }finally {
            App.preference.setSortingDesc(bakAsc);
            App.preference.setSortingTypeId(bakId);
        }
    }
    @Test
    public void testFindWithRetentionSort() {
        int bakId = App.preference.getSortingTypeId();
        try {
            App.preference.setSortingTypeId(R.id.sort_by_retention);
            mList = FilterHelper.query("cat", 0, 100);
            assertEquals(1, mList.size());
        } finally {
            App.preference.setSortingTypeId(bakId);
        }
    }
    @Test
    public void testSortByDate() {
        int bakId = App.preference.getSortingTypeId();
        boolean bakDesc = App.preference.isSortingDesc();
        Reminder reminder = Reminder.get();

        List<BasicWordUIContainer> containerList = new ArrayList<>();
        try {
            App.preference.setSortingTypeId(R.id.sort_by_date);
            App.preference.setSortingDesc(true);
            WordPair wp = new WordPair("first", "первый", LocaleHelper.EN, LocaleHelper.RU);
            wp.save();

            wp = new WordPair("second", "второй", LocaleHelper.EN, LocaleHelper.RU);
            wp.save();

            FilterHelper.fetchContainers(containerList, "", 0, 100);
            assertEquals("second", containerList.get(0).getHead());
            assertEquals("first", containerList.get(1).getHead());

            reminder.setPairs(""+wp.getId());
            reminder.setCurrentWordPairId(wp.getId());
            reminder.setGuessPair(true);
            reminder.save();
            assertTrue(reminder.isGuessPair());

            assertEquals(wp.getId(), reminder.getCurrentWordPairId());
            wp.doCorrectAnswer(true);
            reminder.doLearnt();
            wp = WordPair.findById(WordPair.class,wp.getId());
            assertEquals(100, wp.calcRetention());

            containerList.clear();
            FilterHelper.fetchContainers(containerList, "", 0, 100);
            assertEquals("second", containerList.get(0).getHead());
            assertEquals("first", containerList.get(1).getHead());


        } finally {
            App.preference.setSortingTypeId(bakId);
            App.preference.setSortingDesc(bakDesc);
        }
    }
    @Test
    public void queryRightLang() {
        App.preference.setLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.EN));
        App.preference.setCurrentLearningLang(LocaleHelper.langCodeToString(App.app, LocaleHelper.RU));
        List<BasicWordUIContainer> containerList = new ArrayList<>();
        FilterHelper.fetchContainers(containerList, "", 0, 20);
        boolean correct= true;
        for(BasicWordUIContainer cont: containerList) {
            if (cont.getRecord() instanceof WordTriple) {
                WordTriple wt = (WordTriple) cont.getRecord();
                correct = wt.getOrgWord1().getLang().equalsIgnoreCase(LocaleHelper.RU) &&
                        wt.getOrgWord2().getLang().equalsIgnoreCase(LocaleHelper.RU) &&
                        wt.getOrgWord3().getLang().equalsIgnoreCase(LocaleHelper.RU) &&
                        wt.getTranslWord().getLang().equalsIgnoreCase(LocaleHelper.EN);
                Log.d("test", "Wrong wordTriple: " + wt.toString() + ". OrgWord must be RU and transl must be EN");
            }
            if (!correct)
                break;
        }
        assertTrue(correct);

    }

}
