package cc.snakelab.edisco.api;

import android.support.test.runner.AndroidJUnit4;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.Word_Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by snake on 03.04.17.
 */

@RunWith(AndroidJUnit4.class)
public class ApiUserWordPairTest extends ApiTest {
    Date mDate;
    @Override
    public void setUp() {
        super.setUp();
        //hard coded date in db which is same on android and cloud
        try {
            mDate = DateHelper.iso8601ZToDate("2017-04-20 12:12:59.123 UTC");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserWordPair() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs();
        mSignal.await();
        
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);
        String uuid = mResult.getData().getJSONObject(0).optString("uuid", null);
        
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs(-1, -1, -1, uuid);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() == 1);
    }


    @Test
    public void getUserWordPairWithPaginationAndTimestamp() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs(1, 1, mDate.getTime() - 1, null);
        mSignal.await();

        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);

        int totalCount = mResult.getTotalCount();
        int count = 1;
        int page = mResult.getPage();
        while (page > 0) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWordPairs(mResult.getPage() + 1, 1, mDate.getTime(), null);
            mSignal.await();
            Thread.sleep(apiDelay);

            assertEquals(true, mResult.isOk());
            ja = mResult.getJson().optJSONArray("data");
            count += ja.length();
            page = mResult.getPage();

        }
        assertEquals(totalCount, count);
    }
    @Test
    public void getUserWordPairWithTimestamp() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs(mDate.getTime() + 1 );
        mSignal.await();

        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(0, ja.length());

        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs(mDate.getTime() - 1);
        mSignal.await();

        assertEquals(true, mResult.isOk());
        ja = mResult.getJson().optJSONArray("data");
        assertNotEquals(0, ja.length());

    }

    @Test
    public void createUserWordPair() throws InterruptedException, JSONException, ParseException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        Word w1 = null;
        Word w2 = null;
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            String text = mResult.getData().getJSONObject(i).optJSONObject("attributes").optString("text");
            if (text.equals("dog"))
                w1 = Word.fromJSON(mResult.getData().getJSONObject(i));
            if (text.equals("собака"))
                w2 = Word.fromJSON(mResult.getData().getJSONObject(i));
        }
        assertNotNull(w1);
        assertNotNull(w2);
        mWordMerger.merge(w1);
        w1 = Word.findByUuid(Word.class, w1.getUuid());
        mWordMerger.merge(w2);
        w2 = Word.findByUuid(Word.class, w2.getUuid());


        WordPair wp = new WordPair();
        wp.setStage(1);
        wp.setOrder(2);
        wp.setOrgWordId(w1.getId());
        wp.setTranslWordId(w2.getId());
        
        mSignal = new CountDownLatch(1);
        mApi.createUserWordPair(wp);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
        assertNotNull(mResult.getData().getJSONObject(0).optString("uuid", null));
    }
    @Test
    public void updateWordPair() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs();
        mSignal.await();
        Thread.sleep(apiDelay);

        WordPair wp = WordPair.fromJSON(mResult.getData().getJSONObject(0));
        Word word = SQLite.select().from(Word.class).where(Word_Table.text.eq("dog")).querySingle();
        wp.setTranslWordId(word.getId());
        wp.setVersion(wp.getVersion() + 1);
        
        mSignal = new CountDownLatch(1);
        mApi.updateUserWordPair(wp);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(wp.getUuid(), mResult.getData().getJSONObject(0).optString("uuid"));

        assertEquals(word.getUuid(), mResult.getData().getJSONObject(0).optJSONObject("attributes").optString("back_uuid"));
    }
    @Test
    public void deleteWordPair() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs();
        mSignal.await();
        Thread.sleep(apiDelay);

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordPairMerger.merge(WordPair.fromJSON(mResult.getData().getJSONObject(i)));

        }
        WordPair wp = SQLite.select().from(WordPair.class).querySingle();
        mSignal = new CountDownLatch(1);
        mApi.deleteUserWordPair(wp);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
    }
}
