package cc.snakelab.edisco.api;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.WordTripleHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 18.04.17.
 */

public class ApiUserWordTripleHistoryTest extends ApiTest {
    @Test
    public void testGetUserWordTripleHistories() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTripleHistories();
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES, mResult.getMethodId());
        assertEquals(true, mResult.isOk());

        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);
        String uuid = mResult.getData().getJSONObject(0).optString("uuid", null);

        mSignal = new CountDownLatch(1);
        mApi.getUserWordTripleHistories(-1, -1, uuid);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES, mResult.getMethodId());

        ja = mResult.getJson().optJSONArray("data");
        assertEquals(1, ja.length());

    }
    @Test
    public void testGetUserWordTripleHistoriesWithPagination() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTripleHistories(-1, 1, null);
        mSignal.await();
        Thread.sleep(apiDelay);

        int totalCount = mResult.getTotalCount();
        int count = 1;
        int page = mResult.getPage();

        while (page != -1) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWordTripleHistories(page + 1, 1, null);
            mSignal.await();
            Thread.sleep(apiDelay);
            page = mResult.getPage();
            count += mResult.getData().length();


        }
        assertEquals(totalCount, count);
    }
    @Test
    public void createUserWordTripleHistory() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORDS, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLES, mResult.getMethodId());

        WordTriple wordTriple = WordTriple.fromJSON(mResult.getData().getJSONObject(0));
        wordTriple = (WordTriple) mWordTripleMerger.merge(wordTriple);

        WordTripleHistory wth = new WordTripleHistory();
        wth.setWord_triple_id(wordTriple.getId());
        wth.setCorrect(true);
        Date date = DateHelper.getCalendar().getTime();
        wth.setCreated_at(date);
        wth.save();

        mSignal = new CountDownLatch(1);
        mApi.createUserWordTripleHistory(wth);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(EdiscoApi.CREATE_USER_WORD_TRIPLE_HISTORY, mResult.getMethodId());
        assertEquals(true, mResult.isOk());

        assertNotNull(mResult.getData().getJSONObject(0).optString("uuid", null));
        wth = WordTripleHistory.fromJSON(mResult.getData().getJSONObject(0));
        assertEquals(date, wth.getCreated_at());

        int size = SQLite.select().from(WordTripleHistory.class).queryList().size();
        mWordTripleHistoryMerger.merge(wth);
        assertEquals(size, SQLite.select().from(WordTripleHistory.class).queryList().size());
    }

    @Test
    public void updateWordTripleHistories() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORDS, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLES, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordTripleMerger.merge(WordTriple.fromJSON(mResult.getData().getJSONObject(i)));
        }

        mSignal = new CountDownLatch(1);
        mApi.getUserWordTripleHistories();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES, mResult.getMethodId());

        WordTripleHistory wth = WordTripleHistory.fromJSON(mResult.getData().getJSONObject(0));
        wth.setVersion(wth.getVersion() + 1);
        wth.setTrashed(true);

        mSignal = new CountDownLatch(1);
        mApi.updateUserWordTripleHistory(wth);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.UPDATE_USER_WORD_TRIPLE_HISTORIES, mResult.getMethodId());

    }
    @Test
    public void deleteUserWrodTripleHistory() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTripleHistories();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_TRIPLE_HISTORIES, mResult.getMethodId());

        String uuid  = mResult.getData().getJSONObject(0).optString("uuid");
        WordTripleHistory wth = new WordTripleHistory();
        wth.setUuid(uuid);

        mSignal = new CountDownLatch(1);
        mApi.deleteUserWordTripleHistory(wth);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.DELETE_USER_WORD_TRIPLE_HISTORY, mResult.getMethodId());
    }
}
