package cc.snakelab.edisco.api;

import com.android.volley.VolleyError;

import cc.snakelab.edisco.BuildConfig;
import cc.snakelab.edisco.core.App;
import cc.snakelab.edisco.core.AppLog;
import cc.snakelab.edisco.core.WordTripleHistoryMerger;
import cc.snakelab.edisco.core.WordTripleMerger;
import cc.snakelab.edisco.main.EmulatorTest;
import cc.snakelab.edisco.core.WordMerger;
import cc.snakelab.edisco.core.WordPairHistoryMerger;
import cc.snakelab.edisco.core.WordPairMerger;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.CountDownLatch;



import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 03.04.17.
 */


public class ApiTest extends EmulatorTest implements EdiscoApi.ApiResultListener  {
    final static private String email = "test@edisco.snakelab.cc";
    final static protected int apiDelay = 0;
    protected EdiscoApi mApi;
    protected CountDownLatch mSignal;
    protected Api.ApiResult mResult;
    protected WordMerger mWordMerger;
    protected WordPairMerger mWordPairMerger;
    protected WordTripleMerger mWordTripleMerger;
    protected WordPairHistoryMerger mWordPairHistoryMerger;
    protected WordTripleHistoryMerger mWordTripleHistoryMerger;
    @Override
    public void setUp() {
        super.setUp();
        mApi = new EdiscoApi(this,
                App.preference.getApiServerUrl(),
                App.preference.getEmail(),
                App.preference.getApiToken());
        mWordMerger = new WordMerger();
        mWordPairMerger = new WordPairMerger();
        mWordPairHistoryMerger = new WordPairHistoryMerger();
        mWordTripleMerger = new WordTripleMerger();
        mWordTripleHistoryMerger = new WordTripleHistoryMerger();

        AppLog.d("APiTest", (new Date()).toString());
    }



    @Override
    public void onErrorResponse(VolleyError error) {
        mSignal.countDown();
    }

    @Override
    public void onResponse(Api.ApiResult response) {
        mResult = response;
        mSignal.countDown();

    }
}
