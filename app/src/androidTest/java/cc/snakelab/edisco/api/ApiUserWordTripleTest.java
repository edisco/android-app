package cc.snakelab.edisco.api;

import android.support.test.runner.AndroidJUnit4;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordTriple;
import cc.snakelab.edisco.orm.Word_Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 07.04.17.
 */


@RunWith(AndroidJUnit4.class)
public class ApiUserWordTripleTest extends ApiTest{
    private Date mDate;

    @Override
    public void setUp() {
        super.setUp();
        //hard coded date in db which is same on android and cloud
        try {
            mDate = DateHelper.iso8601ZToDate("2017-04-20 12:12:59.123 UTC");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserWordTriples() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples();
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);
        String uuid = mResult.getData().getJSONObject(0).optString("uuid", null);

        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples(-1, -1, -1, uuid);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
        ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() == 1);
    }

    @Test
    public void getUserWordTriplesWithTimestamp() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples(mDate.getTime() - 1);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);

        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples(mDate.getTime() + 1);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        ja = mResult.getJson().optJSONArray("data");
        assertEquals(0, ja.length());

    }
    @Test
    public void getUserWordTriplesPagination() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples(-1, 1, -1, null);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        int totalCount = mResult.getTotalCount();
        int page = mResult.getPage();
        int count = 1;
        while (page > 0) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWordTriples(mResult.getPage() + 1, 1, -1, null);
            mSignal.await();
            Thread.sleep(apiDelay);

            assertEquals(true, mResult.isOk());
            JSONArray ja = mResult.getJson().optJSONArray("data");
            count += ja.length();
            page = mResult.getPage();
        }
        assertEquals(totalCount, count);
    }

    @Test
    public void getUserWordTriplesPaginationWithTimesamp() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples(-1, 1, mDate.getTime() -1, null);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        int totalCount = mResult.getTotalCount();
        int page = mResult.getPage();
        int count = 1;
        while (page > 0) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWordTriples(mResult.getPage() + 1, 1, mDate.getTime() - 1, null);
            mSignal.await();
            Thread.sleep(apiDelay);

            assertEquals(true, mResult.isOk());
            JSONArray ja = mResult.getJson().optJSONArray("data");
            count += ja.length();
            page = mResult.getPage();
        }
        assertEquals(totalCount, count);
    }
    @Test
    public void update() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples();
        mSignal.await();
        Thread.sleep(apiDelay);

        WordTriple wt = WordTriple.fromJSON(mResult.getData().getJSONObject(0));
        Word word = SQLite.select().from(Word.class).where(Word_Table.text.eq("dog")).querySingle();
        wt.setTransWordId(word.getId());
        wt.setVersion(wt.getVersion() + 1);

        mSignal = new CountDownLatch(1);
        mApi.updateUserWordTriple(wt);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());

    }
    @Test
    public void delete() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordTriples();
        mSignal.await();
        Thread.sleep(apiDelay);
        WordTriple wt = WordTriple.fromJSON(mResult.getData().getJSONObject(0));

        mSignal = new CountDownLatch(1);
        mApi.deleteUserWordTriple(wt);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());


    }
}
