package cc.snakelab.edisco.api;

import android.support.test.runner.AndroidJUnit4;

import cc.snakelab.edisco.helper.DateHelper;

import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.test.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static cc.snakelab.edisco.api.EdiscoApi.WIZARD_SETUP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by snake on 14.03.17.
 */
@RunWith(AndroidJUnit4.class)
public class ApiUserWordTest extends ApiTest {

    @Test
    public void getUserWords() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);

    }
    @Test
    public void getUserWordsLastTimestamp() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        assertEquals(true, mResult.isOk());
        assertEquals("2017-04-21 12:12:59.123", DateHelper.dateToDBIso8601(mResult.getLastTimestamp()));

    }


    @Test
    public void getUserWordsWithTimestamp() throws InterruptedException, ParseException {
        //hard coded date in db which is same on android and cloud
        Date date = DateHelper.iso8601ZToDate("2017-04-20 12:13:00.123 UTC");

        mSignal = new CountDownLatch(1);
        mApi.getUserWords(date.getTime());
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(2, ja.length());
    }
    @Test
    public void getUserWordsWithTimestampAndPagination() throws InterruptedException, ParseException {
        //hard coded date in db which is same on android and cloud
        Date date = DateHelper.iso8601ZToDate("2017-04-20 12:13:00.123 UTC");

        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, date.getTime(), null);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() == 1);
        assertEquals(true, mResult.getTotalCount() > 0);
        int totalCount = mResult.getTotalCount();
        int count = 1;
        int page = mResult.getPage();
        while (page > 0) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWords(mResult.getPage() + 1, 1, date.getTime(), null);
            mSignal.await();
            Thread.sleep(apiDelay);

            assertEquals(true, mResult.isOk());
            ja = mResult.getJson().optJSONArray("data");
            count += ja.length();
            page = mResult.getPage();

        }
        assertEquals(totalCount, count);
    }
    @Test
    public void getUserWordsWithPagination() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, null);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() == 1);
        assertEquals(true, mResult.getTotalCount() > 0);
        int totalCount = mResult.getTotalCount();
        int count = 1;
        int page = mResult.getPage();
        while (page > 0) {
            mSignal = new CountDownLatch(1);
            mApi.getUserWords(mResult.getPage() + 1, 1, null);
            mSignal.await();
            Thread.sleep(apiDelay);

            assertEquals(true, mResult.isOk());
            ja = mResult.getJson().optJSONArray("data");
            count += ja.length();
            page = mResult.getPage();

        }
        assertEquals(totalCount, count);
    }
    @Test
    public void updateUserWord() throws InterruptedException, JSONException, ParseException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, null);
        mSignal.await();
        Thread.sleep(apiDelay);
        JSONObject jo = mResult.getData().getJSONObject(0);
        Word word = Word.fromJSON(jo);
        String newText = word.getText()+"test";
        int newVersion = word.getVersion() + 1;


        word.setText(newText);
        word.setVersion(newVersion);
        mSignal = new CountDownLatch(1);
        mApi.updateUserWord(word);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
        assertEquals(200, mResult.getStatusCode());

        assertEquals(newText, mResult.getData().getJSONObject(0).getJSONObject("attributes").getString("text"));
        assertEquals(newVersion, mResult.getData().getJSONObject(0).getJSONObject("attributes").getInt("version"));

    }
    @Test
    public void updateUserWordWithNewLang() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, null);
        mSignal.await();
        Thread.sleep(apiDelay);
        JSONObject jo = mResult.getData().getJSONObject(0);
        Word word = Word.fromJSON(jo);
        String new_lang = "uk";

        int newVersion = word.getVersion() + 1;



        word.setLang(new_lang);
        String dateStr = "2017-04-20 12:12:59.123 UTC";
        Date date = DateHelper.iso8601ZToDate(dateStr);
        word.setCreated_at(date);
        word.setVersion(newVersion);
        mSignal = new CountDownLatch(1);
        mApi.updateUserWord(word);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(200, mResult.getStatusCode());

        assertEquals(new_lang, mResult.getData().getJSONObject(0).getJSONObject("attributes").getString("lang"));
        assertEquals(newVersion, mResult.getData().getJSONObject(0).getJSONObject("attributes").getInt("version"));
        assertEquals(date, Word.fromJSON(mResult.getData().getJSONObject(0)).getCreated_at());

    }

    @Test
    public void createUserWord() throws InterruptedException, JSONException, ParseException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, null);
        mSignal.await();
        Thread.sleep(apiDelay);
        Word word = new Word();
        word.setText("new_test");
        word.setLang("en");

        mSignal = new CountDownLatch(1);
        mApi.createUserWord(word);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
        JSONObject data = mResult.getData().getJSONObject(0);
        assertEquals("new_test", data.getJSONObject("attributes").getString("text"));

        word = Word.fromJSON(data);

        mWordMerger.merge(word);
        Word newWord = Word.findByUuid(Word.class, word.getUuid());
        assertEquals(true, newWord != null);

    }
    @Test
    public void createUserWord2() throws InterruptedException, JSONException, ParseException {
        Word word = new Word();
        word.setLang("en");
        word.setText("new word");
        word.save();

        mSignal = new CountDownLatch(1);
        mApi.createUserWord(word);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        JSONObject data = mResult.getData().getJSONObject(0);
        word = Word.fromJSON(data);
        mWordMerger.merge(word);

        Word newWord = Word.findByUuid(Word.class, word.getUuid());
        assertEquals(true, newWord != null);
    }
    @Test
    public void deleteUserWord() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords(1, 1, null);
        mSignal.await();
        Thread.sleep(apiDelay);
        String uuid = mResult.getData().getJSONObject(0).getString("uuid");
        mSignal = new CountDownLatch(1);
        mApi.deleteUserWord(uuid);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
    }








}
