package cc.snakelab.edisco.api;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import cc.snakelab.edisco.helper.DateHelper;
import cc.snakelab.edisco.orm.Word;
import cc.snakelab.edisco.orm.WordPair;
import cc.snakelab.edisco.orm.WordPairHistory;
import cc.snakelab.edisco.orm.WordPairHistory_Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 11.04.17.
 */

public class ApiUserWordPairHistoryTest extends ApiTest {
    @Test
    public void getUserWordPairHistories() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairHistories();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIR_HISTORIES, mResult.getMethodId());
        JSONArray ja = mResult.getJson().optJSONArray("data");
        assertEquals(true, ja.length() > 0);
        String uuid = mResult.getData().getJSONObject(0).optString("uuid", null);

        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairHistories(-1, -1, uuid);
        mSignal.await();
        Thread.sleep(apiDelay);
        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIR_HISTORIES, mResult.getMethodId());

        ja = mResult.getJson().optJSONArray("data");
        assertEquals(1, ja.length());
    }
    @Test
    public void createUserWordPairHistory() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORDS, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIRS, mResult.getMethodId());

        WordPair wordPair = WordPair.fromJSON(mResult.getData().getJSONObject(0));
        wordPair = (WordPair) mWordPairMerger.merge(wordPair);

        WordPairHistory wph = new WordPairHistory();
        wph.setWord_pair_id(wordPair.getId());
        Date date = DateHelper.getCalendar().getTime();
        wph.setCreated_at(date);
        wph.save();

        mSignal = new CountDownLatch(1);
        mApi.createUserWordPairHistory(wph);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.CREATE_USER_WORD_PAIR_HISTORY, mResult.getMethodId());

        assertNotNull(mResult.getData().getJSONObject(0).optString("uuid", null));
//        DateHelper.convertToTimeZone(date, "");
        wph = WordPairHistory.fromJSON(mResult.getData().getJSONObject(0));
        assertEquals(DateHelper.dateToIso8601(date), DateHelper.dateToIso8601(wph.getCreated_at()));
        assertEquals(date, wph.getCreated_at());
        int size = SQLite.select().from(WordPairHistory.class).queryList().size();
        mWordPairHistoryMerger.merge(wph);
        assertEquals(size, SQLite.select().from(WordPairHistory.class).queryList().size());
    }

    @Test
    public void updateUserWordPairHistory() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWords();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORDS, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordMerger.merge(Word.fromJSON(mResult.getData().getJSONObject(i)));

        }

        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairs();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIRS, mResult.getMethodId());

        for(int i = 0; i < mResult.getData().length(); i++ ) {
            mWordPairMerger.merge(WordPair.fromJSON(mResult.getData().getJSONObject(i)));
        }

        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairHistories();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIR_HISTORIES, mResult.getMethodId());

        WordPairHistory wordPairHistory = null;
        for(int i = 0; i < mResult.getData().length(); i++ ) {
            WordPairHistory wph = WordPairHistory.fromJSON(mResult.getData().getJSONObject(i));
            wordPairHistory = (WordPairHistory) mWordPairHistoryMerger.merge(wph);
        }
//        WordPairHistory wph = SQLite.select().from(WordPairHistory.class).where(WordPairHistory_Table.uuid.isNotNull()).querySingle();
        wordPairHistory.setVersion(wordPairHistory.getVersion()+ 1);
        wordPairHistory.setTrashed(true);


        mSignal = new CountDownLatch(1);
        mApi.updateUserWordPairHistory(wordPairHistory);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(EdiscoApi.UPDATE_USER_WORD_PAIR_HISTORIES, mResult.getMethodId());
        assertEquals(true, mResult.isOk());
    }

    @Test
    public void deleteUserWordPairHistory() throws Exception {
        mSignal = new CountDownLatch(1);
        mApi.getUserWordPairHistories();
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.GET_USER_WORD_PAIR_HISTORIES, mResult.getMethodId());

        String uuid  = mResult.getData().getJSONObject(0).optString("uuid");
        WordPairHistory wph = new WordPairHistory();
        wph.setUuid(uuid);

        mSignal = new CountDownLatch(1);
        mApi.deleteUserWordPairHistory(wph);
        mSignal.await();
        Thread.sleep(apiDelay);

        assertEquals(true, mResult.isOk());
        assertEquals(EdiscoApi.DELETE_USER_WORD_PAIR_HISTORY, mResult.getMethodId());
    }
}
