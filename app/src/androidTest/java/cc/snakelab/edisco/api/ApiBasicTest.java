package cc.snakelab.edisco.api;

import android.support.test.runner.AndroidJUnit4;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import cc.snakelab.edisco.helper.LocaleHelper;
import cc.snakelab.edisco.test.BuildConfig;

import static cc.snakelab.edisco.api.EdiscoApi.TRANSLATION_SUGGESTIONS;
import static cc.snakelab.edisco.api.EdiscoApi.WIZARD_SETUP;
import static org.junit.Assert.assertEquals;

/**
 * Created by snake on 11.09.17.
 */
@RunWith(AndroidJUnit4.class)
public class ApiBasicTest extends ApiTest {
    @Test
    public void signIn() throws InterruptedException {
        mSignal = new CountDownLatch(1);
        mApi.signIn("test@test.com", BuildConfig.EDISCO_TEST_TOKEN);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        assertEquals(true, mResult.getJson().optJSONObject("meta").optBoolean("new"));

    }
    @Test
    public void wizardSetup() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.wizardSetup("en", "ru");
        mSignal.await();
        assertEquals(true, mResult.isOk());
        assertEquals(WIZARD_SETUP, mResult.getMethodId());

    }
    @Test
    public void translationSuggestions() throws InterruptedException, JSONException {
        mSignal = new CountDownLatch(1);
        mApi.translationSuggestion("dog", LocaleHelper.EN, LocaleHelper.RU);
        mSignal.await();
        assertEquals(true, mResult.isOk());
        assertEquals(TRANSLATION_SUGGESTIONS, mResult.getMethodId());
        JSONArray ja = mResult.getData();
        assertEquals("собака", ja.getJSONObject(0).optJSONObject("attributes").optString("back_text"));

    }
}
